﻿using AllPacka.DataContainer.Request;
using AllPacka.DataContainer.Response;
using System.Collections.Generic;
using System;
using AllPacka.DataContainer.Common;
using ParcelrateUtilities;
using Microsoft.Extensions.Logging;
using AllPacka.Interfaces;
using Microsoft.Extensions.Configuration;

namespace AllPacka
{
    public class AvailableServices : IAvailableServices
    {
        private readonly ILogger<AvailableServices> _logger;
        private readonly IConfiguration _configuration;

        public AvailableServices(IConfiguration configuration, ILogger<AvailableServices> logger)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public AvailableCarriersResponseModel ProcessRequest(AvailableCarriersRequestModel request)
        {
            AvailableCarriersResponseModel result = null;
            try
            {
                var payload = mapGetAvailableServicesData(request);
                var response = Helper.sendServiceRequest(payload, Constants.ViewModels.AvailableServices, _configuration.GetSection("Settings:AllPackaServiceURL").Value);
                result = processGetAvailableServicesResponse(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.AvailableServices, Constants.MethodName.ProcessRequest, ex));
            }
            return result;
        }

        #region "Get Available Services Request"

        /// <summary>
        /// Map payload information to 
        /// </summary>
        private RootObject mapGetAvailableServicesData(AvailableCarriersRequestModel request)
        {
            return new RootObject()
            {
                REQUEST = new Request()
                {
                    cdLang = "EN",
                    flDebug = "true",
                    txEmail = request.UserName,
                    txPassword = request.PassWord,
                    QUOTE = Helper.mapShipmentDetails(request)
                }
            };
        }
        #endregion

        #region "Get Available services Response"

        private AvailableCarriersResponseModel processGetAvailableServicesResponse(Response response)
        {
            AvailableCarriersResponseModel result = null;

            if (response.Messages == null && response.Quotes != null && response.Quotes.Count > 0)
            {
                result = new AvailableCarriersResponseModel();

                result.Services = new List<CarrierModel>(response.Quotes.Count);

                foreach (var quote in response.Quotes)
                {
                    result.Services.Add(
                        new CarrierModel()
                        {
                            Id = quote.Service.idCarrier,
                            ServiceId = quote.Service.idService,
                            ServiceName = quote.Service.nmService,
                            Name = quote.Service.nmCarrier,
                            Messages = quote.Service.Attributes,
                            PriceInfo = new PriceInfoResponseModel()
                            {
                                NetPrice = quote.Service.amNet,
                                TotalVat = Convert.ToDecimal(quote.Service.amVAT),
                                TotalPrice = quote.Service.amNet + Convert.ToDecimal(quote.Service.amVAT)
                            }
                        });
                }

            }
            else
                if (response.Messages != null && response.Messages.Count > 0)
                result = new AvailableCarriersResponseModel { Messages = response.Messages };

            return result;
        }

        #endregion
    }
}
