﻿using AllPacka.DataContainer.Common;
using AllPacka.DataContainer.Request;
using AllPacka.DataContainer.Response;
using AllPacka.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ParcelrateUtilities;
using System;
using System.Collections.Generic;

namespace AllPacka
{
    public class ShipmentServices : IShipmentServices
    {
        private readonly ILogger<ShipmentServices> _logger;
        private readonly IConfiguration _configuration;

        public ShipmentServices(IConfiguration configuration, ILogger<ShipmentServices> logger)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public CreateShipmentResponseModel ProcessRequest(CreateShipmentRequestModel request)
        {
            CreateShipmentResponseModel result = null;
            try
            {
                var payload = mapCreateShipmentServicesData(request);
                var response = Helper.sendServiceRequest(payload, Constants.ViewModels.ShipmentServices, _configuration.GetSection("Settings:AllPackaServiceURL").Value);
                result = processCreateShipmentServicesResponse(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentServices, Constants.MethodName.ProcessRequest, ex));
            }
            return result;
        }

        #region "Get Available Services Request"
        /// <summary>
        /// Map payload information to 
        /// </summary>
        private RootObject mapCreateShipmentServicesData(CreateShipmentRequestModel request)
        {
            return new RootObject()
            {
                REQUEST = new BookingRequest()
                {
                    cdLang = "EN",
                    flDebug = "false",
                    txEmail = request.UserName,
                    txPassword = request.PassWord,
                    QUOTE = Helper.mapShipmentDetails(request),
                    BOOK = mapBookingDetails(request)
                }
            };
        }

        private BookModel mapBookingDetails(CreateShipmentRequestModel request)
        {
            return new BookModel()
            {
                dtPickup = request.PickupDate,
                idCarrier = request.CarrierId,
                idService = request.ServiceId
            };
        }

        #endregion

        #region "Get Available services Response"

        private CreateShipmentResponseModel processCreateShipmentServicesResponse(Response response)
        {
            CreateShipmentResponseModel result = null;

            if (response.Quotes != null && response.Quotes.Count > 0)
            {
                result = new CreateShipmentResponseModel();

                result.Services = new List<CarrierModel>(response.Quotes.Count);

                foreach (var quote in response.Quotes)
                {
                    result.Services.Add(
                        new CarrierModel()
                        {
                            Id = quote.Service.idCarrier,
                            ServiceId = quote.Service.idService,
                            ServiceName = quote.Service.nmService,
                            Name = quote.Service.nmCarrier,
                            Messages = quote.Service.Attributes,
                            PriceInfo = new PriceInfoResponseModel()
                            {
                                NetPrice = quote.Service.amNet,
                                TotalVat = Convert.ToDecimal(quote.Service.amVAT)
                            }
                        });
                }
                result.Quotes = response.Quotes;
            }
            else
                if (response.Messages != null && response.Messages.Count > 0)
                result = new CreateShipmentResponseModel { Messages = response.Messages };

            return result;
        }

        #endregion
    }
}
