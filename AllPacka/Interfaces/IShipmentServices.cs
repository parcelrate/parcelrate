﻿namespace AllPacka.Interfaces
{
    public interface IShipmentServices
    {
        CreateShipmentResponseModel ProcessRequest(CreateShipmentRequestModel request);
    }
}
