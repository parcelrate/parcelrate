﻿namespace AllPacka.Interfaces
{
    public interface IServices
    {

        /// <summary>
        /// Gets available carrier services with rates for provided shipment
        /// </summary>        
        AvailableCarriersResponseModel GetAvailableServices(AvailableCarriersRequestModel request);

        /// <summary>
        /// Creates a shipment for given parcel details and for a selected carrier
        /// </summary>
        CreateShipmentResponseModel CreateShipment(CreateShipmentRequestModel request);

    }
}
