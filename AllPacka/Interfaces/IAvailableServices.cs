﻿namespace AllPacka.Interfaces
{
    public interface IAvailableServices
    {
        AvailableCarriersResponseModel ProcessRequest(AvailableCarriersRequestModel request);
    }
}
