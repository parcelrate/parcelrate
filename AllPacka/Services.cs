﻿
using AllPacka.Interfaces;

namespace AllPacka
{
    public class Services : IServices
    {
        private readonly IAvailableServices _availableServices;
        private readonly IShipmentServices _shipmentServices;

        public Services(IAvailableServices availableServices, IShipmentServices shipmentServices)
        {
            _availableServices = availableServices;
            _shipmentServices = shipmentServices;
        }

        /// <summary>
        /// Gets available carrier services with rates for provided shipment
        /// </summary>        
        public AvailableCarriersResponseModel GetAvailableServices(AvailableCarriersRequestModel request)
        {
            return _availableServices.ProcessRequest(request);
        }

        /// <summary>
        /// Creates a shipment for given parcel details and for a selected carrier
        /// </summary>
        public CreateShipmentResponseModel CreateShipment(CreateShipmentRequestModel request)
        {
            return _shipmentServices.ProcessRequest(request);
        }

    }
}
