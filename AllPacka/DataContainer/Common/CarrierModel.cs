﻿using AllPacka.DataContainer.Response;
using System.Collections.Generic;

namespace AllPacka
{
    public class CarrierModel
    {
        public int Id { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get;  set; }
        public string Name { get;  set; }
        public byte[] Image { get;  set; }
        public string CutoffTime { get;  set; }
        public bool CutoffTimeSpecified { get;  set; }
        public string Description { get;  set; }
        public List<Attribute> Messages { get; set; }
        public string TransferTime { get; set; }
        public PriceInfoResponseModel PriceInfo { get; set; }
    }
}