﻿using AllPacka.DataContainer.Request;
using System;
using System.Collections.Generic;
using AllPacka.DataContainer.Response;
using ParcelrateUtilities;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;
using Microsoft.Extensions.Logging;

namespace AllPacka.DataContainer.Common
{
    internal static class Helper
    {
        private static ILogger Logger { get; } = LogginManager.CreateLogger<Services>();
        #region "Get Available Services Request"

        internal static DataContainer.Request.Quote mapShipmentDetails(AvailableCarriersRequestModel request)
        {
            return new DataContainer.Request.Quote()
            {
                tyCOD = "NONE",
                flNothingProhibited = "true",
                flAgreedToTermsAndConditions = "true",
                flInsured = "false",
                ORDERER = mapOrdererDetails(request),
                ADDRESSES = mapAddresses(request),
                PACKAGES = mapParcels(request.Parcels)
            };
        }

        internal static Orderer mapOrdererDetails(AvailableCarriersRequestModel request)
        {
            return new Orderer()
            {
                nmFirst = request.PickupAddress.FirstName,
                nmLast = request.PickupAddress.LastName,
                txPhone = @request.PickupAddress.PhonePrefix + request.PickupAddress.Phone,
                txEmail = request.PickupAddress.Email
            };
        }

        internal static Addresses mapAddresses(AvailableCarriersRequestModel request)
        {
            return new Addresses()
            {
                COLLECTION = mapCollectionDetails(request),
                DESTINATION = mapDestinationDetails(request),
                INVOICE = mapInvoiceDetails(request)
            };
        }

        internal static Collection mapCollectionDetails(AvailableCarriersRequestModel request)
        {
            return new Collection()
            {
                nmCompanyOrPerson = @request.PickupAddress.FirstName + " " + request.PickupAddress.LastName,
                cdCountry = request.PickupAddress.CountryCode,
                txAddress = request.PickupAddress.Street,
                txAddressNumber = request.PickupAddress.House,
                txPost = request.PickupAddress.Zip,
                txCity = request.PickupAddress.City,
                nmContact = @request.PickupAddress.FirstName + request.PickupAddress.LastName,
                txPhoneContact = @request.PickupAddress.PhonePrefix + request.PickupAddress.Phone,
                txEmailContact = request.PickupAddress.Email,
                txInstruction = "Some Instructions"
            };
        }

        internal static Destination mapDestinationDetails(AvailableCarriersRequestModel request)
        {
            return new Destination()
            {
                nmCompanyOrPerson = @request.DeliveryAddress.FirstName + " " + request.DeliveryAddress.LastName,
                cdCountry = request.DeliveryAddress.CountryCode,
                txAddress = request.DeliveryAddress.Street,
                txAddressNumber = request.DeliveryAddress.House,
                txPost = request.DeliveryAddress.Zip,
                txCity = request.DeliveryAddress.City,
                nmContact = request.DeliveryAddress.Company,
                txPhoneContact = @request.DeliveryAddress.PhonePrefix + request.DeliveryAddress.Phone,
                txEmailContact = request.DeliveryAddress.Email
            };
        }

        internal static Invoice mapInvoiceDetails(AvailableCarriersRequestModel request)
        {
            return new Invoice()
            {
                nmCompanyOrPerson = @request.DeliveryAddress.FirstName + " " + request.DeliveryAddress.LastName,
                cdCountry = request.DeliveryAddress.CountryCode,
                txAddress = request.DeliveryAddress.Street,
                txAddressNumber = request.DeliveryAddress.House,
                txPost = request.DeliveryAddress.Zip,
                txCity = request.DeliveryAddress.City,
                flCompany = "false",
                txInvoiceEmail = request.DeliveryAddress.Email
            };
        }

        internal static Packages mapParcels(List<ParcelModel> parcels)
        {
            Package[] items = new Package[parcels.Count];
            var ctr = 0;

            foreach (var parcel in parcels)
            {
                int weight = Convert.ToInt32(parcel.Weight, System.Globalization.CultureInfo.InvariantCulture);
                var item = new Package()
                {
                    tyPackage = parcel.ShipmentType,
                    ctPackage = parcel.Quantity,
                    cmWidth = parcel.Width,
                    cmLength = parcel.Length,
                    cmHeight = parcel.Height,
                    gWt = weight <= 0 ? 1 : weight,
                    amContent = parcel.GoodsValue,
                    txContent = parcel.Description
                };

                items[ctr] = item;
                ctr++;
            }

            return new Packages() { PACKAGE = items };
        }

        /// <summary>
        /// Send service request
        /// </summary>
        /// <param name="payload"></param>
        internal static AllPacka.DataContainer.Response.Response sendServiceRequest(RootObject payload, string viewModelName,string serviceURL)
        {
            AllPacka.DataContainer.Response.Response result = new AllPacka.DataContainer.Response.Response();
            string data = string.Empty;
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(payload), Encoding.UTF8, "application/json");

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(serviceURL);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // List data response.
                HttpResponseMessage response = client.PostAsync(serviceURL, content).Result;  // Blocking call!
                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body. Blocking!
                    data = response.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<AllPacka.DataContainer.Response.Response>(data);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(StaticFunctions.LogErrorMessage(viewModelName, Constants.MethodName.sendServiceRequest, ex));
                if (!string.IsNullOrEmpty(data))
                    result.Messages = new List<Message> { (new Message { Type = 0, Text = data }) };
            }
            return result;
        }
        #endregion
    }
}
