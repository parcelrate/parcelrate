﻿namespace AllPacka.DataContainer.Request
{
    public class Request
    {
        public string flDebug { get; set; }
        public string txEmail { get; set; }
        public string txPassword { get; set; }
        public string cdLang { get; set; }
        public Quote QUOTE { get; set; }
    }
}
