﻿namespace AllPacka.DataContainer.Request
{
    public class Collection
    {
        public string nmCompanyOrPerson { get; set; }
        public string cdCountry { get; set; }
        public string txAddress { get; set; }
        public string txAddressNumber { get; set; }
        public string txPost { get; set; }
        public string txCity { get; set; }
        public string nmContact { get; set; }
        public string txPhoneContact { get; set; }
        public string txEmailContact { get; set; }
        public string txInstruction { get; set; }
    }

}
