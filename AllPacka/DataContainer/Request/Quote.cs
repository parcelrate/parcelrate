﻿namespace AllPacka.DataContainer.Request
{    
    public class Quote
    {
        public string tyCOD { get; set; }
        public string flNothingProhibited { get; set; }
        public string flAgreedToTermsAndConditions { get; set; }
        public string flInsured { get; set; }
        public Orderer ORDERER { get; set; }
        public Addresses ADDRESSES { get; set; }
        public Packages PACKAGES { get; set; }
    }

}
