﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllPacka.DataContainer.Request
{
    public class Orderer
    {
        public string nmLast { get; set; }
        public string nmFirst { get; set; }
        public string txPhone { get; set; }
        public string txEmail { get; set; }
    }

}
