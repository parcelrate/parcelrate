﻿
namespace AllPacka.DataContainer.Request
{
    public class AddressModel
    {
        public long CountryId{ get; set; }
        public string CountryCode { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string PhonePrefix { get; set; }
        public string Email { get; set; }
        public string Company { get; set; }
    }
}
