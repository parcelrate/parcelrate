﻿using AllPacka.DataContainer.Request;

namespace AllPacka
{
    internal class BookingRequest:Request
    {
        public BookModel BOOK { get; set; }
    }
}