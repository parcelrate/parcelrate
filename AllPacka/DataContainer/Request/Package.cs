﻿namespace AllPacka.DataContainer.Request
{
    public class Package
    {
        public string tyPackage { get; set; }
        public int ctPackage { get; set; }
        public int cmWidth { get; set; }
        public int cmLength { get; set; }
        public int cmHeight { get; set; }
        public int gWt { get; set; }
        public string amContent { get; set; }
        public string txContent { get; set; }
    }

}
