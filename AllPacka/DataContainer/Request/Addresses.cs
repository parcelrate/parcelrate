﻿namespace AllPacka.DataContainer.Request
{
    public class Addresses
    {
        public Collection COLLECTION { get; set; }
        public Destination DESTINATION { get; set; }
        public Invoice INVOICE { get; set; }
    }
}
