﻿namespace AllPacka.DataContainer.Request
{
    public class Invoice
    {
        public string nmCompanyOrPerson { get; set; }
        public string cdCountry { get; set; }
        public string txAddress { get; set; }
        public string txAddressNumber { get; set; }
        public string txPost { get; set; }
        public string txCity { get; set; }
        public string flCompany { get; set; }
        public string txVAT { get; set; }
        public string txInvoiceEmail { get; set; }
    }

}
