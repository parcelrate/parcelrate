﻿using AllPacka.DataContainer.Request;
using System.Collections.Generic;

namespace AllPacka
{
    public class AvailableCarriersRequestModel
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public AddressModel PickupAddress { get; set; }
        public AddressModel DeliveryAddress { get; set; }      
        public List<ParcelModel> Parcels { get; set; }
        public string ContentDescription { get; set; }                
    }
}