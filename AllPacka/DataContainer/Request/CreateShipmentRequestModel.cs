﻿namespace AllPacka
{
    public class CreateShipmentRequestModel:AvailableCarriersRequestModel
    {   
        public int CarrierId { get; set; }
        public int ServiceId { get; set; }
        public string PickupDate { get; set; }
    }
}