﻿using System.Collections.Generic;

namespace AllPacka.DataContainer.Response
{
    public class Quote
    {
        public int idQuote { get; set; }
        public int idOperation { get; set; }
        public Service Service { get; set; }
        public List<object> WayBills { get; set; }
        public List<object> Labels { get; set; }
    }
}