﻿using System.Collections.Generic;
using AllPacka.DataContainer.Response;

namespace AllPacka
{
    public class AvailableCarriersResponseModel
    {
        public List<CarrierModel> Services { get; set; }
        public List<Message> Messages { get; set; }
    }
}