﻿using System.Collections.Generic;
namespace AllPacka
{
    public class CreateShipmentResponseModel: AvailableCarriersResponseModel
    {
        public List<AllPacka.DataContainer.Response.Quote> Quotes { get; set; }
    }
}