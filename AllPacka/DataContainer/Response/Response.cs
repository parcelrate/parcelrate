﻿using System.Collections.Generic;

namespace AllPacka.DataContainer.Response
{
    public class Response
    {
        public bool flDebug { get; set; }
        public string cdLang { get; set; }
        public string dtGenerated { get; set; }
        public string tyReply { get; set; }
        public List<Quote> Quotes { get; set; }
        public List<Message> Messages { get; set; }
    }

    public class Message
    {
        public int Type { get; set; }
        public string Text { get; set; }
    }
}
