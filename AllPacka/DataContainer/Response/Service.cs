﻿using System.Collections.Generic;

namespace AllPacka.DataContainer.Response
{
    public class Service
    {
        public int idCarrier { get; set; }
        public string nmCarrier { get; set; }
        public int idService { get; set; }
        public string nmService { get; set; }
        public bool flCanPPFrom { get; set; }
        public bool flCanPPTo { get; set; }
        public bool flCanCOD { get; set; }
        public bool flCanINS { get; set; }
        public bool flMustPRINT { get; set; }
        public int amNet { get; set; }
        public double amVAT { get; set; }
        public int amInsuranceNet { get; set; }
        public double amInsuranceVAT { get; set; }
        public List<string> lsPossiblePickupDates { get; set; }
        public List<AllPacka.DataContainer.Response.Attribute> Attributes { get; set; }
    }
}