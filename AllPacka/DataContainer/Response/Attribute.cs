﻿namespace AllPacka.DataContainer.Response
{
    public class Attribute
    {
        public string Type { get; set; }
        public string Description { get; set; }
    }
}
