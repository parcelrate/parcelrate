﻿namespace LetMeShip
{
    public class PriceInfoResponseModel
    {   
        public decimal RealWeight { get; set; }
        public decimal BillingWeight { get; set; }        
        public decimal BasePrice { get; set; }        
        public decimal DiscountedPrice { get; set; }
        public decimal NetPrice { get; set; }       
        public decimal TotalVat { get; set; }        
        public decimal TotalPrice { get; set; }        
    }
}