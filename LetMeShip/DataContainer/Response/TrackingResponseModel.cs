﻿using LetMeShip.DataContainer.Common;
using LetMeShip.DataContainer.Request;
using LetMeShip.Sandbox;
using System;
using System.Collections.Generic;

namespace LetMeShip.DataContainer.Response
{
    public class TrackingResponseModel : TrackingModel
    {
        public string CurrentTrackingStatus { get; set; }
        public trackingState LmsTrackingStatus { get; set; }
        public DateTime CurrentStatusTime { get; set; }
        public string CurrentStatusLocation { get; set; }
        public List<TrackingEvent> TrackingEventList { get; set; }
        public List<string> Messages { get; set; } 
    }

    public class TrackingEvent
    {
        public string Date { get; set; }
        public string Time { get; set; }
        public string Location { get; set; }
        public string Event { get; set; }
    }
}
