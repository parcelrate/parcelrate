﻿using System.Collections.Generic;
namespace LetMeShip
{
    public class GetLabelResponseModel
    {
        public string MimeType { get; set; }
        public string Status { get; set; }
        public string DocumentType { get; set; }
        public byte[] Data { get; set; }
        public List<string> Messages { get; set; }
    }
}