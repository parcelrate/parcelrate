﻿
using LetMeShip.Sandbox;
using System.Collections.Generic;
namespace LetMeShip
{
    public class ShipmentStateResponseModel
    {
        public List<Shipment> Shipments { get; set; }
        public List<string> Messages { get; set; }

        public ShipmentStateResponseModel()
        {
            Shipments = new List<Shipment>();
            Messages = new List<string>();
        }
    }

    public class Shipment
    {
        public long ShipmentID { get; set; }
        public shipmentState ShipmentState { get; set; }
        public labelState LabelState { get; set; }
    }
}
