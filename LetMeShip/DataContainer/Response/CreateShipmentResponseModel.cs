﻿using System.Collections.Generic;
namespace LetMeShip
{
    public class CreateShipmentResponseModel
    {
        public long ShipmentId { get; set; }
        public List<string> Messages { get; set; }
    }
}