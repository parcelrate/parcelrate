﻿using System.Collections.Generic;

namespace LetMeShip
{
    public class AvailableCarriersResponseModel
    {
        public List<CarrierModel> Services { get; set; }
        public List<string> Messages { get; set; } 
    }
}