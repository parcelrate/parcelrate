﻿namespace LetMeShip
{
    public class CreateShipmentRequestModel: AvailableCarriersRequestModel {

        public CarrierModel SelectedService { get; set; }
        public decimal TotalPrice { get; set; }     

    }    
}