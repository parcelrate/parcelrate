﻿using System;
using System.Collections.Generic;

namespace LetMeShip
{
    public class AvailableCarriersRequestModel
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public Address PickupAddress { get; set; }
        public Address DeliveryAddress { get; set; }
        public List<ParcelModel> Parcels { get; set; }
        public string ContentDescription { get; set; }
        public decimal GoodsValue { get; set; }
        public bool GoodsValueSpecified { get; set; }
        public DateTime PickupInterval_Date { get; set; }
        public DateTime PickupTimeFrom { get; set; }
        public DateTime PickupTimeTo { get; set; }
        public string Culture { get; set; }
        public bool IsPickupOrder { get; set; }
    }
}