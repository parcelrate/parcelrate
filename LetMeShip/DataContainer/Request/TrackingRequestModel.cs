﻿
using LetMeShip.DataContainer.Common;
namespace LetMeShip.DataContainer.Request
{
    public class TrackingRequestModel : TrackingModel
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string Culture { get; set; }
    }
}
