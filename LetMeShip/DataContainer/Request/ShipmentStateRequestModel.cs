﻿
using System.Collections.Generic;
namespace LetMeShip
{
    public class ShipmentStateRequestModel
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public List<ShipmentCountryRequest> Shipments { get; set; }
        public string Culture { get; set; }
    }

    public class ShipmentCountryRequest
    {
        public long ShipmentID { get; set; }
        public string CountryCode { get; set; }
    }
}
