﻿namespace LetMeShip
{
    public class GetLabelRequestModel{

        public string UserName { get; set; }
        public string PassWord { get; set; }
        public long ShipmentId { get; set; }
        public bool SendLabel { get; set; }
        public bool SendSummary { get; set; }
        public string Culture { get; set; }
        public string CountryCode { get; set; }
    }    
}