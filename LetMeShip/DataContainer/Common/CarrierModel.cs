﻿using System;
using System.Collections.Generic;

namespace LetMeShip
{
    public class CarrierModel
    {
        public int Id { get; set; }
        public string ServiceName { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public DateTime CutoffTime { get; set; }
        public bool CutoffTimeSpecified { get; set; }
        public string Description { get; set; }
        public List<string> Messages { get; set; }
        public string TransferTime { get; set; }

        public PriceInfoResponseModel LetMeShipPriceInfo { get; set; }
        public List<SurchargeModel> Surcharges { get; set; }
    }
}