﻿
namespace LetMeShip.DataContainer.Common
{
    public class TrackingModel
    {
        public long ShipmentId { get; set; }
        public string Carrier { get; set; }
        public string AwbNumber { get; set; }
        public string CountryCode { get; set; }
    }
}
