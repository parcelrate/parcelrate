﻿namespace LetMeShip
{
    public class SurchargeModel
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public decimal Percentage { get; set; }
    }
}