﻿namespace LetMeShip
{
    public class ParcelModel
    {
        public string ShipmentType { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public decimal Weight { get; set; }
        public int Quantity { get; set; }
    }
}