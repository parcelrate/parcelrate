﻿using LetMeShip.Sandbox;
using System.Collections.Generic;

namespace LetMeShip.DataContainer.Common
{
    internal static class Helper
    {
        internal static clientInfo mapClientInfo(string culture)
        {
            return new clientInfo()
            {
                language = !string.IsNullOrEmpty(culture) && culture.ToLower().Contains("de") ? supportedLangType.de : supportedLangType.en,
                languageSpecified = true,
                note = ""
            };
        }

        internal static parcelItem[] mapParcels(List<ParcelModel> parcels)
        {
            parcelItem[] items = null;

            if (parcels != null && parcels.Count > 0)
            {
                var ctr = 0;
                items = new parcelItem[parcels.Count];

                foreach (var parcel in parcels)
                {
                    var item = new parcelItem()
                    {
                        height = parcel.Height,
                        length = parcel.Length,
                        width = parcel.Width,
                        quantity = parcel.Quantity,
                        quantitySpecified = true,
                        weight = parcel.Weight
                    };

                    items[ctr] = item;
                    ctr++;
                }
            }

            return items;
        }

        internal static address mapPickupAddress(AvailableCarriersRequestModel payload)
        {
            return new address()
            {
                countryCode = payload.PickupAddress.CountryCode,
                zip = payload.PickupAddress.Zip,
                city = payload.PickupAddress.City,
                street = payload.PickupAddress.Street,
                houseNo = payload.PickupAddress.House,
                addressInfo1 = "",
                addressInfo2 = "",
                company = payload.PickupAddress.Company,
                firstName = payload.PickupAddress.FirstName,
                lastName = payload.PickupAddress.LastName,
                phoneNumber = payload.PickupAddress.Phone,
                phonePrefix = payload.PickupAddress.PhonePrefix,
                email = payload.PickupAddress.Email
            };
        }

    }
}
