﻿using LetMeShip.DataContainer.Common;
using LetMeShip.DataContainer.Request;
using LetMeShip.DataContainer.Response;
using LetMeShip.Interfaces;
using LetMeShip.Sandbox;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ParcelrateUtilities;
using System;
using System.Linq;
using System.ServiceModel;

namespace LetMeShip
{
    public class TrackingServices : ITrackingServices
    {
        private IConfiguration _configuration;
        private readonly ILogger<TrackingServices> _logger;

        public TrackingServices(IConfiguration configuration, ILogger<TrackingServices> logger)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public TrackingResponseModel ProcessRequest(TrackingRequestModel payload)
        {
            TrackingResponseModel responseModel = null;
            try
            {
                var request = preparePayload(payload);
                var response = sendGetTrackingServicesRequest(payload.UserName, payload.PassWord, request, payload.CountryCode);
                responseModel = processGetTrackingServicesResponse(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.TrackingServices, Constants.MethodName.ProcessRequest, ex));
            }
            return responseModel;
        }

        #region "Get Tracking Services Request"

        /// <summary>
        /// Map payload information to 
        /// </summary>
        private getTrackingDataRequest preparePayload(TrackingRequestModel payload)
        {
            return new getTrackingDataRequest()
            {
                clientInfo = Helper.mapClientInfo(payload.Culture),
                carrier = payload.Carrier,
                awbNumber = payload.AwbNumber,
                shipmentId = payload.ShipmentId
            };
        }

        /// <summary>
        /// Send service request
        /// </summary>
        /// <param name="payload"></param>
        getTrackingDataResponse sendGetTrackingServicesRequest(string un, string pw, getTrackingDataRequest request, string countryCode)
        {
            //Specify the binding to be used for the client.
            var binding = new BasicHttpsBinding { MaxReceivedMessageSize = int.MaxValue, MaxBufferSize = int.MaxValue };

            string serviceEnpoint = string.Empty;
            if (!string.IsNullOrEmpty(countryCode) && countryCode.ToLower() == "ch") // If pickup country switzerland
                serviceEnpoint = _configuration.GetSection("Settings:LetMeShipSwissServiceURL").Value;
            else
                serviceEnpoint = _configuration.GetSection("Settings:LetMeShipServiceURL").Value;

            //Specify the address to be used for the client.
            var address = new EndpointAddress(serviceEnpoint);
            // Create a client that is configured with this address and binding.
            LmsWsImpl service = new LmsWsImplClient(binding, address);

            var trackingDataRequest = new GetTrackingDataRequest1(new security() { usernameToken = new usernameToken() { username = un, password = pw } }, request);
            var response = service.GetTrackingDataAsync(trackingDataRequest);
            return response?.Result?.GetTrackingDataResponse;
        }

        #endregion

        #region "Get Available services Response"

        TrackingResponseModel processGetTrackingServicesResponse(getTrackingDataResponse response)
        {
            TrackingResponseModel result = null;
            if (response != null)
            {
                if (response.status.code == 0)
                {
                    result = new TrackingResponseModel
                    {
                        ShipmentId = response.shipmentId,
                        Carrier = response.carrier,
                        AwbNumber = response.awbNumber,
                        CurrentTrackingStatus = response.currectTrackingStatus,
                        LmsTrackingStatus = response.lmsTrackingStatus,
                        CurrentStatusTime = response.currentStatusTime,
                        CurrentStatusLocation = response.currentStatusLocation,
                        TrackingEventList = new System.Collections.Generic.List<TrackingEvent>(response.trackingEventList.OrderByDescending(x => x.dateTime).Select(
                            tc => new TrackingEvent
                            {
                                Date = tc.dateTime != default(DateTime) ? tc.dateTime.ToString(Constants.DefaultDateFormat) : string.Empty,
                                Time = tc.dateTime != default(DateTime) ? tc.dateTime.ToShortTimeString() : string.Empty,
                                Location = tc.location,
                                Event = tc.description
                            }
                            ))
                    };
                }
                else
                {
                    if (response.status.message != null)
                        result = new TrackingResponseModel { Messages = response.status.message.ToList() };
                }
            }
            return result;
        }

        #endregion
    }
}
