﻿
using LetMeShip.DataContainer.Request;
using LetMeShip.DataContainer.Response;
using LetMeShip.Interfaces;

namespace LetMeShip
{
    public class Services : IServices
    {
        private readonly IAvailableServices _availableServices;
        private readonly IShipmentServices _shipmentServices;
        private readonly ILabelServices _labelServices;
        private readonly ITrackingServices _trackingServices;

        public Services(IAvailableServices availableServices,IShipmentServices shipmentServices, ILabelServices labelServices, ITrackingServices trackingServices)
        {
            _availableServices = availableServices;
            _shipmentServices = shipmentServices;
            _labelServices = labelServices;
            _trackingServices = trackingServices;
        }

        /// <summary>
        /// Gets available carrier services with rates for provided shipment
        /// </summary>
        /// <param name="shipment"></param>
        public AvailableCarriersResponseModel GetAvailableServices(AvailableCarriersRequestModel request)
        {
            return _availableServices.ProcessRequest(request);
        }

        /// <summary>
        /// Creates a shipment for given parcel details and for a selected carrier
        /// </summary>
        public CreateShipmentResponseModel CreateShipment(CreateShipmentRequestModel request)
        {
            return _shipmentServices.ProcessRequest(request);
        }

        /// <summary>
        /// Gets label for service
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetLabelResponseModel GetLabel(GetLabelRequestModel request)
        {
            return _labelServices.ProcessRequest(request);
        }

        /// <summary>
        /// Gets tracking detail shipment
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public TrackingResponseModel GetTracking(TrackingRequestModel request)
        {
            return _trackingServices.ProcessRequest(request);
        }

        /// <summary>
        /// Gets tracking detail shipment
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ShipmentStateResponseModel GetShipmentsStatus(ShipmentStateRequestModel request)
        {
            return _shipmentServices.GetShipmentsStatus(request);
        }
    }
}