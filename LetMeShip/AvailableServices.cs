﻿using LetMeShip.DataContainer.Common;
using LetMeShip.Interfaces;
using LetMeShip.Sandbox;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ParcelrateUtilities;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace LetMeShip
{
    public class AvailableServices : IAvailableServices
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<AvailableServices> _logger;

        public AvailableServices(IConfiguration configuration, ILogger<AvailableServices> logger)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public AvailableCarriersResponseModel ProcessRequest(AvailableCarriersRequestModel payload)
        {
            AvailableCarriersResponseModel responseModel = null;
            try
            {
                var request = preparePayload(payload);
                var response = sendGetAvailableServicesRequest(payload.UserName, payload.PassWord, request);
                responseModel = processGetAvailableServicesResponse(response, payload.Culture);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.AvailableServices, Constants.MethodName.ProcessRequest, ex));
            }
            return responseModel;
        }

        #region "Get Available Services Request"

        /// <summary>
        /// Map payload information to 
        /// </summary>
        private availableServicesRequest preparePayload(AvailableCarriersRequestModel payload)
        {
            return new availableServicesRequest()
            {
                clientInfo = Helper.mapClientInfo(payload.Culture),
                deliveryAddress = mapDeliveryAddress(payload),
                pickupAddress = Helper.mapPickupAddress(payload),
                shipmentDetails = mapShipmentDetails(payload)
            };
        }

        private address mapDeliveryAddress(AvailableCarriersRequestModel payload)
        {
            return new address()
            {
                countryCode = payload.DeliveryAddress.CountryCode,
                zip = payload.DeliveryAddress.Zip,
                city = payload.DeliveryAddress.City,
                street = payload.DeliveryAddress.Street,
                houseNo = payload.DeliveryAddress.House,
                addressInfo1 = "",
                addressInfo2 = "",
                firstName = payload.DeliveryAddress.FirstName,
                lastName = payload.DeliveryAddress.LastName,
                phoneNumber = payload.DeliveryAddress.Phone,
                phonePrefix = payload.DeliveryAddress.PhonePrefix,
                email = payload.DeliveryAddress.Email,
                company = payload.DeliveryAddress.Company
            };
        }

        private shipmentDetails mapShipmentDetails(AvailableCarriersRequestModel payload)
        {
            return new shipmentDetails()
            {
                shipmentType = payload.Parcels != null && payload.Parcels[0].ShipmentType == Constants.DocumentText ? shipmentType.document : shipmentType.parcel,
                goodsValue = payload.GoodsValue,
                goodsValueSpecified = payload.GoodsValueSpecified,
                parcels = Helper.mapParcels(payload.Parcels),
                pickupInterval = new pickupInterval() { date = payload.PickupInterval_Date, timeFromSpecified = true, timeToSpecified = true, timeFrom = payload.PickupTimeFrom, timeTo = payload.PickupTimeTo },
                contentDescription = payload.ContentDescription,
                note = "",
                serviceType = transportType.standard,
                pickupOrder = payload.IsPickupOrder
            };
        }

        /// <summary>
        /// Send service request
        /// </summary>
        /// <param name="payload"></param>
        availableServicesResponse sendGetAvailableServicesRequest(string un, string pw, availableServicesRequest request)
        {
            //Specify the binding to be used for the client.
            var binding = new BasicHttpsBinding { MaxReceivedMessageSize = int.MaxValue, MaxBufferSize = int.MaxValue };

            string serviceEnpoint = string.Empty;
            if (!string.IsNullOrEmpty(request.pickupAddress.countryCode) && request.pickupAddress.countryCode.ToLower() == "ch") // If pickup country switzerland
                serviceEnpoint = _configuration.GetSection("Settings:LetMeShipSwissServiceURL").Value;
            else
                serviceEnpoint = _configuration.GetSection("Settings:LetMeShipServiceURL").Value;

            //Specify the address to be used for the client.
            var address = new EndpointAddress(serviceEnpoint);
            // Create a client that is configured with this address and binding.
            LmsWsImpl service = new LmsWsImplClient(binding, address);
            var response = service.GetAvailableServicesAsync(new GetAvailableServicesRequest(new security() { usernameToken = new usernameToken() { username = un, password = pw } }, request));
            return response?.Result?.AvailableServicesResponse;
        }

        #endregion

        #region "Get Available services Response"

        AvailableCarriersResponseModel processGetAvailableServicesResponse(availableServicesResponse response, string culture)
        {
            AvailableCarriersResponseModel result = null;

            if (response != null)
            {
                if (response.status.code == 0)
                {
                    result = new AvailableCarriersResponseModel();

                    if (response.services != null && response.services.Length > 0)
                    {
                        result.Services = new List<CarrierModel>(response.services.Length);

                        foreach (var service in response.services)
                        {
                            result.Services.Add(
                                new CarrierModel()
                                {
                                    Id = service.id,
                                    Name = service.carrier,
                                    ServiceName = service.name,
                                    CutoffTime = service.cutOffTime,
                                    CutoffTimeSpecified = true,
                                    TransferTime = culture != null && culture.Contains("de") && service.transferTime != null ? (service.transferTime.Replace(Constants.DaysText, string.Empty).Replace(Constants.DayText, string.Empty)).Replace(Constants.BusinessText, ParcelrateResources.Resources.Business_Days) : service.transferTime,
                                    Messages = service.message != null ? service.message.ToList() : null,
                                    Description = service.description,
                                    Image = service.carrierImage,
                                    Surcharges = getSurcharges(service.priceInfo.surcharges),
                                    LetMeShipPriceInfo = new PriceInfoResponseModel()
                                    {
                                        BasePrice = service.priceInfo.basePrice,
                                        BillingWeight = service.priceInfo.billingWeight,
                                        DiscountedPrice = service.priceInfo.discountedPrice,
                                        NetPrice = service.priceInfo.netPrice,
                                        RealWeight = service.priceInfo.realWeight,
                                        TotalPrice = service.priceInfo.totalPrice,
                                        TotalVat = service.priceInfo.totalVat
                                    }
                                });
                        }
                    }
                }
                else
                {
                    if (response.status.message != null)
                        result = new AvailableCarriersResponseModel { Messages = response.status.message.ToList() };
                }
            }
            return result;
        }

        List<SurchargeModel> getSurcharges(surcharge[] surcharges)
        {
            List<SurchargeModel> result = null;

            if (surcharges != null && surcharges.Length > 0)
            {
                result = new List<SurchargeModel>(surcharges.Length);

                foreach (var surcharge in surcharges)
                {
                    result.Add(
                      new SurchargeModel()
                      {
                          Name = surcharge.name,
                          Amount = surcharge.amount,
                          Percentage = surcharge.percentage
                      });
                }
            }

            return result;
        }


        #endregion
    }
}