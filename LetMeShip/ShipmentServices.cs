﻿using LetMeShip.Sandbox;
using System.Linq;
using LetMeShip.DataContainer.Common;
using ParcelrateUtilities;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using System.ServiceModel;
using Microsoft.Extensions.Configuration;
using LetMeShip.Interfaces;

namespace LetMeShip
{
    public class ShipmentServices : IShipmentServices
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<ShipmentServices> _logger;

        public ShipmentServices(IConfiguration configuration, ILogger<ShipmentServices> logger)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public CreateShipmentResponseModel ProcessRequest(CreateShipmentRequestModel payload)
        {
            CreateShipmentResponseModel responseModel = null;
            try
            {
                var request = preparePayload(payload);
                var response = sendCreateShipmentServiceRequest(payload.UserName, payload.PassWord, request);
                responseModel = processCreateShipmentResponse(response);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentServices, Constants.MethodName.ProcessRequest, ex));
            }
            return responseModel;
        }

        #region "Create Shipment Request"
        /// <summary>
        /// Map payload information to 
        /// </summary>
        createShipmentRequest preparePayload(CreateShipmentRequestModel payload)
        {
            return new createShipmentRequest()
            {
                clientInfo = Helper.mapClientInfo(payload.Culture),
                deliveryAddress = mapBookingDeliveryAddress(payload),
                service = mapService(payload),
                pickupAddress = Helper.mapPickupAddress(payload),
                shipmentDetails = mapShipmentDetails(payload),
                labelOptions = new labelOptions() { labelSize = labelSize.Item100x150mm, labelSizeSpecified = true },
                trackingOptions = null
            };
        }

        private bookingDeliveryAddress mapBookingDeliveryAddress(CreateShipmentRequestModel payload)
        {
            return new bookingDeliveryAddress()
            {
                countryCode = payload.DeliveryAddress.CountryCode,
                zip = payload.DeliveryAddress.Zip,
                city = payload.DeliveryAddress.City,
                street = payload.DeliveryAddress.Street,
                houseNo = payload.DeliveryAddress.House,
                addressInfo1 = "",
                addressInfo2 = "",
                firstName = payload.DeliveryAddress.FirstName,
                lastName = payload.DeliveryAddress.LastName,
                phoneNumber = payload.DeliveryAddress.Phone,
                phonePrefix = payload.DeliveryAddress.PhonePrefix,
                email = payload.DeliveryAddress.Email,
                company = payload.DeliveryAddress.Company
            };
        }

        private serviceDetail mapService(CreateShipmentRequestModel payload)
        {
            return new serviceDetail()
            {
                id = payload.SelectedService.Id,
                carrier = payload.SelectedService.ServiceName,
                name = payload.SelectedService.Name
            };
        }

        bookingShipmentDetails mapShipmentDetails(CreateShipmentRequestModel payload)
        {
            return new bookingShipmentDetails()
            {
                shipmentType = payload.Parcels != null && payload.Parcels[0].ShipmentType == Constants.DocumentText ? shipmentType.document : shipmentType.parcel,
                goodsValue = payload.GoodsValue,
                goodsValueSpecified = payload.GoodsValueSpecified,
                parcels = Helper.mapParcels(payload.Parcels),
                pickupInterval = new pickupInterval() { date = payload.PickupInterval_Date, timeFromSpecified = true, timeToSpecified = true, timeFrom = payload.PickupTimeFrom, timeTo = payload.PickupTimeTo },
                contentDescription = payload.ContentDescription,
                note = "",
                pickupOrder = payload.IsPickupOrder
            };
        }

        /// <summary>
        /// Send service request
        /// </summary>
        /// <param name="payload"></param>
        createShipmentResponse sendCreateShipmentServiceRequest(string un, string pw, createShipmentRequest request)
        {
            //Specify the binding to be used for the client.
            var binding = new BasicHttpsBinding { MaxReceivedMessageSize = int.MaxValue, MaxBufferSize = int.MaxValue };

            string serviceEnpoint = string.Empty;
            if (!string.IsNullOrEmpty(request.pickupAddress.countryCode) && request.pickupAddress.countryCode.ToLower() == "ch") // If pickup country switzerland
                serviceEnpoint = _configuration.GetSection("Settings:LetMeShipSwissServiceURL").Value;
            else
                serviceEnpoint = _configuration.GetSection("Settings:LetMeShipServiceURL").Value;

            //Specify the address to be used for the client.
            var address = new EndpointAddress(serviceEnpoint);
            // Create a client that is configured with this address and binding.
            LmsWsImpl service = new LmsWsImplClient(binding, address);

            var createShipmentRequest = new CreateShipmentRequest1(new security() { usernameToken = new usernameToken() { username = un, password = pw } }, request);
            var response = service.CreateShipmentAsync(createShipmentRequest);
            return response?.Result?.CreateShipmentResponse;
        }
        #endregion

        #region "Get Available services Response"

        CreateShipmentResponseModel processCreateShipmentResponse(createShipmentResponse response)
        {
            CreateShipmentResponseModel result = null;

            if (response.status.code == 0)
            {
                result = new CreateShipmentResponseModel() { ShipmentId = response.shipmentId };
            }
            else
            {
                if (response.status.message != null)
                    result = new CreateShipmentResponseModel() { Messages = response.status.message.ToList() };
            }
            return result;
        }

        #endregion

        public ShipmentStateResponseModel GetShipmentsStatus(ShipmentStateRequestModel payload)
        {
            ShipmentStateResponseModel responseModel = null;
            try
            {
                var request = prepareShipmentStateRequest(payload);
                responseModel = sendGetShipmentStateRequest(payload.UserName, payload.PassWord, payload);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentServices, Constants.MethodName.ProcessRequest, ex));
            }
            return responseModel;
        }

        #region "Create Shipment State Request"
        /// <summary>
        /// Map payload information to 
        /// </summary>
        static getShipmentStatusRequest prepareShipmentStateRequest(ShipmentStateRequestModel shipmentStausRequest)
        {
            return new getShipmentStatusRequest()
            {
                clientInfo = Helper.mapClientInfo(shipmentStausRequest.Culture),
                shipments = new List<shipmentId>(shipmentStausRequest.Shipments.Select(x => new shipmentId { shipmentId1 = x.ShipmentID })).ToArray()
            };
        }

        /// <summary>
        /// Send service request
        /// </summary>
        /// <param name="payload"></param>
        ShipmentStateResponseModel sendGetShipmentStateRequest(string un, string pw, ShipmentStateRequestModel request)
        {
            var resultSwiss = new ShipmentStateResponseModel();
            var resultOther = new ShipmentStateResponseModel();
            var response = new ShipmentStateResponseModel();

            if (request.Shipments.Any(s => s.CountryCode?.ToLower() == "ch"))
            {
                string serviceEnpoint = _configuration.GetSection("Settings:LetMeShipSwissServiceURL").Value;
                var requestModel = new getShipmentStatusRequest()
                {
                    clientInfo = Helper.mapClientInfo(request.Culture),
                    shipments = new List<shipmentId>(request.Shipments.Where(s => s.CountryCode?.ToLower() == "ch").Select(x => new shipmentId { shipmentId1 = x.ShipmentID })).ToArray()
                };
                var result = GetStateReponse(un, pw, requestModel, serviceEnpoint);
                resultSwiss = processGetShipmentStateResponse(result);
            }
            if (request.Shipments.Any(s => s.CountryCode == null || s.CountryCode?.ToLower() != "ch"))
            {
                string serviceEnpoint = _configuration.GetSection("Settings:LetMeShipServiceURL").Value;
                var requestModel = new getShipmentStatusRequest()
                {
                    clientInfo = Helper.mapClientInfo(request.Culture),
                    shipments = new List<shipmentId>(request.Shipments.Where(s => s.CountryCode == null || s.CountryCode?.ToLower() != "ch").Select(x => new shipmentId { shipmentId1 = x.ShipmentID })).ToArray()
                };
                var res = GetStateReponse(un, pw, requestModel, serviceEnpoint);
                resultOther = processGetShipmentStateResponse(res);
            }
            response.Shipments = resultSwiss.Shipments.Union(resultOther.Shipments).ToList();
            response.Messages = resultSwiss.Messages.Union(resultOther.Messages).ToList();
            return response;
        }

        private getShipmentStateResponse GetStateReponse(string un, string pw, getShipmentStatusRequest request, string serviceEnpoint)
        {
            //Specify the binding to be used for the client.
            var binding = new BasicHttpsBinding { MaxReceivedMessageSize = int.MaxValue, MaxBufferSize = int.MaxValue };
            //Specify the address to be used for the client.
            var address = new EndpointAddress(serviceEnpoint);
            // Create a client that is configured with this address and binding.
            LmsWsImpl service = new LmsWsImplClient(binding, address);
            var shipmentStatusRequest = new GetShipmentStateRequest(new security() { usernameToken = new usernameToken() { username = un, password = pw } }, request);
            return service.GetShipmentStateAsync(shipmentStatusRequest).Result?.GetShipmentStateResponse;
        }

        #endregion

        #region "Get Shipment Status Response"

        ShipmentStateResponseModel processGetShipmentStateResponse(getShipmentStateResponse response)
        {
            ShipmentStateResponseModel result = null;
            if (response != null)
            {
                if (response != null && response.status.code == 0)
                {
                    result = new ShipmentStateResponseModel();
                    if (response.shipments != null && response.shipments.Count() > 0)
                    {
                        result.Shipments = new List<Shipment>(response.shipments.Length);

                        foreach (var shipment in response.shipments)
                        {
                            result.Shipments.Add(
                                new Shipment()
                                {
                                    ShipmentID = shipment.shipmentId1,
                                    ShipmentState = shipment.shipmentState,
                                    LabelState = shipment.labelState
                                });
                        }
                    }
                }
                else
                {
                    if (response.status.message != null)
                        result = new ShipmentStateResponseModel() { Messages = response.status.message.ToList() };
                }
            }
            return result;
        }

        #endregion
    }
}