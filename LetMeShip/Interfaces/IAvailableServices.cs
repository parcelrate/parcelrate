﻿namespace LetMeShip.Interfaces
{
    public interface IAvailableServices
    {
        AvailableCarriersResponseModel ProcessRequest(AvailableCarriersRequestModel payload);
    }
}
