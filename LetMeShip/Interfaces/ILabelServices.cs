﻿namespace LetMeShip.Interfaces
{
    public interface ILabelServices
    {
        GetLabelResponseModel ProcessRequest(GetLabelRequestModel payload);
    }
}
