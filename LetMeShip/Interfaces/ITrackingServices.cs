﻿using LetMeShip.DataContainer.Request;
using LetMeShip.DataContainer.Response;

namespace LetMeShip.Interfaces
{
    public interface ITrackingServices
    {
        TrackingResponseModel ProcessRequest(TrackingRequestModel payload);
    }
}
