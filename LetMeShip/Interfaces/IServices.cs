﻿using LetMeShip.DataContainer.Request;
using LetMeShip.DataContainer.Response;

namespace LetMeShip.Interfaces
{
    public interface IServices
    {
        AvailableCarriersResponseModel GetAvailableServices(AvailableCarriersRequestModel request);

        CreateShipmentResponseModel CreateShipment(CreateShipmentRequestModel request);

        GetLabelResponseModel GetLabel(GetLabelRequestModel request);

        TrackingResponseModel GetTracking(TrackingRequestModel request);

        ShipmentStateResponseModel GetShipmentsStatus(ShipmentStateRequestModel request);
    }
}
