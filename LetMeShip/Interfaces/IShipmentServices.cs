﻿namespace LetMeShip.Interfaces
{
    public interface IShipmentServices
    {
        CreateShipmentResponseModel ProcessRequest(CreateShipmentRequestModel payload);

        ShipmentStateResponseModel GetShipmentsStatus(ShipmentStateRequestModel payload);
    }
}
