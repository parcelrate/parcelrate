﻿using LetMeShip.Sandbox;
using System.Linq;
using LetMeShip.DataContainer.Common;
using ParcelrateUtilities;
using Microsoft.Extensions.Logging;
using System.ServiceModel;
using Microsoft.Extensions.Configuration;
using LetMeShip.Interfaces;

namespace LetMeShip
{
    public class LabelServices : ILabelServices
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<LabelServices> _logger;

        public LabelServices(IConfiguration configuration, ILogger<LabelServices> logger)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public GetLabelResponseModel ProcessRequest(GetLabelRequestModel payload)
        {
            GetLabelResponseModel responseModel = null;
            try
            {
                var request = preparePayload(payload);
                var response = sendGetLabelServiceRequest(payload.UserName, payload.PassWord, request, payload.CountryCode);
                responseModel = processGetLabelServicesResponse(response);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.LabelServices, Constants.MethodName.ProcessRequest, ex));
            }
            return responseModel;
        }

        #region "Get Label Request"
        /// <summary>
        /// Map payload information to 
        /// </summary>
        getDocumentsRequest preparePayload(GetLabelRequestModel payload)
        {
            return new getDocumentsRequest()
            {
                clientInfo = Helper.mapClientInfo(payload.Culture),
                sendLabel = true,
                sendManifest = true,
                sendSummary = true,
                shipmentId = payload.ShipmentId
            };
        }

        /// <summary>
        /// Send service request
        /// </summary>
        /// <param name="payload"></param>
        getDocumentsResponse sendGetLabelServiceRequest(string un, string pw, getDocumentsRequest request, string countryCode)
        {
            //Specify the binding to be used for the client.
            var binding = new BasicHttpsBinding { MaxReceivedMessageSize = int.MaxValue, MaxBufferSize = int.MaxValue };

            string serviceEnpoint = string.Empty;
            if (!string.IsNullOrEmpty(countryCode) && countryCode.ToLower() == "ch") // If pickup country switzerland
                serviceEnpoint = _configuration.GetSection("Settings:LetMeShipSwissServiceURL").Value;
            else
                serviceEnpoint = _configuration.GetSection("Settings:LetMeShipServiceURL").Value;

            //Specify the address to be used for the client.
            var address = new EndpointAddress(serviceEnpoint);
            // Create a client that is configured with this address and binding.
            LmsWsImpl service = new LmsWsImplClient(binding, address);

            var documentsRequest = new GetDocumentsRequest1(new security() { usernameToken = new usernameToken() { username = un, password = pw } }, request);
            var response = service.GetDocumentsAsync(documentsRequest);
            return response?.Result?.GetDocumentsResponse;
        }
        #endregion

        #region "Get Available services Response"

        GetLabelResponseModel processGetLabelServicesResponse(getDocumentsResponse response)
        {
            GetLabelResponseModel result = null;
            if (response != null)
            {
                if (response.status.code == 0 && response.documents.Length > 0)
                {
                    document label = response.documents.FirstOrDefault(o => o.type == documentType.label);

                    if (label != null)
                    {
                        result = new GetLabelResponseModel()
                        {
                            Data = label.data,
                            DocumentType = "PDF",
                            MimeType = "application/pdf",
                            Status = label.status.ToString()
                        };
                    }
                }
                else
                {
                    if (response.status.message != null)
                        result = new GetLabelResponseModel { Messages = response.status.message.ToList() };
                }
            }
            return result;
        }

        #endregion
    }
}