﻿using Microsoft.Extensions.Logging;

namespace ParcelrateUtilities
{
    public static class LogginManager
    {
        public static ILoggerFactory LoggerFactory { get; } = new LoggerFactory();
        public static ILogger CreateLogger<T>() =>
          LoggerFactory.CreateLogger<T>();
    }
}
