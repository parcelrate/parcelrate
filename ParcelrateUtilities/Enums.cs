﻿namespace ParcelrateUtilities
{
    public class Enums
    {
        public enum ShipmentStatus
        {
            bookingInProgress,

            bookingFinished,

            locked,

            cancelRequested,

            cancelled,

            notFound

        }        

        public enum ServiceSortOption
        {
            Price_High_To_Low,

            Price_Low_To_High,

            Available_Services_First,

            Available_Services_Last

        }

        public enum ContactMeForOption
        {
            Account_set_up,
            Demonstration,
            Carrier_Integration,
            Other
        }
    }
}
