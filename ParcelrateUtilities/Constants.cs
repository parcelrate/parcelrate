﻿
namespace ParcelrateUtilities
{
    public static class Constants
    {
        #region Doubles/Int/Long
        public const int SplashScreenTime = 1000;
        public const long MaxResponseContentBufferSize = int.MaxValue;
        public const int PageSize = 6;
        public const int AddressPageSize = 10;
        public const int DefaultId = 0;
        #endregion

        public const string TemplateFolder = @"\templates\";
        public const string ViewModelLog = "View Model : ";
        public const string MethodLog = " , Method : ";
        public const string ErrorLog = " , Error : ";
        public const string Token = "Token";
        public const string EmailValidationRegex = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|" + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)" + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";
        public const string DigitValidationRegex = @"^[0-9]*$";
        public const string DecimalValidationRegex = @"^[0-9\.]+$";
        public const string AlphabetWithSpaceRegex = "[a-zA-Z ]";
        public const string ForwardSlash = "/";
        public const string PhonePrefix = "PhonePrefix";
        public const string ContentTypeValue = "application/json";
        public const string DefaultImagePath = @"..\Assets\Images\";
        public const string CarrierLogoPath = @"\images\";
        public const string PdfPath = @"\App_Data\pdf";
        public const string DefaultCarrierLogo = "default-logo.png";
        public const string DefaultDateFormat = "dd MMM yyyy";
        public const string AllPackaDateFormat = "yyyy-MM-dd";
        public const string DefaultExtension = "csv";
        public const string FileFilter = "CSV files (*.csv)|*.csv";
        public const string CsvFileExtension = ".csv";
        public const string DocumentReadyStatus = "documentready";
        public const string MessageBoxStyle = "messageBoxStyle";
        public const string AddressId = "AddressId";
        public const string LetMeShipText = "letmeship";
        public const string AllPackaText = "allpacka";
        public const string DeviceID = "DeviceID";
        public const string Caption = "Caption";
        public const string ShipmentID = "ShipmentID";
        public const string SampleCSVFileName = "Sample.csv";
        public const string EmailTemplate = "EmailTemplate.";
        public const string LetMeShipPdfFolder = @"\LetMeShip";
        public const string AllPackaPdfFolder = @"\AllPacka";
        public const string PdfFileExtension = ".pdf";
        public const string BedalText = "Bedal";
        public const string ParcelText = "PARCEL";
        public const string DocumentText = "DOCUMENT";
        public const string BedalSwf = "Bedal.swf";
        public const string P4DText = "P4D";
        public const string BookedLabelNotGeneratedStatus = "bookingInProgress";
        public const string BookedLabelCompleteStatus = "Booked (Complete)";
        public const string ManualText = "Manually";
        public const string Culture = "Culture";
        public const string English = "English";
        public const string German = "German";
        public const string BusinessText = "Business";
        public const string DaysText = "days";
        public const string DayText = "day";
        public const string HtmlExtension = ".html";
        public const string TaskName = "ShipmentPollingWeb";
        public const string PollingSchedulerName = "ParcelratePollingScheduler.dll";
        public const string ShipmentCancelledStatus = "cancelled";
        public const string ShipmentInvoicedStatus = "locked";
        public const string IsLetMeShip = "IsLetMeShip";
        public const string ShipmentDefaultSortExpression = "-OrderBookedOn";
        public const string EXCEL_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        public const string SHIPMENTS = "Shipments";
        public const string EXCEL_EXTENSION_1 = ".xlsx";
        public const string TIME_STEMP = "ddMMyyyyHHmmss";
        public const string AddressDefaultSortExpression = "-Id";
        public const string Filter = "filter";
        public const string EnquiryEmailTemplate = "EnquiryTemplate";
        public const string EnquirySubject = "Parcelrate - New user enquiry";


        public static class Smtp
        {
            public const string Host = "smtp.gmail.com";
            public const int Port = 25;
            public const string SmtpUserName = "nsitesting.parcelrate@gmail.com";
            public const string SmtpPassword = "netsolutions";
            public const string EmailFrom = "nsitesting.parcelrate@gmail.com";
        }

        public static class PropertiesName
        {
            public const string IsPortSelectionEnabled = "IsPortSelectionEnabled";
            public const string IsBedalConnected = "IsBedalConnected";
            public const string BedalConnectButtonText = "BedalConnectButtonText";
            public const string BedalNotConnectedVisibility = "BedalNotConnectedVisibility";
            public const string BedalConnectedVisibility = "BedalConnectedVisibility";
            public const string IsInternetNotConnected = "IsInternetNotConnected";
            public const string IsMessageShow = "IsMessageShow";
            public const string IsShowLoader = "IsShowLoader";
            public const string Email = "Email";
            public const string Password = "Password";
            public const string EmailErrorMessage = "EmailErrorMessage";
            public const string EmailErrorMessageVisibility = "EmailErrorMessageVisibility";
            public const string PasswordErrorMessage = "PasswordErrorMessage";
            public const string PasswordErrorMessageVisibility = "PasswordErrorMessageVisibility";
            public const string EmailBorderBrush = "EmailBorderBrush";
            public const string PasswordBorderBrush = "PasswordBorderBrush";
            public const string IsEmailFocused = "IsEmailFocused";
            public const string IsPasswordFocused = "IsPasswordFocused";
            public const string ServiceProviderList = "ServiceProviderList";
            public const string CountryList = "CountryList";
            public const string SelectedServiceProvider = "SelectedServiceProvider";
            public const string SelectedCountry = "SelectedCountry";
            public const string CountryErrorMessage = "CountryErrorMessage";
            public const string ServiceProviderErrorMessage = "ServiceProviderErrorMessage";
            public const string IsHidePassword = "IsHidePassword";
            public const string IsRememberChecked = "IsRememberChecked";
            public const string LeftColumnWidth = "LeftColumnWidth";
            public const string ShipmentList = "ShipmentList";
            public const string ProviderLogo = "ProviderLogo";
            public const string IsNewShipment = "IsNewShipment";
            public const string IsShipmentListing = "IsShipmentListing";
            public const string IsShipmentActive = "IsShipmentActive";
            public const string IsNewAddress = "IsNewAddress";
            public const string IsStepOne = "IsStepOne";
            public const string IsStepTwo = "IsStepTwo";
            public const string IsStepThree = "IsStepThree";
            public const string IsAddressListing = "IsAddressListing";
            public const string IsAddressActive = "IsAddressActive";
            public const string AddressList = "AddressList";
            public const string IsPrintLabel = "IsPrintLabel";
            public const string IsShipmentStepOne = "IsShipmentStepOne";
            public const string IsShipmentStepTwo = "IsShipmentStepTwo";
            public const string IsShipmentStepThree = "IsShipmentStepThree";
            public const string Weight = "Weight";
            public const string Height = "Height";
            public const string Length = "Length";
            public const string Width = "Width";
            public const string IsManualInputChecked = "IsManualInputChecked";
            public const string IsPrintLabelShow = "IsPrintLabelShow";
            public const string IsLabelPopupShow = "IsLabelPopupShow";
            public const string ProviderID = "ProviderID";
            public const string Username = "Username";
            public const string CountryId = "CountryId";
            public const string CountryCode = "CountryCode";
        }

        public static class Urls
        {
            public const string GoogleUrl = "google.com";
        }

        public static class Messages
        {
            public const string InternetConnectionMessageError = "Internet connection not found. Please check ";
            public const string FileCopyError = "Application encountered error while copying copying files to AppData";
            public const string ShipmentStepOne = "Shipment Pickup and Delivery Addresses screen";
            public const string ShipmentStepTwo = "Shipment Product Dimensions manually/using Bedal screen";
            public const string ShipmentStepThree = "Select any carrier screen";
            public const string BuyService = "Clicked Buy this Service Button";
            public const string ScaleValueError = "Scale value error (10% max weight), please remove parcels placed on Bedal";
            public const string ParcelWeightError = "The parcel has exceeded maximum measurable weight";
            public const string ResetScaleError = "Scale error, kindly reset scale";
            public const string ScaleWeightError = "Scale value error (2% max weight), please remove parcels placed on Bedal";
            public const string ScaleZeroError = "Scale 0 error";
            public const string ScaleCellError = "Scale load cell error, scale is returning negative (-) values";
            public const string ExceptionalError = "Exceptional error thrown from Bedal";
            public const string NotReturnResponseError = "Bedal is not returning any response, kindly verify if the Bedal is connected";
            public const string MotorOpenError = "Bedal returned stepping motor open error";
            public const string MotorCloseError = "Bedal returned stepping motor close error";
            public const string DimensionExceedError = "Please ensure the parcel is more than 43cm, close extension bar if not needed";
            public const string SensorError = "Sensor error 1 to 9";
            public const string UltraSonicError = "Ultra sonic error 1 to 7";
            public const string UltraSonicSizeExceedError = "Ultra sonic size exceeded 1 to 7";
            public const string DataTransmitError = "Error in data transmission";
            public const string DisconnectError = "Error while disconnecting Bedal";
            public const string DataReceivedInfo = "Response received from Bedal, response = {0}";
            public const string BedalResponseInfo = "Response received from Bedal, response code = {0}, response = {1}";
            public const string BedalNotConnectInfo = "BedalConnect() --> Port = {0} & Bedal is not on this port";
            public const string BedalConnectStatus = "BedalConnect() --> Port = {0} & Bedal Connectivity Status = {1}";
            public const string BedalDisconnected = "BedalConnect() --> Bedal disconnected";
            public const string BedalDimensionsInfo = "Bedal Dimensions => Height : {0}, Width: {1}, Length: {2}, Weight {3}";
            public const string ShipmentStatusSchedulerStartInfo = "Polling Scheduler Start";
            public const string ShipmentStatusSchedulerEndInfo = "Polling Scheduler End";
            public const string NoShipmentFoundInfo = "No Shipment Status Found";
            public const string GetShipmentsStartInfo = "Get LetMeShip Shipments Start";
            public const string GetShipmentsEndInfo = "Get LetMeShip Shipments End";
            public const string GetShipmentsStatusRequestStartInfo = "Get Shipments Status Request Start";
            public const string GetShipmentsStatusRequestEndInfo = "Get Shipments Status Request End";
            public const string ServerErrorInfo = "Service not responding, please check";
            public const string NoShipmentPendingLabelFoundInfo = "No shipment found with pending label";
            public const string ErrorInfo = "We are unable to process your request at this moment. Please try again later";
            public const string GetShipmentsLabelsStartInfo = "Get Shipment Labels Start";
            public const string GetShipmentsLabelsEndInfo = "Get Shipment Labels End";
            public const string ShipmentLabelReadyInfo = "Shipment {0} Label ready";
            public const string GetShipmentLabelStartInfo = "Get Labels Request for shipment {0} Start";
            public const string GetShipmentLabelEndInfo = "Get Labels for shipment {0} Request End";
            public const string PollingExeNotExistInfo = "Polling scheduler file not found";
            public const string NoUserFound = "No user found";
        }

        public static class ViewModels
        {
            public const string LoginViewModel = "LoginViewModel";
            public const string HttpManager = "HttpManager";
            public const string App = "App";
            public const string Common = "Common";
            public const string ShipmentViewModel = "ShipmentViewModel";
            public const string AddressViewModel = "AddressViewModel";
            public const string AddressComponent = "AddressComponent";
            public const string ShipmentComponent = "ShipmentComponent";
            public const string NewShipmentViewModel = "NewShipmentViewModel";
            public const string ShipmentListingViewModel = "ShipmentListingViewModel";
            public const string AddressListingViewModel = "AddressListingViewModel";
            public const string AvailableServices = "AvailableServices";
            public const string LabelServices = "LabelServices";
            public const string ShipmentServices = "ShipmentServices";
            public const string StaticFunctions = "StaticFunctions";
            public const string StatisticsViewModel = "StatisticsViewModel";
            public const string TrackingServices = "TrackingServices";
            public const string CarrierComponent = "CarrierComponent";
            public const string ParcelrateShipmentStateScheduler = "ParcelrateShipmentStateScheduler";
        }

        public static class MethodName
        {
            public const string LoginViewModel = "LoginViewModel";
            public const string Post = "Post";
            public const string Get = "Get";
            public const string Put = "Put";
            public const string ProcessRequest = "ProcessRequest";
            public const string OnDispatcherUnhandledException = "OnDispatcherUnhandledException";
            public const string LoginClick = "LoginClick";
            public const string GetServiceProviderList = "GetServiceProviderList";
            public const string GetCountryListByServiceProvider = "GetCountryListByServiceProvider";
            public const string GetCountryList = "GetCountryList";
            public const string BindServiceProviderList = "BindServiceProviderList";
            public const string BindCountryList = "BindCountryList";
            public const string UploadCSV = "UploadCSV";
            public const string AddAddresses = "AddAddresses";
            public const string AddAddress = "AddAddress";
            public const string GetAddressCustomerList = "GetAddressCustomerList";
            public const string AddUpdateShipment = "AddUpdateShipment";
            public const string CreateShipment = "CreateShipment";
            public const string BuyServiceClick = "BuyServiceClick";
            public const string UpdateShipmentLabelStatus = "UpdateShipmentLabelStatus";
            public const string ProcessShipment = "ProcessShipment";
            public const string GetLMSLabel = "GetLMSLabel";
            public const string GetLetMeShipCarriers = "GetLetMeShipCarriers";
            public const string GetAllPackaCarriers = "GetAllPackaCarriers";
            public const string BedalDataReceivedProcessResponse = "BedalDataReceivedProcessResponse";
            public const string SendEmail = "SendEmail";
            public const string DeleteAddress = "DeleteAddress";
            public const string BindAddressCustomerList = "BindAddressCustomerList";
            public const string GetAllPackaLabel = "GetAllPackaLabel";
            public const string FillAddress = "FillAddress";
            public const string SendLabelAsMail = "SendLabelAsMail";
            public const string ViewLabel = "ViewLabel";
            public const string LoadData = "LoadData";
            public const string LoadUserInfo = "LoadUserInfo";
            public const string BedalConnect = "BedalConnect";
            public const string ShowStepTwo = "ShowStepTwo";
            public const string SetDeliveryAdddress = "SetDeliveryAdddress";
            public const string UpdateLabelStatus = "UpdateLabelStatus";
            public const string sendServiceRequest = "sendServiceRequest";
            public const string IsModelValid = "IsModelValid";
            public const string BedalDataReceived = "BedalDataReceived";
            public const string IsPDFHeader = "IsPDFHeader";
            public const string Constructor = "Constructor";
            public const string GetShipments = "GetShipments";
            public const string GetShipmentsCountByCountry = "GetShipmentsCountByCountry";
            public const string GetShipmentsPriceByCountry = "GetShipmentsPriceByCountry";
            public const string GetShipmentsPriceByCarrier = "GetShipmentsPriceByCarrier";
            public const string GetShipmentsOverOneYear = "GetShipmentsOverOneYear";
            public const string GetTrackingDetails = "GetTrackingDetails";
            public const string GetShipmentByID = "GetShipmentByID";
            public const string GetCarrierIDByName = "GetCarrierIDByName";
            public const string GetCountries = "GetCountries";
            public const string BindPickUpFromTimeRange = "BindPickUpFromTimeRange";
            public const string BindPickUpToTimeRange = "BindPickUpToTimeRange";
            public const string GetShipmentsStatus = "GetShipmentsStatus";
            public const string GetLetMeShipShipmentIDs = "GetLetMeShipShipmentIDs";
            public const string GetShipmentLabels = "GetShipmentLabels";
            public const string GetLabel = "GetLabel";
            public const string GetShipmentStatus = "GetShipmentStatus";
            public const string GetShipmentsCountByCarrier = "GetShipmentsCountByCarrier";
            public const string UpdateShipmentAWBNumber = "UpdateShipmentAWBNumber";
            public const string GetProvidersCountryList = "GetProvidersCountryList";
            public const string GetShipmentListPriceByCountry = "GetShipmentListPriceByCountry";
            public const string GetShipmentsListPriceByCarrier = "GetShipmentsListPriceByCarrier";
            public const string GetShipmentsListOverOneYear = "GetShipmentsListOverOneYear";
            public const string GetShipmentsListCountByCarrier = "GetShipmentsListCountByCarrier";
            public const string DownloadShipmentListOrdersByCarrier = "DownloadShipmentListOrdersByCarrier";
            public const string DownloadShipmentListPriceByCountry = "DownloadShipmentListPriceByCountry";
            public const string DownloadShipmentListPriceByCarrier = "DownloadShipmentListPriceByCarrier";
            public const string DownloadShipmentListOverYear = "DownloadShipmentListOverYear";
            public const string DownloadFile = "DownloadFile";
        }

        public static class ControllerName
        {
            public const string HomeController = "HomeController";
            public const string ShipmentController = "ShipmentController";
        }
    }
}
