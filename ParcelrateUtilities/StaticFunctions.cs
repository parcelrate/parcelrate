﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net.NetworkInformation;
using System.Reflection;

namespace ParcelrateUtilities
{
    public static class StaticFunctions
    {
        /// <summary>
        /// Get application root path
        /// </summary>
        /// <returns></returns>
        public static string GetRootPath()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }

        /// <summary>
        /// Get assembly execution path
        /// </summary>
        /// <returns></returns>
        public static string GetAssemblyExecutionPath()
        {
            return string.Concat(Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath), "\\");
        }

        /// <summary>
        /// Check internet connection
        /// </summary>
        /// <returns></returns>
        public static bool IsInternetConnected()
        {
            try
            {
                Ping ping = new Ping();
                String host = Constants.Urls.GoogleUrl;
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = ping.Send(host, timeout, buffer, pingOptions);
                return (reply.Status == IPStatus.Success);
            }
            catch (Exception)
            {
                return false;
            }
        }


        public static string LogErrorMessage(string ViewModelName, string MethodName, string ErrorMessage)
        {
            return Constants.ViewModelLog + ViewModelName + Constants.MethodLog + MethodName + Constants.ErrorLog + ErrorMessage + Environment.NewLine;
        }

        public static string LogErrorMessage(string ViewModelName, string MethodName, Exception ex)
        {

            return Constants.ViewModelLog + ViewModelName + Constants.MethodLog + MethodName + Constants.ErrorLog + GetExceptionString(ex) + Environment.NewLine;
        }

        public static string LogErrorMessage(string ViewModelName, string MethodName, IOException ex)
        {
            return Constants.ViewModelLog + ViewModelName + Constants.MethodLog + MethodName + Constants.ErrorLog + GetExceptionString(ex) + Environment.NewLine;
        }

        private static string GetExceptionString(Object ex)
        {
            PropertyInfo[] properties = ex.GetType()
                            .GetProperties();
            List<string> fields = new List<string>();
            foreach (PropertyInfo property in properties)
            {
                object value = property.GetValue(ex, null);
                fields.Add(String.Format(
                                 "{0} = {1}",
                                 property.Name,
                                 value != null ? value.ToString() : String.Empty
                ));
            }
            return String.Join("\n", fields.ToArray());
        }

        /// <summary>
        /// Validate File Format
        /// </summary>
        /// <param name="fileName">fileName</param>
        /// <returns></returns>
        public static bool ValidateFileFormat(string fileName)
        {
            string[] extList = Constants.CsvFileExtension.Split(',');
            foreach (string ext in extList)
            {
                string fileNameInSmallLetters = fileName;
                if (fileNameInSmallLetters.ToLower(System.Globalization.CultureInfo.InvariantCulture).Contains(ext))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Method To convert csv to datatable
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static DataTable ReadCsvFile(Stream stream)
        {
            DataTable dtCsv = new DataTable();
            string fulltext;
            if (stream.Length > 0)
            {
                using (var sr = new StreamReader(stream))
                {
                    while (!sr.EndOfStream)
                    {
                        fulltext = sr.ReadToEnd(); //read full file text  
                        string[] rows = fulltext.Split('\n'); //split full file text into rows  
                        dtCsv = ConvertCSVToDataTable(rows);
                    }
                }
            }
            return dtCsv;
        }

        private static DataTable ConvertCSVToDataTable(string[] rows)
        {
            DataTable dtCsv = new DataTable();
            for (int i = 0; i <= rows.Length - 1; i++)
            {
                if (!String.IsNullOrEmpty(rows[i]))
                {
                    string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  
                    {
                        if (i == 0)
                        {
                            for (int j = 0; j < rowValues.Length; j++)
                            {
                                dtCsv.Columns.Add(rowValues[j].Trim()); //add headers  
                            }
                        }
                        else
                        {
                            DataRow dr = dtCsv.NewRow();
                            for (int k = 0; k < rowValues.Length; k++)
                            {
                                dr[k] = rowValues[k].Trim();
                            }
                            dtCsv.Rows.Add(dr); //add other rows  
                        }
                    }
                }
            }
            return dtCsv;
        }

        public static bool IsPDFHeader(string fileName)
        {
            byte[] buffer = null;
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);

            buffer = br.ReadBytes(5);

            var enc = new System.Text.ASCIIEncoding();
            var header = enc.GetString(buffer);
            fs.Dispose();
            //%PDF−1.0
            // If you are loading it into a long, this is (0x04034b50).
            if (buffer[0] == 0x25 && buffer[1] == 0x50
                && buffer[2] == 0x44 && buffer[3] == 0x46)
            {
                return header.StartsWith("%PDF-");
            }
            return false;
        }

        public static string DecodeString(string encodedString)
        {
            return encodedString;
            //System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            //System.Text.Decoder utf8Decode = encoder.GetDecoder();
            //byte[] todecode_byte = Convert.FromBase64String(encodedString);
            //int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            //char[] decoded_char = new char[charCount];
            //utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            //string decodedString = new String(decoded_char);
            //return decodedString;
        }

        public static string EncodeString(string decodedString)
        {
            return decodedString;
            //byte[] encDataByte = new byte[decodedString.Length];
            //encDataByte = System.Text.Encoding.UTF8.GetBytes(decodedString);
            //string encodedPassword = Convert.ToBase64String(encDataByte);
            //return encodedPassword;
        }

        public static string DisplayOption(this Enums.ServiceSortOption sortOption)
        {
            string displayName = string.Empty;
            switch (sortOption)
            {
                case Enums.ServiceSortOption.Price_High_To_Low:
                    displayName = ParcelrateResources.Resources.Price_High_To_Low;
                    break;
                case Enums.ServiceSortOption.Price_Low_To_High:
                    displayName = ParcelrateResources.Resources.Price_Low_To_High;
                    break;
                case Enums.ServiceSortOption.Available_Services_First:
                    displayName = ParcelrateResources.Resources.Available_Services_First;
                    break;
                case Enums.ServiceSortOption.Available_Services_Last:
                    displayName = ParcelrateResources.Resources.Available_Services_Last;
                    break;
                default:
                    break;
            }
            return displayName;
        }

        public static string DisplayOption(this Enums.ContactMeForOption sortOption)
        {
            string displayName = string.Empty;
            switch (sortOption)
            {
                case Enums.ContactMeForOption.Account_set_up:
                    displayName = ParcelrateResources.Resources.Account_set_up;
                    break;
                case Enums.ContactMeForOption.Carrier_Integration:
                    displayName = ParcelrateResources.Resources.Carrier_Integration;
                    break;
                case Enums.ContactMeForOption.Demonstration:
                    displayName = ParcelrateResources.Resources.Demonstration;
                    break;
                case Enums.ContactMeForOption.Other:
                    displayName = ParcelrateResources.Resources.Other;
                    break;
                default:
                    break;
            }
            return displayName;
        }
    }
}