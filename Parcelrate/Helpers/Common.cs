using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ParcelrateComponent.Interfaces;
using ParcelrateEntities;
using ParcelrateUtilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;

namespace Parcelrate.Helpers
{
    public class Common
    {
        private readonly IHostingEnvironment _env;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILogger<Common> _logger;
        private readonly IUserComponent _userComponent;
        private readonly IShipmentComponent _shipmentComponent;
		private readonly IConfiguration _configuration;

        public Common(IHostingEnvironment env, IHttpContextAccessor httpContextAccesor, ILogger<Common> logger, IUserComponent userComponent, IShipmentComponent shipmentComponent, IConfiguration configuration)
        {
            _env = env;
            _httpContextAccessor = httpContextAccesor;
            _logger = logger;
            _userComponent = userComponent;
            _shipmentComponent = shipmentComponent;
			_configuration = configuration;
        }
        public void SetCulture(string culture)
        {
            var cultureInfo = new CultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = cultureInfo;
            Thread.CurrentThread.CurrentCulture = cultureInfo;
            ParcelrateResources.Resources.Culture = cultureInfo;
            Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = ".";
            Thread.CurrentThread.CurrentCulture.NumberFormat.NumberGroupSeparator = ",";
        }

        /// <summary>
        /// get time range values list based on start and end time
        /// </summary>
        public List<PickupTime> GetTimeRangeValueList(TimeSpan startTime, TimeSpan endTime, TimeSpan interval)
        {
            var timeList = new List<PickupTime>();
            for (TimeSpan tsLoop = startTime; tsLoop <= endTime; tsLoop = tsLoop.Add(interval))
            {
                var defaultDate = DateTime.MinValue.Add(tsLoop);
                timeList.Add(new PickupTime { Caption = defaultDate.ToString("HH:mm", CultureInfo.InvariantCulture), Value = defaultDate });
            }
            return timeList;
        }

        public string GetPDFPath(string shipmentID, bool isLetMeShip, string userName)
        {
            var user = _userComponent.GetUserByUsername(userName);
            if (user != null)
            {
                string filePath = _env.ContentRootPath + string.Concat(Constants.PdfPath + @"\" + user.ID, isLetMeShip ? Constants.LetMeShipPdfFolder : Constants.AllPackaPdfFolder);
                if (!Directory.Exists(filePath))
                    Directory.CreateDirectory(filePath);
                return filePath + @"\" + shipmentID + Constants.PdfFileExtension;
            }
            return string.Empty;
        }

        public string GetTemplatePath()
        {
            string culture = _httpContextAccessor?.HttpContext?.User?.Claims?.FirstOrDefault(x => x.Type == Constants.Culture)?.Value ?? "en-US";
            string filePath = string.Concat(Constants.TemplateFolder + Constants.EmailTemplate + culture + Constants.HtmlExtension);
            return _env.WebRootPath + filePath;
        }

        public string GetWebRootPath()
        {
            return _env.WebRootPath;
        }

        public Dictionary<string, string> GetLanguageList()
        {
            return new Dictionary<string, string>() { { "en-US", "English" }, { "de-DE", "German" } };
        }

        /// <summary>
        /// Common method to write excel file.
        /// </summary>
        /// <param name="shipmentList"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public Byte[] WriteShipmentsExcel(List<ParcelrateEntities.Models.Shipment> shipmentList, string fileName)
        {
            int colIndex = 1;
            Byte[] bytes = null;
            try
            {
                using (ExcelPackage xlPackage = new ExcelPackage())
                {
                    ExcelWorksheet retailerSheet = xlPackage.Workbook.Worksheets.Add(fileName);
                    List<string> lstColumns = new List<string> { ParcelrateResources.Resources.Carrier, ParcelrateResources.Resources.Service, ParcelrateResources.Resources.Shipment_ID, ParcelrateResources.Resources.Price, ParcelrateResources.Resources.Pickup_Date, ParcelrateResources.Resources.Booked_On, ParcelrateResources.Resources.AWB_Number, ParcelrateResources.Resources.Dimensions_Captured, ParcelrateResources.Resources.Status, ParcelrateResources.Resources.Ship_From, ParcelrateResources.Resources.Ship_To };
                    foreach (var retailerTemplateSheetColumn in lstColumns)
                    {
                        retailerSheet.Cells[1, colIndex].Value = retailerTemplateSheetColumn;
                        colIndex = colIndex + 1;
                    }

                    if (shipmentList != null)
                    {
                        for (Int32 i = 0; i < shipmentList.Count; i++)
                        {
                            retailerSheet.Cells[i + 2, 1].Value = shipmentList[i].Carrier;
                            retailerSheet.Cells[i + 2, 2].Value = shipmentList[i].Service;
                            retailerSheet.Cells[i + 2, 3].Value = shipmentList[i].ShipmentId;
                            retailerSheet.Cells[i + 2, 4].Value = shipmentList[i].TotalPrice + " " + shipmentList[i].PickupCountry?.Currency;
                            retailerSheet.Cells[i + 2, 5].Value = Convert.ToDateTime(shipmentList[i].PickUpDate).ToString(Constants.DefaultDateFormat);
                            retailerSheet.Cells[i + 2, 6].Value = shipmentList[i].OrderBookedOn.ToString(Constants.DefaultDateFormat);
                            retailerSheet.Cells[i + 2, 7].Value = shipmentList[i].Awbnumber;
                            retailerSheet.Cells[i + 2, 8].Value = shipmentList[i].DimensionsCaptured == Constants.ManualText ? ParcelrateResources.Resources.Manually : ParcelrateResources.Resources.Bedal;
                            retailerSheet.Cells[i + 2, 9].Value = _shipmentComponent.GetShipmentStatus(shipmentList[i].Status);
                            retailerSheet.Cells[i + 2, 10].Value = shipmentList[i].PickupCountry?.Name;
                            retailerSheet.Cells[i + 2, 11].Value = shipmentList[i].DeliveryCountry?.Name;
                        }
                    }

                    retailerSheet.Cells.AutoFitColumns();
                    retailerSheet.Row(1).Height = 40;
                    retailerSheet.Row(1).Style.Font.Color.SetColor(Color.Black);
                    retailerSheet.Row(1).Style.Font.Bold = true;
                    retailerSheet.Row(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    var ms = new System.IO.MemoryStream();
                    xlPackage.SaveAs(ms);
                    bytes = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ControllerName.ShipmentController, Constants.MethodName.DownloadFile, ex));
            }
            return bytes;
        }

        public Dictionary<string, string> GetSortOptionList()
        {
            var items = new Dictionary<string, string>();
            foreach (var name in Enum.GetNames(typeof(Enums.ServiceSortOption)))
            {
                Enums.ServiceSortOption myEnum = (Enums.ServiceSortOption)Enum.Parse(typeof(Enums.ServiceSortOption), name);
                items.Add(name, myEnum.DisplayOption());
            }
            return items;
        }

        public Dictionary<string, string> GetContactMeForList()
        {
            var items = new Dictionary<string, string>();
            foreach (var name in Enum.GetNames(typeof(Enums.ContactMeForOption)))
            {
                Enums.ContactMeForOption myEnum = (Enums.ContactMeForOption)Enum.Parse(typeof(Enums.ContactMeForOption), name);
                items.Add(name, myEnum.DisplayOption());
            }
            return items;
        }

        public bool SendSMTPMail(string toEmail, string subject, string body, Attachment attachment)
        {
            Boolean result = true;
            try
            {
                MailMessage mail = new MailMessage(_configuration.GetSection("SMTPSettings:EmailFrom").Value, toEmail, subject, body);
                System.Net.NetworkCredential auth = new System.Net.NetworkCredential(_configuration.GetSection("SMTPSettings:SmtpUserName").Value, _configuration.GetSection("SMTPSettings:SmtpPassword").Value);
                SmtpClient client = new SmtpClient(_configuration.GetSection("SMTPSettings:Host").Value, Convert.ToInt32(_configuration.GetSection("SMTPSettings:Port").Value));
                if (attachment != null)
                {
                    mail.Attachments.Add(attachment);
                }
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                mail.BodyEncoding = UTF8Encoding.UTF8;
                mail.IsBodyHtml = true;
                client.Credentials = auth;
                client.Send(mail);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ControllerName.ShipmentController, Constants.MethodName.DownloadFile, ex));
                result = false;
            }
            return result;
        }
    }
}