﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Config;
using NLog.Extensions.Logging;
using NLog.Web;
using Parcelrate.Helpers;
using ParcelrateComponent;
using ParcelrateComponent.Interfaces;
using ParcelrateDataAccess;
using ParcelrateDataAccess.Interfaces;
using ParcelrateEntities.Models;
using ParcelrateUtilities;
using ReflectionIT.Mvc.Paging;
using System;

namespace Parcelrate
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            env.ConfigureNLog(env.ContentRootPath + "\\nlog.config");
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ParcelrateDbEntities>(options =>
                     options.UseSqlServer(Configuration.GetConnectionString("ParcelrateDbEntities")));



            services.AddAuthentication(options =>
            {
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;

            }).AddCookie(options =>
               {
                   // Cookie settings
                   options.Cookie.HttpOnly = true;
                   options.Cookie.Expiration = new TimeSpan(0, 15, 0);
                   options.LoginPath = "/Account/Login";
                   options.LogoutPath = "/Account/Logout";
                   options.AccessDeniedPath = "/Account/Login";
                   options.SlidingExpiration = true;
                   options.ExpireTimeSpan = new TimeSpan(0, 15, 0);
               });
            
            services.AddAutoMapper();
            services.AddPaging();
            services.AddSession();

            services.AddMvc();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUserDataAccess, UserDataAccess>();
            services.AddScoped<IAddressDataAccess, AddressDataAccess>();
            services.AddScoped<ICountryDataAccess, CountryDataAccess>();
            services.AddScoped<ICarrierDataAccess, CarrierDataAccess>();
            services.AddScoped<IServiceProviderDataAccess, ServiceProviderDataAccess>();
            services.AddScoped<IShipmentDataAccess, ShipmentDataAccess>();
            services.AddScoped<IUserComponent, UserComponent>();
            services.AddScoped<IAddressComponent, AddressComponent>();
            services.AddScoped<ICountryComponent, CountryComponent>();
            services.AddScoped<ICarrierComponent, CarrierComponent>();
            services.AddScoped<IServiceProviderComponent, ServiceProviderComponent>();
            services.AddScoped<IShipmentComponent, ShipmentComponent>();
            services.AddScoped<LetMeShip.Interfaces.IAvailableServices, LetMeShip.AvailableServices>();
            services.AddScoped<LetMeShip.Interfaces.IShipmentServices, LetMeShip.ShipmentServices>();
            services.AddScoped<LetMeShip.Interfaces.ILabelServices, LetMeShip.LabelServices>();
            services.AddScoped<LetMeShip.Interfaces.ITrackingServices, LetMeShip.TrackingServices>();
            services.AddScoped<LetMeShip.Interfaces.IServices, LetMeShip.Services>();
            services.AddScoped<AllPacka.Interfaces.IServices, AllPacka.Services>();
            services.AddScoped<AllPacka.Interfaces.IAvailableServices, AllPacka.AvailableServices>();
            services.AddScoped<AllPacka.Interfaces.IShipmentServices, AllPacka.ShipmentServices>();
            services.AddTransient<Common>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddNLog();

            var logger = loggerFactory.CreateLogger(typeof(Startup));
            NLog.LogManager.Configuration = new XmlLoggingConfiguration(env.ContentRootPath + "\\nlog.config", true);

            if (Convert.ToBoolean(Configuration.GetSection("IsDBLoggingEnabled").Value))
                NLog.LogManager.Configuration.Variables["connectionString"] = Configuration.GetConnectionString("ParcelrateDbEntities");
            else
                NLog.LogManager.Configuration.Variables["configDir"] = env.ContentRootPath + "\\logs\\" + DateTime.UtcNow.ToString("MMM-dd-yyyy");

            //add NLog.Web
            app.AddNLogWeb();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseExceptionHandler("/Home/Error");
                app.UseDeveloperExceptionPage();
            }
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseSession();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
