using AllPacka.DataContainer.Response;
using AutoMapper;
using LetMeShip.DataContainer.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using Parcelrate.Helpers;
using Parcelrate.Models;
using ParcelrateComponent.Interfaces;
using ParcelrateEntities;
using ParcelrateUtilities;
using ReflectionIT.Mvc.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Parcelrate.Controllers
{
    [CustomAuthorize]
    public class ShipmentController : Controller
    {
        private readonly IShipmentComponent _shipmentComponent;
        private readonly ICountryComponent _countryComponent;
        private readonly IAddressComponent _addressComponent;
        private readonly ICarrierComponent _carrierComponent;
        private readonly IUserComponent _userComponent;
        private readonly IMapper _mapper;
        private readonly ILogger<ShipmentController> _logger;

        private readonly IHttpContextAccessor _httpContextAccessor;
        private ClaimsPrincipal user => _httpContextAccessor.HttpContext.User;
        private readonly bool _isLetMeShip;
        private readonly long _providerID;
        private readonly Common _common;
        private readonly LetMeShip.Interfaces.IServices _lmsServices;
        private readonly AllPacka.Interfaces.IServices _allPackaServices;
        private string _currency;

        public ShipmentController(IHttpContextAccessor httpContextAccesor, IMapper mapper, Common common, LetMeShip.Interfaces.IServices lmsServices, AllPacka.Interfaces.IServices allPackaServices,
            IShipmentComponent shipmentComponent, ICountryComponent countryComponent, IAddressComponent addressComponent, ICarrierComponent carrierComponent, IUserComponent userComponent, ILogger<ShipmentController> logger)
        {
            _httpContextAccessor = httpContextAccesor;
            _mapper = mapper;
            _common = common;
            _lmsServices = lmsServices;
            _allPackaServices = allPackaServices;
            _shipmentComponent = shipmentComponent;
            _countryComponent = countryComponent;
            _addressComponent = addressComponent;
            _carrierComponent = carrierComponent;
            _userComponent = userComponent;
            _logger = logger;
            _isLetMeShip = Convert.ToBoolean(user.Claims.FirstOrDefault(x => x.Type == Constants.IsLetMeShip)?.Value);
            _providerID = Convert.ToInt64(user.Claims.FirstOrDefault(x => x.Type == Constants.PropertiesName.ProviderID)?.Value);
        }

        [HttpGet]
        public async Task<IActionResult> Index(int page = 1, string sortExpression = Constants.ShipmentDefaultSortExpression)
        {
            long providerID = Convert.ToInt64(User.Claims.FirstOrDefault(x => x.Type == Constants.PropertiesName.ProviderID)?.Value);
            var user = _userComponent.GetUserByUsername(User.Identity.Name);
            var qry = _shipmentComponent.GetShipmentsQuery(providerID, user.ID);
            var model = await PagingList.CreateAsync(qry, Constants.PageSize, page, sortExpression, Constants.ShipmentDefaultSortExpression);
            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var shipmentViewModel = new ShipmentDetailsViewModel();
            try
            {
                string culture = Convert.ToString(User.Claims.FirstOrDefault(x => x.Type == Constants.Culture)?.Value);
                ViewBag.CountryList = _countryComponent.GetCountries(culture);
                ViewBag.PickupTimeFromRange = BindPickUpFromTimeRange();
                ViewBag.PickupTimeToRange = BindPickUpToTimeRange();
                var user = _userComponent.GetUserByUsername(User.Identity.Name);
                ViewBag.CustomerList = _addressComponent.GetAddressCustomers(user.ID);
                var defaultAddress = _addressComponent.GetDefaultAddress(user.ID);
                if (defaultAddress != null)
                {
                    shipmentViewModel.ShippedFromFirstName = defaultAddress.FirstName;
                    shipmentViewModel.ShippedFromLastName = defaultAddress.LastName;
                    shipmentViewModel.ShippedFromAddress1 = defaultAddress.Address1;
                    shipmentViewModel.ShippedFromAddress2 = defaultAddress.Address2;
                    shipmentViewModel.ShippedFromCompany = defaultAddress.Company;
                    shipmentViewModel.ShippedFromPhoneNo = defaultAddress.PhoneNo;
                    shipmentViewModel.ShippedFromCity = defaultAddress.City;
                    shipmentViewModel.ShippedFromPostalCode = defaultAddress.PostalCode;
                    shipmentViewModel.ShippedFromEmail = defaultAddress.Email;
                    shipmentViewModel.IsPickupOrder = defaultAddress.IsPickupOrder;
                    shipmentViewModel.ShippedFromCountryId = defaultAddress.CountryId;
                    shipmentViewModel.ShippedFromCountryCode = defaultAddress.CountryCode;
                    shipmentViewModel.ShippedFromPhonePrefix = defaultAddress.PhonePrefix;
                    shipmentViewModel.IsDefaultAddressExist = true;
                    ViewBag.Currency = defaultAddress.CountryCurrency;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(ShipmentController), nameof(Create), ex));
            }
            return View("ShippingDetails", shipmentViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ShipmentDetailsViewModel shipmentViewModel)
        {
            try
            {
                string culture = Convert.ToString(User.Claims.FirstOrDefault(x => x.Type == Constants.Culture)?.Value);
                ViewBag.CountryList = _countryComponent.GetCountries(culture);
                ViewBag.PickupTimeFromRange = BindPickUpFromTimeRange();
                ViewBag.PickupTimeToRange = BindPickUpToTimeRange();
                var user = _userComponent.GetUserByUsername(User.Identity.Name);
                ViewBag.CustomerList = _addressComponent.GetAddressCustomers(user.ID);
                ModelState.Remove(nameof(shipmentViewModel.CustomerID));
                if (shipmentViewModel.PickupTimeFrom > shipmentViewModel.PickupTimeTo)
                    ModelState.AddModelError(nameof(shipmentViewModel.PickupTimeTo), ParcelrateResources.Resources.Valid_TimeRange);
                else if ((shipmentViewModel.PickupTimeTo - shipmentViewModel.PickupTimeFrom).TotalHours < 2)
                    ModelState.AddModelError(nameof(shipmentViewModel.PickupTimeTo), ParcelrateResources.Resources.Minimum_TimeFrame);
                if (!shipmentViewModel.IsAgreeTermsConditions)
                    ModelState.AddModelError(nameof(shipmentViewModel.IsAgreeTermsConditions), ParcelrateResources.Resources.Required_Agree);
                if (ModelState.IsValid)
                {
                    if (shipmentViewModel.IsSaveAsDefaultAddress)
                        SaveAddress(shipmentViewModel, true);
                    if (shipmentViewModel.IsSaveAddressToDb)
                        SaveAddress(shipmentViewModel, false);
                    return RedirectToAction(nameof(ShipmentController.ProductDimensions));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(ShipmentController), nameof(Create), ex));
            }
            return View("ShippingDetails", shipmentViewModel);
        }

        [HttpGet]
        public IActionResult ProductDimensions()
        {
            var shipmentViewModel = new ProductDimensionsViewModel();
            return View(shipmentViewModel);
        }

        [HttpPost]
        public IActionResult ProductDimensions(ProductDimensionsViewModel productDimensionsViewModel)
        {
            if (ModelState.IsValid)
                return RedirectToAction(nameof(SelectCarrier));
            return View(productDimensionsViewModel);
        }

        [HttpGet]
        public IActionResult SelectCarrier(List<CarrierEntity> carrierList)
        {
            ViewBag.SortOptionsList = _common.GetSortOptionList();
            return View(carrierList);
        }

        [HttpPost]
        public JsonResult GetAvailableCarriers(ShipmentRequestViewModel shipmentRequestModel, string sortOption)
        {
            try
            {
                if (StaticFunctions.IsInternetConnected())
                {
                    shipmentRequestModel.UserName = User.Identity.Name;
                    shipmentRequestModel.PassWord = User.Claims.FirstOrDefault(x => x.Type == Constants.PropertiesName.Password)?.Value;
                    _currency = _countryComponent.GetCountryByID(shipmentRequestModel.PickupAddress.CountryId)?.Currency;
                    if (_isLetMeShip)
                    {
                        var createShipmentModel = _mapper.Map<ShipmentRequestViewModel, LetMeShip.CreateShipmentRequestModel>(shipmentRequestModel);
                        var result = _lmsServices.GetAvailableServices(createShipmentModel);
                        if (result != null)
                        {
                            var carrierList = new List<CarrierEntity>();
                            if (result.Services != null && result.Services.Count > 0)
                            {
                                var services = result.Services.Select(c => new CarrierEntity
                                {
                                    Id = c.Id,
                                    Image = c.Image,
                                    Name = c.Name,
                                    ServiceName = c.ServiceName,
                                    Messages = c.Messages != null ? string.Join(Environment.NewLine + "- ", c.Messages.ToArray()) : null,
                                    CutoffTime = Convert.ToString(c.CutoffTime),
                                    CutoffTimeSpecified = c.CutoffTimeSpecified,
                                    Description = c.Description,
                                    TransferTime = c.TransferTime.Replace(Constants.BusinessText, ParcelrateResources.Resources.Business_Days).Replace(Constants.DaysText, string.Empty).Replace(Constants.DayText, string.Empty),
                                    LetMeShipPriceInfo = c.LetMeShipPriceInfo,
                                    FuelSurcharge = c.Surcharges != null ? c.Surcharges.Sum(x => x.Amount) : 0,
                                    IsLetMeShip = true,
                                    Currency = _currency,
                                    IsAvailable = c.Messages == null || c.Messages.Count == 0
                                });
                                if (!string.IsNullOrEmpty(sortOption))
                                    SortServices(sortOption, ref carrierList, services);
                                else
                                    carrierList = services.OrderByDescending(c => c.IsAvailable).ThenBy(c => c.LetMeShipPriceInfo?.TotalPrice).ToList();
                            }
                            else if (result.Messages != null && result.Messages.Count > 0)
                            {
                                result.Messages[0] = "- " + result.Messages[0];
                                return Json(new { IsSuccess = false, error = string.Join(Environment.NewLine + "- ", result.Messages.ToArray()) });
                            }
                            return Json(new { IsSuccess = true, data = carrierList });
                        }
                        else
                            return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.Error_Message_OnService });
                    }
                    else
                    {
                        var createShipmentModel = _mapper.Map<ShipmentRequestViewModel, AllPacka.CreateShipmentRequestModel>(shipmentRequestModel);
                        createShipmentModel.PickupDate = shipmentRequestModel.PickupInterval_Date.ToString(Constants.AllPackaDateFormat);
                        var result = _allPackaServices.GetAvailableServices(createShipmentModel);
                        if (result != null)
                        {
                            var carrierList = new List<CarrierEntity>();
                            if (result.Services != null && result.Services.Count > 0)
                            {
                                var services = result.Services.Select(c => new CarrierEntity
                                {
                                    Id = c.Id,
                                    ServiceId = c.ServiceId,
                                    Image = c.Image,
                                    Name = c.Name,
                                    ServiceName = c.ServiceName,
                                    Messages = c.Messages != null ? string.Join(Environment.NewLine + "- ", c.Messages.Select(x => x.Description).ToArray()) : null,
                                    CutoffTime = c.CutoffTime,
                                    CutoffTimeSpecified = c.CutoffTimeSpecified,
                                    Description = c.Description,
                                    TransferTime = c.TransferTime,
                                    PriceInfo = c.PriceInfo,
                                    Logo = GetCarrierLogo(c.Id),
                                    DoesProvideLabel = DoesCarrierProvideLabel(c.Id),
                                    Currency = _currency,
                                    IsAvailable = c.Messages == null || c.Messages.Count == 0
                                });
                                if (!string.IsNullOrEmpty(sortOption))
                                    SortServices(sortOption, ref carrierList, services);
                                else
                                    carrierList = services.OrderByDescending(c => c.IsAvailable).ThenBy(c => c.PriceInfo?.TotalPrice).ToList();
                            }
                            else if (result.Messages != null && result.Messages.Count > 0)
                            {
                                result.Messages[0].Text = "- " + result.Messages[0].Text;
                                return Json(new { IsSuccess = false, error = string.Join(Environment.NewLine + "- ", result.Messages.Select(x => x.Text).ToArray()) });
                            }
                            return Json(new { IsSuccess = true, data = carrierList });
                        }
                        return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.Error_Message_OnService });
                    }
                }
                else
                    return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.InternetConnection_Error });
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(ShipmentController), nameof(GetAvailableCarriers), ex));
                return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.Error_Message_OnService });
            }
        }

        private void SortServices(string sortOption, ref List<CarrierEntity> carrierList, IEnumerable<CarrierEntity> services)
        {
            Enums.ServiceSortOption selectedSortOption;
            if (Enum.TryParse(sortOption, out selectedSortOption))
            {
                switch (selectedSortOption)
                {
                    case Enums.ServiceSortOption.Price_High_To_Low:
                        carrierList = services.OrderByDescending(c => _isLetMeShip ? c.LetMeShipPriceInfo?.TotalPrice : c.PriceInfo?.TotalPrice).ToList();
                        break;
                    case Enums.ServiceSortOption.Price_Low_To_High:
                        carrierList = services.OrderBy(c => _isLetMeShip ? c.LetMeShipPriceInfo?.TotalPrice : c.PriceInfo?.TotalPrice).ToList();
                        break;
                    case Enums.ServiceSortOption.Available_Services_First:
                        carrierList = services.OrderByDescending(c => c.IsAvailable).ToList();
                        break;
                    case Enums.ServiceSortOption.Available_Services_Last:
                        carrierList = services.OrderBy(c => c.IsAvailable).ToList();
                        break;
                }
            }
            else
                carrierList = services.OrderByDescending(c => c.IsAvailable).ThenBy(c => _isLetMeShip ? c.LetMeShipPriceInfo?.TotalPrice : c.PriceInfo?.TotalPrice).ToList();
        }

        [HttpPost]
        public IActionResult CreateShipment(ShipmentRequestViewModel shipmentRequestModel)
        {
            try
            {
                if (StaticFunctions.IsInternetConnected())
                {
                    long shipmentID = 0;
                    dynamic shipmentResponse;
                    shipmentRequestModel.UserName = User.Identity.Name;
                    shipmentRequestModel.PassWord = User.Claims.FirstOrDefault(x => x.Type == Constants.PropertiesName.Password)?.Value;
                    _currency = _countryComponent.GetCountryByID(shipmentRequestModel.PickupAddress.CountryId)?.Currency;
                    if (_isLetMeShip)
                    {
                        var createShipmentModel = _mapper.Map<ShipmentRequestViewModel, LetMeShip.CreateShipmentRequestModel>(shipmentRequestModel);
                        shipmentResponse = _lmsServices.CreateShipment(createShipmentModel);
                        if (shipmentResponse != null)
                        {
                            var lmsShipmentRespose = shipmentResponse as LetMeShip.CreateShipmentResponseModel;
                            if (lmsShipmentRespose.Messages != null && lmsShipmentRespose.Messages.Count > 0)
                            {
                                lmsShipmentRespose.Messages[0] = "- " + lmsShipmentRespose.Messages[0];
                                return Json(new { IsSuccess = false, error = string.Join(Environment.NewLine + "- ", lmsShipmentRespose.Messages.ToArray()) });
                            }
                            else
                                shipmentID = lmsShipmentRespose.ShipmentId;
                        }
                        else
                            return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.Error_Message_OnService });
                    }
                    else
                    {
                        var createShipmentModel = _mapper.Map<ShipmentRequestViewModel, AllPacka.CreateShipmentRequestModel>(shipmentRequestModel);
                        createShipmentModel.CarrierId = shipmentRequestModel.SelectedService.Id;
                        createShipmentModel.ServiceId = shipmentRequestModel.SelectedService.ServiceId;
                        createShipmentModel.PickupDate = shipmentRequestModel.PickupInterval_Date.ToString(Constants.AllPackaDateFormat);
                        shipmentResponse = _allPackaServices.CreateShipment(createShipmentModel);
                        if (shipmentResponse != null)
                        {
                            var allPackaShipmentResponse = shipmentResponse as AllPacka.CreateShipmentResponseModel;
                            if (allPackaShipmentResponse.Messages != null && allPackaShipmentResponse.Messages.Count > 0)
                            {
                                allPackaShipmentResponse.Messages[0].Text = "- " + allPackaShipmentResponse.Messages[0].Text;
                                return Json(new { IsSuccess = false, error = string.Join(Environment.NewLine + "- ", allPackaShipmentResponse.Messages.Select(x => x.Text).ToArray()) });
                            }
                            else if (allPackaShipmentResponse.Quotes != null && allPackaShipmentResponse.Quotes.Count > 0)
                                shipmentID = allPackaShipmentResponse.Quotes[0].idQuote;
                        }
                        else
                            return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.Error_Message_OnService });
                    }
                    long carrierID = _carrierComponent.GetCarrierIDByName(shipmentRequestModel.SelectedService.Name);
                    shipmentRequestModel.SelectedService.Id = (int)carrierID;
                    long id = AddUpdateShipment(shipmentRequestModel, shipmentResponse, shipmentID);
                    if (id > 0)
                        return Json(new { IsSuccess = true, NewShipmentID = shipmentID });
                    else
                        return Json(new
                        {
                            IsSuccess = false,
                            error = ParcelrateResources.Resources.Error_Message_OnService
                        });
                }
                else
                    return Json(new
                    {
                        IsSuccess = false,
                        error = ParcelrateResources.Resources.InternetConnection_Error
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(ShipmentController), nameof(CreateShipment), ex));
                return Json(new
                {
                    IsSuccess = false,
                    error = ParcelrateResources.Resources.Error_Message_OnService
                });
            }
        }

        [HttpPost]
        public JsonResult GetCountryData(long countryID)
        {
            var country = _countryComponent.GetCountryByID(countryID);
            ViewBag.Currency = country?.Currency;
            return Json(country);
        }

        [HttpPost]
        public JsonResult GetCustomerAddress(long addressID)
        {
            var adddress = _addressComponent.GetAddressByID(addressID);
            return Json(adddress);
        }

        [HttpPost]
        public IActionResult ViewPDF(long shipmentID)
        {
            try
            {
                var shipment = _shipmentComponent.GetShipmentByID(shipmentID);
                if (shipment != null && shipment.IsCancelled)
                    return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.Shipment_Cancel_Message });
                string filePath = _common.GetPDFPath(shipmentID.ToString(), _isLetMeShip, User.Identity.Name);
                if (!System.IO.File.Exists(filePath) && _isLetMeShip)
                {
                    if (StaticFunctions.IsInternetConnected())
                        GetLMSLabel(shipmentID, shipment.PickupCountry.Code);
                    else
                        return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.InternetConnection_Error });
                }
                if (System.IO.File.Exists(filePath))
                {
                    string fileName = shipmentID.ToString() + Constants.PdfFileExtension;
                    return Json(new { IsSuccess = true, Url = Url.Action(nameof(ShipmentController.DownloadPDF), new { fileName, filePath }) });
                }
                return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.Label_Error });
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(ShipmentController), nameof(ViewPDF), ex));
                return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.ErrorMessage });
            }
        }

        [HttpGet]
        public IActionResult DownloadPDF(string fileName, string filePath)
        {
            var contentDisposition = new ContentDispositionHeaderValue("attachment");
            contentDisposition.SetHttpFileName(fileName);
            Response.Headers[HeaderNames.ContentDisposition] = contentDisposition.ToString();
            return File(System.IO.File.OpenRead(filePath), "application/pdf", fileName);
        }

        [HttpPost]
        public IActionResult EmailPDF(string email, long shipmentID)
        {
            try
            {
                var shipment = _shipmentComponent.GetShipmentByID(shipmentID);
                if (shipment != null && shipment.IsCancelled)
                    return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.Shipment_Cancel_Message });
                string filePath = _common.GetPDFPath(shipmentID.ToString(), _isLetMeShip, User.Identity.Name);
                if (!string.IsNullOrEmpty(filePath))
                {
                    if (!System.IO.File.Exists(filePath) && _isLetMeShip)
                    {
                        var country = _countryComponent.GetCountryByID(Convert.ToInt64(shipment.PickupCountryId));
                        if (StaticFunctions.IsInternetConnected())
                            GetLMSLabel(shipmentID, country.Code);
                        else
                            return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.InternetConnection_Error });
                    }
                    if (System.IO.File.Exists(filePath))
                    {
                        SendEmail(filePath, email);
                        return Json(new { IsSuccess = true, message = ParcelrateResources.Resources.Email_Success_Message });
                    }
                }
                return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.Label_Error });
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(ShipmentController), nameof(EmailPDF), ex));
                return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.ErrorMessage });
            }
        }

        [HttpPost]
        public IActionResult UpdateShipmentAWBNumber(long id, string awbNumber)
        {
            try
            {
                bool isUpdated = _shipmentComponent.UpdateShipmentAWBNumber(id, awbNumber);
                return Json(new { IsSuccess = true, message = ParcelrateResources.Resources.AWB_Saved });
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(ShipmentController), nameof(UpdateShipmentAWBNumber), ex));
                return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.ErrorMessage });
            }
        }

        [HttpPost]
        public IActionResult TrackShipment(long shipmentID)
        {
            try
            {
                var shipment = _shipmentComponent.GetShipmentByID(shipmentID);
                if (string.IsNullOrEmpty(shipment.AWBNumber))
                    return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.Required_AWBNumber });
                else
                {
                    var trackingRequest = new TrackingRequestModel
                    {
                        UserName = User.Identity.Name,
                        PassWord = User.Claims.FirstOrDefault(x => x.Type == Constants.PropertiesName.Password)?.Value,
                        Carrier = shipment.Carrier,
                        ShipmentId = shipment.ShipmentID,
                        AwbNumber = shipment.AWBNumber,
                        Culture = User.Claims.FirstOrDefault(x => x.Type == Constants.Culture)?.Value,
                        CountryCode = shipment.PickupCountry.Code
                    };
                    var result = _lmsServices.GetTracking(trackingRequest);
                    if (result != null)
                    {
                        if (result.Messages != null && result.Messages.Count > 0)
                        {
                            result.Messages[0] = "- " + result.Messages[0];
                            return Json(new { IsSuccess = false, error = string.Join(Environment.NewLine + "- ", result.Messages.ToArray()) });
                        }
                        else
                        {
                            return Json(new { IsSuccess = true, data = result });
                        }
                    }
                    else
                        return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.Error_Message_OnService });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(ShipmentController), nameof(TrackShipment), ex));
                return Json(new
                {
                    IsSuccess = false,
                    error = ParcelrateResources.Resources.ErrorMessage
                });
            }
        }

        /// <summary>
        /// Method to download result file
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public ActionResult DownloadFile()
        {
            Byte[] btArray = null;
            try
            {
                var user = _userComponent.GetUserByUsername(User.Identity.Name);
                btArray = _common.WriteShipmentsExcel(_shipmentComponent.GetShipmentsQuery(Convert.ToInt64(User.Claims.FirstOrDefault(x => x.Type == Constants.PropertiesName.ProviderID)?.Value), user.ID).OrderByDescending(x => x.OrderBookedOn).ToList(), Constants.SHIPMENTS);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ControllerName.ShipmentController, Constants.MethodName.DownloadFile, ex));
            }
            return File(btArray, Constants.EXCEL_CONTENT_TYPE, DateTime.UtcNow.ToString(Constants.TIME_STEMP) + "_" + ParcelrateResources.Resources.Shipments + Constants.EXCEL_EXTENSION_1);
        }

        #region Private Methods


        #region LetMeShip

        /// <summary>
        /// Method to get LetMeShip Label
        /// </summary>
        /// <param name="shipmentID"></param>
        /// <param name="countryCode"></param>
        private void GetLMSLabel(long shipmentID, string countryCode)
        {
            try
            {
                if (shipmentID > 0)
                {
                    var labelRequest = new LetMeShip.GetLabelRequestModel()
                    {
                        UserName = User.Identity.Name,
                        PassWord = User.Claims.FirstOrDefault(x => x.Type == Constants.PropertiesName.Password)?.Value,
                        ShipmentId = shipmentID,
                        SendLabel = true,
                        SendSummary = false,
                        CountryCode = countryCode
                    };
                    var labelResult = _lmsServices.GetLabel(labelRequest);
                    if (labelResult.Status == Constants.DocumentReadyStatus)
                    {
                        string filePath = _common.GetPDFPath(shipmentID.ToString(), _isLetMeShip, User.Identity.Name);
                        if (!string.IsNullOrEmpty(filePath) && !System.IO.File.Exists(filePath))
                            System.IO.File.WriteAllBytes(filePath, labelResult.Data);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(ShipmentController), nameof(GetLMSLabel), ex));
            }
        }

        #endregion

        #region AllPacka

        /// <summary>
        /// Method to get AllPacka Label
        /// </summary>
        /// <param name="label"></param>
        private void GetAllPackaLabel(object label, long shipmentID)
        {
            try
            {
                if (label != null)
                {
                    byte[] data = Convert.FromBase64String(Convert.ToString(label));
                    if (data != null)
                    {
                        string filePath = _common.GetPDFPath(shipmentID.ToString(), _isLetMeShip, User.Identity.Name);
                        if (!System.IO.File.Exists(filePath))
                            System.IO.File.WriteAllBytes(filePath, data);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(ShipmentController), nameof(GetAllPackaLabel), ex));
            }
        }

        /// <summary>
        /// Method to get carrier logo from db
        /// </summary>
        /// <param name="carrierID"></param>
        /// <returns></returns>
        private string GetCarrierLogo(long carrierID)
        {
            var logoName = _carrierComponent.GetCarriers().FirstOrDefault(x => x.AllPackaCarrierId == carrierID)?.Logo ?? Constants.DefaultCarrierLogo;
            return string.Concat(Constants.CarrierLogoPath, logoName);
        }

        /// <summary>
        /// Method to check whether Allpacka carrier provide label or not
        /// </summary>
        /// <param name="carrierID"></param>
        /// <returns></returns>
        private bool DoesCarrierProvideLabel(long carrierID)
        {
            bool provideLabel = false;
            return _carrierComponent.GetCarriers().FirstOrDefault(x => x.AllPackaCarrierId == carrierID)?.DoesProvideLabel ?? provideLabel;
        }

        #endregion

        /// <summary>
        ///  Methods to save the address details
        /// </summary>
        /// <param name="shipmentViewModel"></param>
        /// <param name="isDefault"></param>
        private void SaveAddress(ShipmentDetailsViewModel shipmentViewModel, bool isDefault)
        {
            try
            {
                var newAddress = new AddressEntity();
                var user = _userComponent.GetUserByUsername(User.Identity.Name);
                if (isDefault)
                {
                    {
                        newAddress.FirstName = shipmentViewModel.ShippedFromFirstName;
                        newAddress.LastName = shipmentViewModel.ShippedFromLastName;
                        newAddress.Address1 = shipmentViewModel.ShippedFromAddress1;
                        newAddress.Address2 = shipmentViewModel.ShippedFromAddress2;
                        newAddress.Company = shipmentViewModel.ShippedFromCompany;
                        newAddress.CountryId = (long)shipmentViewModel.ShippedFromCountryId;
                        newAddress.CountryCode = shipmentViewModel.ShippedFromCountryCode;
                        newAddress.PhonePrefix = shipmentViewModel.ShippedFromPhonePrefix;
                        newAddress.PhoneNo = shipmentViewModel.ShippedFromPhoneNo;
                        newAddress.City = shipmentViewModel.ShippedFromCity;
                        newAddress.PostalCode = shipmentViewModel.ShippedFromPostalCode;
                        newAddress.Email = shipmentViewModel.ShippedFromEmail;
                        newAddress.IsDefault = isDefault;
                        newAddress.IsPickupOrder = shipmentViewModel.IsPickupOrder;
                    };
                }
                else
                {
                    newAddress.Id = shipmentViewModel.DeliveryAddressID;
                    newAddress.CustomerID = shipmentViewModel.CustomerID;
                    if (string.IsNullOrEmpty(newAddress.CustomerID))
                    {
                        newAddress.CustomerID = Guid.NewGuid().ToString().Substring(0, 8).ToUpper();
                        TempData["CustomerId"] = newAddress.CustomerID;
                    }
                    newAddress.FirstName = shipmentViewModel.FirstName;
                    newAddress.LastName = shipmentViewModel.LastName;
                    newAddress.Address1 = shipmentViewModel.Address1;
                    newAddress.Address2 = shipmentViewModel.Address2;
                    newAddress.Company = shipmentViewModel.Company;
                    newAddress.CountryId = (long)shipmentViewModel.CountryId;
                    newAddress.CountryCode = shipmentViewModel.CountryCode;
                    newAddress.PhonePrefix = shipmentViewModel.PhonePrefix;
                    newAddress.PhoneNo = shipmentViewModel.PhoneNo;
                    newAddress.City = shipmentViewModel.City;
                    newAddress.PostalCode = shipmentViewModel.PostalCode;
                    newAddress.Email = shipmentViewModel.Email;
                    newAddress.IsDefault = isDefault;
                    newAddress.IsPickupOrder = false;
                }
                TempData["AddressId"] = _addressComponent.AddAddress(newAddress, user.ID);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(ShipmentController), nameof(SaveAddress), ex));
            }
        }

        /// <summary>
        /// Bind pickup from time range
        /// </summary>
        private List<PickupTime> BindPickUpFromTimeRange()
        {
            return _common.GetTimeRangeValueList(new TimeSpan(09, 00, 00), new TimeSpan(19, 45, 00), new TimeSpan(00, 15, 00));
        }

        /// <summary>
        /// Bind pickup from time range
        /// </summary>
        private List<PickupTime> BindPickUpToTimeRange()
        {
            return _common.GetTimeRangeValueList(new TimeSpan(13, 00, 00), new TimeSpan(23, 45, 00), new TimeSpan(00, 15, 00));
        }

        /// <summary>
        /// Method to add new shipment in database
        /// </summary>
        /// <param name="createShipmentRequestModel"></param>
        /// <param name="shipmentRes"></param>
        /// <param name="shipmentID"></param>
        private long AddUpdateShipment(dynamic createShipmentRequestModel, dynamic shipmentRes, long shipmentID)
        {
            long shipID = 0;
            try
            {
                var user = _userComponent.GetUserByUsername(User.Identity.Name);
                var shipmentEntity = new ShipmentEntity
                {
                    ProviderID = _providerID,
                    Carrier = createShipmentRequestModel.SelectedService?.Name,
                    Service = createShipmentRequestModel.SelectedService?.ServiceName,
                    Reference = createShipmentRequestModel.ShipperReference,
                    ShipmentID = shipmentID,
                    TotalPrice = _isLetMeShip ? createShipmentRequestModel.SelectedService?.LetMeShipPriceInfo.TotalPrice : createShipmentRequestModel.SelectedService?.PriceInfo.TotalPrice,
                    PickupDateTime = createShipmentRequestModel.PickupInterval_Date,
                    Status = Constants.BookedLabelNotGeneratedStatus,
                    DimensionsCaptured = createShipmentRequestModel.IsBedalConnected ? Constants.BedalText : Constants.ManualText,
                    PickupCountryId = createShipmentRequestModel.PickupAddress.CountryId,
                    DeliveryCountryId = createShipmentRequestModel.DeliveryAddress.CountryId,
                    CarrierId = createShipmentRequestModel.SelectedService?.Id <= 0 ? (long?)null : createShipmentRequestModel.SelectedService?.Id,
                    UserId = user.ID
                };
                shipID = _shipmentComponent.AddUpdateShipment(shipmentEntity);
                if (shipID > 0)
                {
                    if (_isLetMeShip)
                        GetLMSLabel(shipmentID, createShipmentRequestModel.PickupAddress?.CountryCode);
                    else
                    {
                        var quote = shipmentRes.Quotes[0] as Quote;
                        if (createShipmentRequestModel.SelectedService?.DoesProvideLabel && quote?.Labels != null && quote?.Labels.Count > 0)
                            GetAllPackaLabel(quote.Labels[0], shipmentID);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(ShipmentController), nameof(AddUpdateShipment), ex));
            }
            return shipID;
        }

        private void SendEmail(string pdfPath, string email)
        {
            try
            {
                string emailTemplatePath = _common.GetTemplatePath();
                Attachment attachment = new Attachment(pdfPath, MediaTypeNames.Application.Octet);
                _common.SendSMTPMail(email, ParcelrateResources.Resources.Shipment_Label, string.Format(System.IO.File.ReadAllText(emailTemplatePath), email), attachment);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(ShipmentController), nameof(SendEmail), ex));
            }
        }

        #endregion
    }
}