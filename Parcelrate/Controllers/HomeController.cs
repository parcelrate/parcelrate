﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Parcelrate.Helpers;
using Parcelrate.Models;
using ParcelrateComponent.Interfaces;
using ParcelrateEntities;
using ParcelrateUtilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Parcelrate.Controllers
{
    [CustomAuthorize]
    public class HomeController : Controller
    {
        private readonly ICountryComponent _countryComponent;
        private readonly IShipmentComponent _shipmentComponent;
        private readonly IUserComponent _userComponent;
        private readonly Common _common;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ICountryComponent countryBAL, IShipmentComponent shipmentBAL, IUserComponent userBAL, Common common, ILogger<HomeController> logger)
        {
            _countryComponent = countryBAL;
            _shipmentComponent = shipmentBAL;
            _userComponent = userBAL;
            _common = common;
            _logger = logger;
        }

        public IActionResult Index()
        {
            try
            {
                long providerID = Convert.ToInt64(User.Claims.FirstOrDefault(x => x.Type == Constants.PropertiesName.ProviderID)?.Value);
                var user = _userComponent.GetUserByUsername(User.Identity.Name);
                var shipmentsPriceByCountry = _shipmentComponent.GetShipmentsPriceByCountry(providerID, user.ID);
                JSPieChart spendByCountryPie = new JSPieChart();
                spendByCountryPie.datasets.Add(new PieDataPoints());
                spendByCountryPie.datasets[0].backgroundColor = shipmentsPriceByCountry.Select(x => x.Color).ToList();
                spendByCountryPie.datasets[0].label = "Dataset 1";
                var spendByCountryLabels = new List<ChartEntity>();

                for (int i = 0; i < shipmentsPriceByCountry.Count; i++)
                {
                    spendByCountryPie.labels.Add(shipmentsPriceByCountry[i].Key);
                    spendByCountryPie.datasets[0].data.Add(string.Concat(String.Format("{0:0.00}", shipmentsPriceByCountry[i].Value)));
                    spendByCountryLabels.Add(new ChartEntity
                    {
                        Key = shipmentsPriceByCountry[i].Content + " (" + shipmentsPriceByCountry[i].Key + ")",
                        Value = shipmentsPriceByCountry[i].Value,
                        Color = shipmentsPriceByCountry[i].Color
                    });
                }
                ViewData["ShipmentsPriceByCountry"] = JsonConvert.SerializeObject(spendByCountryPie);
                ViewData["SpendByCountryLabels"] = spendByCountryLabels;

                var ordersByCarrier = _shipmentComponent.GetShipmentsCountByCarrier(providerID, user.ID);
                JSPieChart ordersByCarrierPie = new JSPieChart();
                ordersByCarrierPie.datasets.Add(new PieDataPoints());
                ordersByCarrierPie.datasets[0].backgroundColor = ordersByCarrier.Select(x => x.Color).ToList();
                ordersByCarrierPie.datasets[0].label = "Dataset 1";
                var ordersByCarrierLabels = new List<ChartEntity>();

                for (int i = 0; i < ordersByCarrier.Count; i++)
                {
                    ordersByCarrierPie.labels.Add(ordersByCarrier[i].Key);
                    ordersByCarrierPie.datasets[0].data.Add(ordersByCarrier[i].Value.ToString());
                    ordersByCarrierLabels.Add(new ChartEntity
                    {
                        Key = ordersByCarrier[i].Key,
                        Value = ordersByCarrier[i].Value,
                        Color = ordersByCarrier[i].Color
                    });
                }
                ViewData["OrdersByCarrier"] = JsonConvert.SerializeObject(ordersByCarrierPie);
                ViewData["OrdersByCarrierLabels"] = ordersByCarrierLabels;

                var shipmentsPriceByCarrier = _shipmentComponent.GetShipmentsPriceByCarrier(providerID, user.ID);
                JSPieChart spendByCarrierPie = new JSPieChart();
                spendByCarrierPie.datasets.Add(new PieDataPoints());
                spendByCarrierPie.datasets[0].backgroundColor = shipmentsPriceByCarrier.Select(x => x.Color).ToList();
                spendByCarrierPie.datasets[0].label = "Dataset 1";
                var spendByCarrierLabels = new List<ChartEntity>();

                for (int i = 0; i < shipmentsPriceByCarrier.Count; i++)
                {
                    spendByCarrierPie.labels.Add(shipmentsPriceByCarrier[i].Key);
                    spendByCarrierPie.datasets[0].data.Add(string.Concat(String.Format("{0:0.00}", shipmentsPriceByCarrier[i].Value)));
                    spendByCarrierLabels.Add(new ChartEntity
                    {
                        Key = shipmentsPriceByCarrier[i].Key,
                        Value = shipmentsPriceByCarrier[i].Value,
                        Color = shipmentsPriceByCarrier[i].Color
                    });
                }
                ViewData["ShipmentsPriceByCarrier"] = JsonConvert.SerializeObject(spendByCarrierPie);
                ViewData["SpendByCarrierLabels"] = spendByCarrierLabels;

                var shipmentsOverOneYear = _shipmentComponent.GetShipmentsOverOneYear(providerID, user.ID);
                JSPieChart shipmentsOverOneYearData = new JSPieChart();
                shipmentsOverOneYearData.datasets.Add(new PieDataPoints());
                shipmentsOverOneYearData.datasets[0].backgroundColor = new List<string>() { "#4bc0c0" };
                shipmentsOverOneYearData.datasets[0].label = ParcelrateResources.Resources.Cargo_Sent_Over_365Days;

                for (int i = 0; i < shipmentsOverOneYear.Count; i++)
                {
                    shipmentsOverOneYearData.labels.Add(shipmentsOverOneYear[i].Key);
                    shipmentsOverOneYearData.datasets[0].data.Add(shipmentsOverOneYear[i].Value.ToString());
                }
                ViewData["ShipmentsOverOneYear"] = JsonConvert.SerializeObject(shipmentsOverOneYearData);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(HomeController), nameof(Index), ex));
            }
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public JsonResult GetCountryData(long countryID)
        {
            try
            {
                var country = _countryComponent.GetCountryByID(countryID);
                return Json(country);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AccountController), nameof(GetCountryData), ex));
            }
            return Json(string.Empty);
        }

        /// <summary>
        /// Method to download Shipment List according to Price by Country result file
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public ActionResult DownloadShipmentListPriceByCountry()
        {
            Byte[] btArray = null;
            try
            {
                var user = _userComponent.GetUserByUsername(User.Identity.Name);
                btArray = _common.WriteShipmentsExcel(_shipmentComponent.GetShipmentListPriceByCountry(Convert.ToInt64(User.Claims.FirstOrDefault(x => x.Type == Constants.PropertiesName.ProviderID)?.Value), user.ID).OrderByDescending(x => x.OrderBookedOn).ToList(), Constants.SHIPMENTS);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ControllerName.HomeController, Constants.MethodName.DownloadShipmentListPriceByCountry, ex));
            }
            return File(btArray, Constants.EXCEL_CONTENT_TYPE, DateTime.UtcNow.ToString(Constants.TIME_STEMP) + "_" + ParcelrateResources.Resources.Spend_By_Country + Constants.EXCEL_EXTENSION_1);
        }

        /// <summary>
        /// Method to download Shipment List according to Orders by Carrier result file
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public ActionResult DownloadShipmentListOrdersByCarrier()
        {
            Byte[] btArray = null;
            try
            {
                var user = _userComponent.GetUserByUsername(User.Identity.Name);
                btArray = _common.WriteShipmentsExcel(_shipmentComponent.GetShipmentsListCountByCarrier(Convert.ToInt64(User.Claims.FirstOrDefault(x => x.Type == Constants.PropertiesName.ProviderID)?.Value), user.ID).OrderByDescending(x => x.OrderBookedOn).ToList(), Constants.SHIPMENTS);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ControllerName.HomeController, Constants.MethodName.DownloadShipmentListOrdersByCarrier, ex));
            }
            return File(btArray, Constants.EXCEL_CONTENT_TYPE, DateTime.UtcNow.ToString(Constants.TIME_STEMP) + "_" + ParcelrateResources.Resources.Orders_Per_Carrier + Constants.EXCEL_EXTENSION_1);
        }

        /// <summary>
        /// Method to download Shipment List according to Price by Carrier result file
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public ActionResult DownloadShipmentListPriceByCarrier()
        {
            Byte[] btArray = null;
            try
            {
                var user = _userComponent.GetUserByUsername(User.Identity.Name);
                btArray = _common.WriteShipmentsExcel(_shipmentComponent.GetShipmentsListPriceByCarrier(Convert.ToInt64(User.Claims.FirstOrDefault(x => x.Type == Constants.PropertiesName.ProviderID)?.Value), user.ID).OrderByDescending(x => x.OrderBookedOn).ToList(), Constants.SHIPMENTS);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ControllerName.HomeController, Constants.MethodName.DownloadShipmentListPriceByCarrier, ex));
            }
            return File(btArray, Constants.EXCEL_CONTENT_TYPE, DateTime.UtcNow.ToString(Constants.TIME_STEMP) + "_" + ParcelrateResources.Resources.Spend_By_Carrier + Constants.EXCEL_EXTENSION_1);
        }

        /// <summary>
        /// Method to download Shipment List over 1 year result file
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public ActionResult DownloadShipmentListOverYear()
        {
            Byte[] btArray = null;
            try
            {
                var user = _userComponent.GetUserByUsername(User.Identity.Name);
                btArray = _common.WriteShipmentsExcel(_shipmentComponent.GetShipmentsListOverOneYear(Convert.ToInt64(User.Claims.FirstOrDefault(x => x.Type == Constants.PropertiesName.ProviderID)?.Value), user.ID).OrderByDescending(x => x.OrderBookedOn).ToList(), Constants.SHIPMENTS);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ControllerName.HomeController, Constants.MethodName.DownloadShipmentListOverYear, ex));
            }
            return File(btArray, Constants.EXCEL_CONTENT_TYPE, DateTime.UtcNow.ToString(Constants.TIME_STEMP) + "_" + ParcelrateResources.Resources.Cargo_Sent_Over_365Days + Constants.EXCEL_EXTENSION_1);
        }

    }
}
