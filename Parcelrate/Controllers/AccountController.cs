using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Parcelrate.Helpers;
using Parcelrate.Models;
using ParcelrateComponent.Interfaces;
using ParcelrateUtilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using ParcelrateEntities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace Parcelrate.Controllers
{
    public class AccountController : Controller
    {
        private readonly ICountryComponent _countryComponent;
        private readonly IServiceProviderComponent _providerComponent;
        private readonly IUserComponent _userComponent;

        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession session => _httpContextAccessor.HttpContext.Session;
        private readonly Common _common;
        private readonly ILogger<AccountController> _logger;
        private readonly IConfiguration _configuration;

        public AccountController(IHttpContextAccessor httpContextAccesor, Common common, ICountryComponent countryBAL, IServiceProviderComponent providerBAL, IUserComponent userBAL, ILogger<AccountController> logger, IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccesor;
            _common = common;
            _countryComponent = countryBAL;
            _providerComponent = providerBAL;
            _userComponent = userBAL;
            _logger = logger;
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult Login()
        {
            var loginViewModel = new LoginViewModel();
            try
            {
                ViewBag.LanguageList = _common.GetLanguageList();
                string culture = "en-US";
                if (TempData["IsCultureChange"] == null)
                {
                    CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
                    culture = currentCulture != null && currentCulture.TwoLetterISOLanguageName.ToLower().Contains("de") ? "de-DE" : "en-US";
                    _common.SetCulture(culture);
                }
                loginViewModel.Culture = TempData["Culture"] != null ? TempData["Culture"].ToString() : culture;
                ViewBag.CountryList = _countryComponent.GetAllProvidersCountryList(loginViewModel.Culture);
                ViewBag.ProviderList = _providerComponent.GetServiceProvidersByCountry(Convert.ToInt64(loginViewModel.CountryId));
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AccountController), nameof(Login), ex));
            }
            return View(loginViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            try
            {
                ViewBag.CountryList = _countryComponent.GetAllProvidersCountryList(loginViewModel.Culture);
                ViewBag.ProviderList = _providerComponent.GetServiceProvidersByCountry(Convert.ToInt64(loginViewModel.CountryId));
                ViewBag.LanguageList = _common.GetLanguageList();
                if (ModelState.IsValid)
                {
                    var provider = _providerComponent.GetProviderByID(Convert.ToInt64(loginViewModel.ProviderId));
                    bool isLetMeShip = provider != null && provider.Name.ToLowerInvariant().Contains(Constants.LetMeShipText);
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, loginViewModel.Username),
                        new Claim(Constants.PropertiesName.Password, loginViewModel.Password),
                        new Claim(Constants.IsLetMeShip, isLetMeShip.ToString()),
                        new Claim(Constants.PropertiesName.ProviderID, Convert.ToString(loginViewModel.ProviderId)),
                        new Claim(Constants.Culture, loginViewModel.Culture??"en-US")
                    };
                    var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity),
                    new AuthenticationProperties
                    {
                        IsPersistent = loginViewModel.RememberMe
                    });
                    var user = new UserEntity
                    {
                        UserName = loginViewModel.Username,
                        Password = loginViewModel.Password,
                        ProviderID = (long)loginViewModel.ProviderId,
                        CountryID = (long)loginViewModel.CountryId,
                        Culture = loginViewModel.Culture
                    };
                    _userComponent.AddUserInfo(user);
                    return RedirectToAction(nameof(HomeController.Index), "Home");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AccountController), nameof(Login), ex));
            }
            return View(loginViewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            try
            {
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AccountController), nameof(Logout), ex));
            }
            return RedirectToAction(nameof(Login));
        }

        [HttpPost]
        public JsonResult GetProvidersByCountry(long countryID)
        {
            try
            {
                var providerList = _providerComponent.GetServiceProvidersByCountry(countryID);
                return Json(providerList);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AccountController), nameof(GetProvidersByCountry), ex));
            }
            return Json(string.Empty);
        }

        [HttpPost]
        public void SetLanguage(string culture)
        {
            TempData["IsCultureChange"] = true;
            TempData["Culture"] = culture;
            _common.SetCulture(culture);
        }

        [HttpGet]
        public IActionResult SignUp()
        {
            var signUpViewModel = new SignUpViewModel();
            try
            {
                ViewBag.LanguageList = _common.GetLanguageList();
                string culture = "en-US";
                if (TempData["IsCultureChange"] == null)
                {
                    CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
                    culture = currentCulture != null && currentCulture.TwoLetterISOLanguageName.ToLower().Contains("de") ? "de-DE" : "en-US";
                    _common.SetCulture(culture);
                }
                signUpViewModel.Culture = TempData["Culture"] != null ? TempData["Culture"].ToString() : culture;
                ViewBag.CountryList = _countryComponent.GetCountries(signUpViewModel.Culture);
                ViewBag.ContactMeForList = _common.GetContactMeForList();
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AccountController), nameof(Login), ex));
            }
            return View(signUpViewModel);
        }

        [HttpPost]
        public IActionResult SignUp(SignUpViewModel model)
        {
            var signUpViewModel = new SignUpViewModel();
            try
            {
                string filePath = _common.GetWebRootPath() + Constants.TemplateFolder + Constants.EnquiryEmailTemplate + Constants.HtmlExtension;
                string templateText = System.IO.File.ReadAllText(filePath);
                string emailTemplatePath = string.Format(System.IO.File.ReadAllText(filePath),
                    DateTime.UtcNow.Year,
                    model.CountryName,
                    model.BusinessName,
                    model.BusinessName,
                    model.Email,
                    model.PhoneNo,
                    model.ContactMeFor);
                _common.SendSMTPMail(_configuration.GetSection("EnquiryEmail").Value, Constants.EnquirySubject, emailTemplatePath, null);
                signUpViewModel.Message = ParcelrateResources.Resources.Signup_Success_Message;
                ViewBag.LanguageList = _common.GetLanguageList();
                signUpViewModel.Culture = model.Culture;
                ViewBag.CountryList = _countryComponent.GetCountries(signUpViewModel.Culture);
                ViewBag.ContactMeForList = _common.GetContactMeForList();
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AccountController), nameof(Login), ex));
            }
            return View(signUpViewModel);
        }
    }
}