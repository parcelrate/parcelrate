using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using Parcelrate.Helpers;
using Parcelrate.Models;
using ParcelrateComponent.Interfaces;
using ParcelrateEntities;
using ParcelrateUtilities;
using ReflectionIT.Mvc.Paging;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Parcelrate.Controllers
{
    [CustomAuthorize]
    public class AddressController : Controller
    {
        private readonly IAddressComponent _addressComponent;
        private readonly ICountryComponent _countryComponent;
        private readonly IUserComponent _userComponent;
        private readonly IMapper _mapper;
        private readonly Common _common;
        private readonly ILogger<AddressController> _logger;


        public AddressController(IMapper mapper, Common common, IAddressComponent addressBAL, ICountryComponent countryBAL, IUserComponent userBAL, ILogger<AddressController> logger)
        {
            _mapper = mapper;
            _common = common;
            _addressComponent = addressBAL;
            _countryComponent = countryBAL;
            _userComponent = userBAL;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Index(string filter, int page = 1, string sortExpression = Constants.AddressDefaultSortExpression)
        {
            var user = _userComponent.GetUserByUsername(User.Identity.Name);
            var qry = _addressComponent.GetAddressesQuery(user.ID);
            ViewData[Constants.Filter] = filter;
            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrWhiteSpace(filter))
                qry = qry.Where(a => a.FirstName != null && a.FirstName.ToLower().Contains(filter.Trim()) ||
                    a.LastName != null && a.LastName.ToLower().Contains(filter.Trim()) ||
                    (a.FirstName + " " + a.LastName).Contains(new Regex(@"\s\s+").Replace(filter.Trim(), " ")));
            var model = await PagingList.CreateAsync(qry, Constants.PageSize, page, sortExpression, Constants.AddressDefaultSortExpression);
            model.RouteValue = new RouteValueDictionary {{ Constants.Filter, filter}};
            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            try
            {
                string culture = Convert.ToString(User.Claims.FirstOrDefault(x => x.Type == Constants.Culture)?.Value);
                ViewBag.CountryList = _countryComponent.GetCountries(culture);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AddressController), nameof(Create), ex));
            }
            return View(new AddressViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(AddressViewModel addressViewModel)
        {
            try
            {
                string culture = Convert.ToString(User.Claims.FirstOrDefault(x => x.Type == Constants.Culture)?.Value);
                ViewBag.CountryList = _countryComponent.GetCountries(culture);
                if (ModelState.IsValid)
                {
                    addressViewModel.CustomerID = addressViewModel.CustomerID.Trim();
                    var address = _mapper.Map<AddressViewModel, AddressEntity>(addressViewModel);
                    var user = _userComponent.GetUserByUsername(User.Identity.Name);
                    if (_addressComponent.AddAddress(address, user.ID) < 0)
                        ViewBag.ErrorMessage = ParcelrateResources.Resources.CustomerID_AlreadyExist_Error;
                    else
                        ViewBag.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ParcelrateResources.Resources.ErrorMessage);
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AddressController), nameof(Create), ex));
            }
            return View(addressViewModel);
        }

        [HttpGet]
        public IActionResult Edit(long? id)
        {
            string culture = Convert.ToString(User.Claims.FirstOrDefault(x => x.Type == Constants.Culture)?.Value);
            ViewBag.CountryList = _countryComponent.GetCountries(culture);
            if (id == null)
                return NotFound();
            var address = _addressComponent.GetAddressByID((long)id);
            var addressModel = _mapper.Map<AddressEntity, AddressViewModel>(address);
            return View(addressModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(long id, AddressViewModel addressViewModel)
        {
            try
            {
                string culture = Convert.ToString(User.Claims.FirstOrDefault(x => x.Type == Constants.Culture)?.Value);
                ViewBag.CountryList = _countryComponent.GetCountries(culture);
                if (id != addressViewModel.Id)
                    return NotFound();
                if (addressViewModel.IsDefault)
                    ModelState.Remove(nameof(addressViewModel.CustomerID));
                if (ModelState.IsValid)
                {
                    if (!addressViewModel.IsDefault)
                        addressViewModel.CustomerID = addressViewModel.CustomerID.Trim();
                    var address = _mapper.Map<AddressViewModel, AddressEntity>(addressViewModel);
                    var user = _userComponent.GetUserByUsername(User.Identity.Name);
                    if (_addressComponent.AddAddress(address, user.ID) < 0)
                        ViewBag.ErrorMessage = ParcelrateResources.Resources.CustomerID_AlreadyExist_Error;
                    else
                        ViewBag.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ParcelrateResources.Resources.ErrorMessage);
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AddressController), nameof(Edit), ex));
            }
            return View(addressViewModel);
        }

        [HttpPost]
        public IActionResult Delete(long id)
        {
            try
            {
                _addressComponent.DeleteAddress(id);
                return Json(new { IsSuccess = true, message = ParcelrateResources.Resources.Address_Deleted });
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AddressController), nameof(Delete), ex));
                return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.DeleteAddress_Error });
            }
        }

        [HttpPost]
        public IActionResult DownloadSampleCSV()
        {
            try
            {
                string filePath = _common.GetWebRootPath() + Constants.TemplateFolder + Constants.SampleCSVFileName;
                if (System.IO.File.Exists(filePath))
                    return Json(new { IsSuccess = true, Url = Url.Action(nameof(AddressController.DownloadCSV), new { filePath }) });
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AddressController), nameof(DownloadSampleCSV), ex));
            }
            return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.SampleCSV_NotExist });
        }

        [HttpGet]
        public IActionResult DownloadCSV(string filePath)
        {
            var contentDisposition = new ContentDispositionHeaderValue("attachment");
            contentDisposition.SetHttpFileName(Constants.SampleCSVFileName);
            Response.Headers[HeaderNames.ContentDisposition] = contentDisposition.ToString();
            return File(System.IO.File.OpenRead(filePath), "text/csv", Constants.SampleCSVFileName);
        }

        [HttpPost]
        public IActionResult UploadFile(IFormFile file)
        {
            DataTable validAddressess = new DataTable();
            string errorMessage = string.Empty;
            DataTable uploadAddressess = null;
            try
            {
                if (file != null && file.Length > 0 && Path.GetExtension(file.FileName) == Constants.CsvFileExtension)
                {
                    uploadAddressess = StaticFunctions.ReadCsvFile(file.OpenReadStream());
                    ValidateUploadedFile(uploadAddressess, out errorMessage, out validAddressess);
                    if (validAddressess.Rows.Count > 0)
                    {
                        var user = _userComponent.GetUserByUsername(User.Identity.Name);
                        bool isSuccess = _addressComponent.AddAddressesFromDataTable(validAddressess, user.ID);
                        if (isSuccess)
                        {
                            if (errorMessage != string.Empty)
                            {
                                errorMessage = validAddressess.Rows.Count + " " + ParcelrateResources.Resources.Address_Imported +
                                    Environment.NewLine + Environment.NewLine +
                                    ParcelrateResources.Resources.Error_Detail_Heading +
                                    Environment.NewLine + Environment.NewLine +
                                    errorMessage;
                            }
                            return Json(new { IsSuccess = true, message = ParcelrateResources.Resources.Address_Imported, errorMessages = errorMessage });
                        }
                    }
                }
                if (errorMessage != string.Empty)
                {
                    errorMessage = ParcelrateResources.Resources.Error_Detail_Heading +
                                Environment.NewLine + Environment.NewLine +
                                errorMessage;
                }
                return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.CSV_Error, errorMessages = errorMessage });
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AddressController), nameof(UploadFile), ex));
                return Json(new { IsSuccess = false, error = ParcelrateResources.Resources.ErrorMessage });
            }
        }

        /// <summary>
        /// common method to validate uploaded csv file
        /// </summary>
        /// <param name="dtAddress"></param>
        /// <param name="errorMessage"></param>
        /// <param name="validAddressess"></param>
        public void ValidateUploadedFile(DataTable dtAddress, out string errorMessage, out DataTable validAddressess)
        {
            validAddressess = new DataTable();
            errorMessage = string.Empty;
            try
            {
                if (dtAddress != null && dtAddress.Rows.Count > 0)
                {
                    if (ValidateHeader(dtAddress.Columns))
                    {
                        ValidateRow(dtAddress, out errorMessage, out validAddressess);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AddressController), nameof(ValidateUploadedFile), ex));
            }
        }

        /// <summary>
        /// Method to validate the header. Returns false if any column is invalid
        /// </summary>
        /// <param name="columns"></param>
        /// <returns></returns>        
        private bool ValidateHeader(DataColumnCollection columns)
        {
            bool isValidHeader = true;
            try
            {
                List<string> lstColumns = new List<string> { "CustomerId", "FirstName", "LastName", "Street", "HouseNo", "Company", "CountryCode", "City", "PhoneNumber", "PostalCode", "Email" };
                for (int index = 0; index < columns.Count; index++)
                {
                    if (Convert.ToString(columns[index]) != lstColumns[index])
                    {
                        isValidHeader = false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AddressController), nameof(ValidateHeader), ex));
                isValidHeader = false;
            }
            return isValidHeader;
        }

        private DataTable AddColumnsInDataTable()
        {
            DataTable dt = new DataTable();
            try
            {
                List<string> lstColumns = new List<string> { "CustomerId", "FirstName", "LastName", "Street", "HouseNo", "Company", "CountryCode", "City", "PhoneNumber", "PostalCode", "Email" };
                for (int index = 0; index < lstColumns.Count; index++)
                {
                    dt.Columns.Add(lstColumns[index]);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AddressController), nameof(ValidateHeader), ex));
            }
            return dt;
        }

        /// <summary>
        /// common method to validate each row of csv file.
        /// </summary>
        /// <param name="dtAddress"></param>
        /// <param name="errorString"></param>
        /// <param name="validAddresses"></param>
        private void ValidateRow(DataTable dtAddress, out string errorString, out DataTable validAddresses)
        {
            StringBuilder stringBuilder = new StringBuilder();
            validAddresses = AddColumnsInDataTable();
            errorString = string.Empty;
            try
            {
                string culture = Convert.ToString(User.Claims.FirstOrDefault(x => x.Type == Constants.Culture)?.Value);
                List<CountryEntity> lstCountries = _countryComponent.GetCountries(culture);
                bool isRecordValid = true;
                for (int i = 0; i < dtAddress.Rows.Count; i++)
                {
                    var customerID = Convert.ToString(dtAddress.Rows[i]["CustomerId"]).ToUpper().Trim();
                    if (customerID == string.Empty)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : CustomerId, " + ParcelrateResources.Resources.Message + " : " + ParcelrateResources.Resources.Required_CustomerID + Environment.NewLine);
                    }

                    if (customerID != string.Empty && customerID.Length > 50)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : CustomerId, " + ParcelrateResources.Resources.Message + " : " + string.Format(ParcelrateResources.Resources.Value_Cannot_Be_Greater_Than, 50) + Environment.NewLine);
                    }

                    if (Convert.ToString(dtAddress.Rows[i]["FirstName"]).ToUpper().Trim().Length > 50)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : FirstName, " + ParcelrateResources.Resources.Message + " : " + string.Format(ParcelrateResources.Resources.Value_Cannot_Be_Greater_Than, 50) + Environment.NewLine);
                    }

                    var lastName = Convert.ToString(dtAddress.Rows[i]["LastName"]).ToUpper().Trim();
                    if (lastName == string.Empty)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : LastName, " + ParcelrateResources.Resources.Message + " : " + ParcelrateResources.Resources.Required_LastName + Environment.NewLine);
                    }

                    if (lastName != string.Empty && lastName.Length > 50)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : LastName, " + ParcelrateResources.Resources.Message + " : " + string.Format(ParcelrateResources.Resources.Value_Cannot_Be_Greater_Than, 50) + Environment.NewLine);
                    }

                    var street = Convert.ToString(dtAddress.Rows[i]["Street"]).ToUpper().Trim();
                    if (street == string.Empty)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : Street, " + ParcelrateResources.Resources.Message + " : " + ParcelrateResources.Resources.Required_Street + Environment.NewLine);
                    }

                    if (street != string.Empty && street.Length > 200)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : Street, " + ParcelrateResources.Resources.Message + " : " + string.Format(ParcelrateResources.Resources.Value_Cannot_Be_Greater_Than, 200) + Environment.NewLine);
                    }

                    var houseNo = Convert.ToString(dtAddress.Rows[i]["HouseNo"]).ToUpper().Trim();
                    if (houseNo == string.Empty)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : HouseNo, " + ParcelrateResources.Resources.Message + " : " + ParcelrateResources.Resources.Required_HouseNumber + Environment.NewLine);
                    }

                    if (houseNo == string.Empty && houseNo.Length > 7)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : HouseNo, " + ParcelrateResources.Resources.Message + " : " + string.Format(ParcelrateResources.Resources.Value_Cannot_Be_Greater_Than, 7) + Environment.NewLine);
                    }

                    if (Convert.ToString(dtAddress.Rows[i]["Company"]).ToUpper().Trim().Length > 50)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : Company, " + ParcelrateResources.Resources.Message + " : " + string.Format(ParcelrateResources.Resources.Value_Cannot_Be_Greater_Than, 50) + Environment.NewLine);
                    }

                    var countryCode = Convert.ToString(dtAddress.Rows[i]["CountryCode"]).ToUpper().Trim();
                    if (countryCode == string.Empty)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : CountryCode, " + ParcelrateResources.Resources.Message + " : " + ParcelrateResources.Resources.Required_CountryCode + Environment.NewLine);
                    }

                    if (countryCode != string.Empty && countryCode.Length > 3)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : CountryCode, " + ParcelrateResources.Resources.Message + " : " + string.Format(ParcelrateResources.Resources.Value_Cannot_Be_Greater_Than, 3) + Environment.NewLine);
                    }

                    if (countryCode != string.Empty && lstCountries.Find(x => x.Code == countryCode) == null)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : CountryCode, " + ParcelrateResources.Resources.Message + " : " + ParcelrateResources.Resources.Valid_CountryCode + Environment.NewLine);
                    }

                    var city = Convert.ToString(dtAddress.Rows[i]["City"]).ToUpper().Trim();
                    if (city == string.Empty)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : City, " + ParcelrateResources.Resources.Message + " : " + ParcelrateResources.Resources.Required_City + Environment.NewLine);
                    }

                    if (city != string.Empty && city.Length > 50)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : City, " + ParcelrateResources.Resources.Message + " : " + string.Format(ParcelrateResources.Resources.Value_Cannot_Be_Greater_Than, 50) + Environment.NewLine);
                    }

                    var phoneNo = Convert.ToString(dtAddress.Rows[i]["PhoneNumber"]).ToUpper().Trim();
                    if (phoneNo == string.Empty)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : PhoneNumber, " + ParcelrateResources.Resources.Message + " : " + ParcelrateResources.Resources.Required_PhoneNo + Environment.NewLine);
                    }

                    long? phone = long.TryParse(phoneNo, System.Globalization.NumberStyles.Float, null, out long result) ? result : (Nullable<long>)null;                    
                    if (phoneNo != string.Empty && phone == null)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : PhoneNumber, " + ParcelrateResources.Resources.Message + " : " + ParcelrateResources.Resources.Valid_Phone_No + Environment.NewLine);
                    }                 

                    if (phoneNo != string.Empty && Convert.ToString(phone).Length > 15)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : PhoneNumber, " + ParcelrateResources.Resources.Message + " : " + string.Format(ParcelrateResources.Resources.Value_Cannot_Be_Greater_Than, 15) + Environment.NewLine);
                    }

                    var postalCode = Convert.ToString(dtAddress.Rows[i]["PostalCode"]).ToUpper().Trim();
                    if (postalCode == string.Empty)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : PostalCode, " + ParcelrateResources.Resources.Message + " : " + ParcelrateResources.Resources.Required_PostalCode + Environment.NewLine);
                    }

                    if (postalCode != string.Empty && postalCode.Length > 10)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : PostalCode, " + ParcelrateResources.Resources.Message + " : " + string.Format(ParcelrateResources.Resources.Value_Cannot_Be_Greater_Than, 10) + Environment.NewLine);
                    }

                    var email = Convert.ToString(dtAddress.Rows[i]["Email"]).ToUpper().Trim();
                    if (email == string.Empty)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : Email, " + ParcelrateResources.Resources.Message + " : " + ParcelrateResources.Resources.Required_Email + Environment.NewLine);
                    }

                    if (email != string.Empty && email.Length > 50)
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : Email, " + ParcelrateResources.Resources.Message + " : " + string.Format(ParcelrateResources.Resources.Value_Cannot_Be_Greater_Than, 50) + Environment.NewLine);
                    }

                    if (email != string.Empty && !Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                    {
                        isRecordValid = false;
                        stringBuilder.Append(ParcelrateResources.Resources.Row + " : " + (i + 1).ToString() + ", " + ParcelrateResources.Resources.Field + " : Email, " + ParcelrateResources.Resources.Message + " : " + ParcelrateResources.Resources.Valid_Email + Environment.NewLine);
                    }
                    if (isRecordValid)
                    {
                        DataRow dr = validAddresses.NewRow();
                        dr.ItemArray = dtAddress.Rows[i].ItemArray;
                        validAddresses.Rows.Add(dr);
                    }
                    isRecordValid = true;
                }
                if (validAddresses.Rows.Count != dtAddress.Rows.Count)
                    errorString = stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(nameof(AddressController), nameof(ValidateRow), ex));
            }
        }
    }
}