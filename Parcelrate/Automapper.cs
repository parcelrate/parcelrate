﻿using AutoMapper;
using Parcelrate.Models;
using ParcelrateEntities;

namespace Parcelrate
{
    public class Automapper : Profile
    {
        public Automapper()
        {
            CreateMap<AddressViewModel, AddressEntity>();
            CreateMap<AddressEntity, AddressViewModel>();
            CreateMap<Address, LetMeShip.Address>();
            CreateMap<Address, AllPacka.DataContainer.Request.AddressModel>();
            CreateMap<ParcelModel, LetMeShip.ParcelModel>();
            CreateMap<ParcelModel, AllPacka.ParcelModel>();
            CreateMap<CarrierModel, LetMeShip.CarrierModel>();
            CreateMap<CarrierModel, AllPacka.CarrierModel>();
            CreateMap<ShipmentRequestViewModel, LetMeShip.CreateShipmentRequestModel>();
            CreateMap<ShipmentRequestViewModel, AllPacka.CreateShipmentRequestModel>();
        }
    }
}
