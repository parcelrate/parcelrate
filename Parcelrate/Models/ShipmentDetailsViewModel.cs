
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ParcelrateUtilities;

namespace Parcelrate.Models
{
    public class ShipmentDetailsViewModel : AddressViewModel
    {
        public string ShippedFromFirstName { get; set; }
        [Required(ErrorMessageResourceName = "Required_LastName", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string ShippedFromLastName { get; set; }
        [Required(ErrorMessageResourceName = "Required_Street", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string ShippedFromAddress1 { get; set; }
        [Required(ErrorMessageResourceName = "Required_HouseNumber", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        [MaxLength(7, ErrorMessageResourceName = "Address_Length", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string ShippedFromAddress2 { get; set; }
        public string ShippedFromCompany { get; set; }
        [Required(ErrorMessageResourceName = "Required_City", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string ShippedFromCity { get; set; }
        [Required(ErrorMessageResourceName = "Required_PostalCode", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string ShippedFromPostalCode { get; set; }
        [Required(ErrorMessageResourceName = "Required_PhoneNo", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        [RegularExpression(Constants.DigitValidationRegex, ErrorMessageResourceName = "Valid_Phone_No", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string ShippedFromPhoneNo { get; set; }
        [Required(ErrorMessageResourceName = "Required_Email", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        [EmailAddress(ErrorMessageResourceName = "Valid_Email", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string ShippedFromEmail { get; set; }
        [Required(ErrorMessageResourceName = "Required_Country", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public long? ShippedFromCountryId { get; set; }
        public string ShippedFromCountryCode { get; set; }
        public string ShippedFromPhonePrefix { get; set; }
        public bool IsSaveAsDefaultAddress { get; set; }
        public bool IsSaveAddressToDb { get; set; }
        public string ShipperReference { get; set; }
        [Required(ErrorMessageResourceName = "Required_Description", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string Description { get; set; }
        [Required(ErrorMessageResourceName = "Required_GoodsValue", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        [RegularExpression(Constants.DecimalValidationRegex, ErrorMessageResourceName = "Valid_Value", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string ValueOfGoods { get; set; }
        public string ShipmentType { get; set; } = Constants.ParcelText;
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMMM yyyy}")]
        public DateTime PickupDate { get; set; }
        public DateTime PickupTimeFrom { get; set; }
        public DateTime PickupTimeTo { get; set; }
        [Required(ErrorMessageResourceName = "Required_Agree", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public bool IsAgreeTermsConditions { get; set; }
        public long DeliveryAddressID { get; set; }
        public bool IsDefaultAddressExist { get; set; }

        public ShipmentDetailsViewModel()
        {
            PickupDate = DateTime.Now;
            PickupTimeTo = DateTime.Parse(new TimeSpan(18, 00, 00).ToString());
        }
    }
}