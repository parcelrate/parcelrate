using LetMeShip;
using System;
using System.Collections.Generic;

namespace Parcelrate.Models
{
    public class ShipmentRequestViewModel
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public Address PickupAddress { get; set; }
        public Address DeliveryAddress { get; set; }
        public List<ParcelModel> Parcels { get; set; }
        public string ContentDescription { get; set; }
        public decimal GoodsValue { get; set; }
        public bool GoodsValueSpecified { get; set; }
        public DateTime PickupInterval_Date { get; set; }
        public DateTime PickupTimeFrom { get; set; }
        public DateTime PickupTimeTo { get; set; }
        public string Culture { get; set; }
        public bool IsPickupOrder { get; set; }
        public CarrierModel SelectedService { get; set; }
        public decimal TotalPrice { get; set; }
        public int CarrierId { get; set; }
        public int ServiceId { get; set; }
        public string ShipperReference { get; set; }
        public bool IsBedalConnected { get; set; }
    }

    public class Address
    {
        public long CountryId { get; set; }
        public string CountryCode { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string PhonePrefix { get; set; }
        public string Email { get; set; }
        public string Company { get; set; }
    }

    public class ParcelModel
    {
        public string ShipmentType { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public decimal Weight { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
        public string GoodsValue { get; set; }
    }

    public class CarrierModel
    {
        public int Id { get; set; }
        public string ServiceName { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public DateTime CutoffTime { get; set; }
        public bool CutoffTimeSpecified { get; set; }
        public string Description { get; set; }
        public List<string> Messages { get; set; }
        public string TransferTime { get; set; }
        public PriceInfoResponseModel LetMeShipPriceInfo { get; set; }
        public List<SurchargeModel> Surcharges { get; set; }
        public int ServiceId { get; set; }
        public List<Attribute> AllPackaMessages { get; set; }
        public AllPacka.PriceInfoResponseModel PriceInfo { get; set; }
        public bool DoesProvideLabel { get; set; }
        public bool IsLetMeShip { get; set; }
    }
}