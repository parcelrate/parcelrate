using System.ComponentModel.DataAnnotations;
using ParcelrateUtilities;

namespace Parcelrate.Models
{
    public class AddressViewModel
    {
        public long Id { get; set; }
        [Required(ErrorMessageResourceName = "Required_CustomerID", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string CustomerID { get; set; }
        public string FirstName { get; set; }
        [Required(ErrorMessageResourceName = "Required_LastName", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string LastName { get; set; }
        [Required(ErrorMessageResourceName = "Required_Street", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string Address1 { get; set; }
        [Required(ErrorMessageResourceName = "Required_HouseNumber", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        [MaxLength(7, ErrorMessageResourceName = "Address_Length", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string Address2 { get; set; }
        public string Company { get; set; }
        [Required(ErrorMessageResourceName = "Required_City", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string City { get; set; }
        [Required(ErrorMessageResourceName = "Required_PostalCode", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string PostalCode { get; set; }
        [Required(ErrorMessageResourceName = "Required_PhoneNo", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(Constants.DigitValidationRegex, ErrorMessageResourceName = "Valid_Phone_No", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string PhoneNo { get; set; }
        [Required(ErrorMessageResourceName = "Required_Email", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        [EmailAddress(ErrorMessageResourceName = "Valid_Email", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string Email { get; set; }
        [Required(ErrorMessageResourceName = "Required_Country", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public long? CountryId { get; set; }
        public string CountryCode { get; set; }
        public string PhonePrefix { get; set; }
        public bool IsDefault { get; set; }
        public bool IsPickupOrder { get; set; }
    }
}