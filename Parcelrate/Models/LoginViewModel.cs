
using System.ComponentModel.DataAnnotations;
namespace Parcelrate.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessageResourceName = "Required_Country", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public long? CountryId { get; set; }
        [Required(ErrorMessageResourceName = "Required_Provider", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public long? ProviderId { get; set; }
        [Required(ErrorMessageResourceName = "Required_Username", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string Username { get; set; }
        [Required(ErrorMessageResourceName = "Required_Password", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public bool IsP4DProvider { get; set; }
        public string Culture { get; set; }
    }
}