﻿using System.ComponentModel.DataAnnotations;
using ParcelrateUtilities;

namespace Parcelrate.Models
{
    public class SignUpViewModel
    {
        [Required(ErrorMessageResourceName = "Required_Country", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public long? CountryId { get; set; }
        public string CountryName { get; set; }
        [Required(ErrorMessageResourceName = "Required_BusinessName", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string BusinessName { get; set; }
        [Required(ErrorMessageResourceName = "Required_Name", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string Name { get; set; }
        [Required(ErrorMessageResourceName = "Required_Email", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        [EmailAddress(ErrorMessageResourceName = "Valid_Email", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string Email { get; set; }
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(Constants.DigitValidationRegex, ErrorMessageResourceName = "Valid_Phone_No", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string PhoneNo { get; set; }
        [Required(ErrorMessageResourceName = "Required_ContactMeFor", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public long? ContactMeForId { get; set; }
        public string ContactMeFor { get; set; }
        public string Culture { get; set; }
        public string Message { get; set; }
    }
}
