
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using ParcelrateUtilities;
namespace Parcelrate.Models
{
    public class ProductDimensionsViewModel
    {
        private double _result;
        private string _height;
        [Required(ErrorMessageResourceName = "Required_Height", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        [RegularExpression(Constants.DecimalValidationRegex, ErrorMessageResourceName = "Valid_Value", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string Height
        {
            get { return _height; }
            set { _height = double.TryParse(Regex.Replace(value, Constants.AlphabetWithSpaceRegex, "", RegexOptions.Compiled), out _result) ? Convert.ToString(Math.Round(Convert.ToDouble(Regex.Replace(value, Constants.AlphabetWithSpaceRegex, "", RegexOptions.Compiled)))) : Regex.Replace(value, Constants.AlphabetWithSpaceRegex, "", RegexOptions.Compiled); }
        }

        private string _width;
        [Required(ErrorMessageResourceName = "Required_Width", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        [RegularExpression(Constants.DecimalValidationRegex, ErrorMessageResourceName = "Valid_Value", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string Width
        {
            get { return _width; }
            set { _width = double.TryParse(Regex.Replace(value, Constants.AlphabetWithSpaceRegex, "", RegexOptions.Compiled), out _result) ? Convert.ToString(Math.Round(Convert.ToDouble(Regex.Replace(value, Constants.AlphabetWithSpaceRegex, "", RegexOptions.Compiled)))) : Regex.Replace(value, Constants.AlphabetWithSpaceRegex, "", RegexOptions.Compiled); }
        }


        private string _length;
        [Required(ErrorMessageResourceName = "Required_Length", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        [RegularExpression(Constants.DecimalValidationRegex, ErrorMessageResourceName = "Valid_Value", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string Length
        {
            get { return _length; }
            set { _length = double.TryParse(Regex.Replace(value, Constants.AlphabetWithSpaceRegex, "", RegexOptions.Compiled), out _result) ? Convert.ToString(Math.Round(Convert.ToDouble(Regex.Replace(value, Constants.AlphabetWithSpaceRegex, "", RegexOptions.Compiled)))) : Regex.Replace(value, Constants.AlphabetWithSpaceRegex, "", RegexOptions.Compiled); }
        }


        private string _weight;
        [Required(ErrorMessageResourceName = "Required_Weight", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        [RegularExpression(Constants.DecimalValidationRegex, ErrorMessageResourceName = "Valid_Value", ErrorMessageResourceType = typeof(ParcelrateResources.Resources))]
        public string Weight
        {
            get { return _weight; }
            set { _weight = Regex.Replace(value, Constants.AlphabetWithSpaceRegex, "", RegexOptions.Compiled); }
        }

        public bool IsInputManually { get; set; }
        public bool IsBedalConnected { get; set; }
    }
}