$(document).ready(function () {
    /*show password*/
    $('.hide-pass').hide();
    $('.show-password').on('click', function () {
        var getAttr = $(this).parent('.form-group').find('.password-textbox').attr('type');
        if (getAttr === 'password') {
            $(this).parent('.form-group').find('.password-textbox').attr('type', 'text');
            $('.show-pass').hide();
            $('.hide-pass').show();
        } else {
            $(this).parent('.form-group').find('.password-textbox').attr('type', 'password');
            $('.show-pass').show();
            $('.hide-pass').hide();
        }
    });

    /*date picker*/
    $('.datepicker').datepicker({
        startDate: new Date(),
        autoclose: true,
        format: "dd MM yyyy",
        daysOfWeekDisabled: [0, 6]
    });
});

function getCountryData(e) {
    $.ajax({
        type: 'POST',
        url: '/Home/GetCountryData',
        dataType: 'json',
        data: { countryID: $(e).val() },
        success: function (response) {
            $("#txtCountryCode").val(response.code);
            $("#txtPhonePrefix").val(response.phonePrefix);
            $("#CountryCode").val(response.code);
            $("#PhonePrefix").val(response.phonePrefix);
        },
        error: function (ex) {
            console.log('Failed to retrieve country code and phone prefix' + ex);
        }
    });
    return false;
};

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hour = d.getHours(),
        minutes = d.getMinutes(),
        seconds = d.getSeconds();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day, hour, minutes, seconds].join('');
}

function DownloadTextFile(message) {
    

    if (window.navigator.msSaveOrOpenBlob && window.Blob) {
        var blob = new Blob([message], { type: "text/plain;charset=utf-8;" });
        navigator.msSaveOrOpenBlob(blob, 'ErrorDetails_' + formatDate(new Date()) + '.txt');
    }
    else {
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(message));
        element.setAttribute('download', 'ErrorDetails_' + formatDate(new Date()) + '.txt');
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }    

}

function SelectFile() {
    var commonFunction = function (e) {
        var formData = new FormData();
        showLoader();
        formData.append('file', $('#files')[0].files[0]);
        $.ajax({
            type: "POST",
            url: "/Address/UploadFile",
            contentType: false,
            processData: false,
            data: formData,
            success: function (response) {
                hideLoader();
                if (response) {
                    if (response.isSuccess) {

                        if (response.errorMessages != "") {
                            showMessagePopup(response.message);
                        }
                        else {
                            ShowAddressMessagePopup(response.message);
                        }

                    }
                    else {
                        showMessagePopup(response.error);
                    }
                    if (response.errorMessages != "") {
                        DownloadTextFile(response.errorMessages);
                    }
                }
            },
            error: function () {
                hideLoader();
                showMessagePopup("Error occurred on uploading csv file!");
            }
        });
        $("#divUpload").html('');
        $("#divUpload").html('<input type="file" accept=".csv" name="files" id="files" />');
    }

    var commonValidation = function () {
        showMessagePopup("Invalid file selected, valid files are of " +
            validExts.toString() + " types.");
    }

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    var validExts = new Array(".csv");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        $("#files").on("change", function (e) {
            var fileExt = this.value;
            fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
            if (validExts.indexOf(fileExt) < 0) {
                commonValidation();
                return false;
            }
            else {
                commonFunction(e);
            }
        }).click();
    }
    else {
        $("#files").click().change(function (e) {
            var fileExt = this.value;
            fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
            if (validExts.indexOf(fileExt) < 0) {
                commonValidation();
                return false;
            }
            else {
                commonFunction(e);
            }
        });
    }
}

function downloadSampleCSV() {
    showLoader();
    $.ajax({
        type: "POST",
        url: "/Address/DownloadSampleCSV",
        success: function (response) {
            hideLoader();
            if (response) {
                if (response.isSuccess == true)
                    window.location = response.url;
                else
                    showMessagePopup(response.error);
            }
        },
        error: function () {
            hideLoader();
            console.log("Error occurred on downloading sample csv file!");
        }
    });
}

function showPDF(shipmentID, isNewShipment) {
    showLoader();
    if (isNewShipment === true)
        shipmentID = newShipmentID;
    $.ajax({
        type: 'POST',
        url: '/Shipment/ViewPDF',
        dataType: 'json',
        data: { shipmentID: shipmentID },
        success: function (response) {
            hideLoader();
            if (response) {
                if (isNewShipment === true) {
                    newShipmentID = 0;
                    $("#divThankYou").slideUp();
                    $("body").removeClass('layout-hide');
                }
                if (response.isSuccess)
                    window.location = response.url;
                else {
                    if (isNewShipment)
                        showShipmentMessagePopup(response.error);
                    else
                        showMessagePopup(response.error);
                }
            }
        },
        error: function (ex) {
            hideLoader();
            console.log('Some error occurred on generate pdf' + ex);
        }
    });
}

function refreshNewShipment() {
    localStorage.removeItem("ShippingDetails");
    localStorage.removeItem("ProductDimensions");
    shipmentRequestModel = {};
}

function showLoader(loaderText) {
    $("body").addClass("layout-hide");
    if (loaderText && loaderText != '')
        $("#spanLoadingText").text(loaderText)
    $("#divLoader").show();
}

function hideLoader() {
    $("body").removeClass("layout-hide");
    $("#divLoader").hide();
}

function closeMessagePopup() {
    $("#divMessage").slideUp();
    $("#errorMessage").text('');
    $("body").removeClass('layout-hide');
}

function showMessagePopup(msg) {
    $("#divMessage").slideDown();
    $("#errorMessage").text(msg);
    $("body").addClass('layout-hide');
}

function showDataLostMessagePopup() {
    $("#divDataLostMessage").slideDown();
    $("body").addClass('layout-hide');
}

function closeDataLostMessagePopup() {
    $("#divDataLostMessage").slideUp();
    $("body").removeClass('layout-hide');
}

function successAction(url) {
    closeDataLostMessagePopup();
    var loc = window.location;
    var strArray = loc.pathname.split("/");
    setTimeout(function () {
        if (url != undefined) {
            location.pathname = url;
        }
        else {
            if (strArray[1] == "Address") {
                location.pathname = "/Address";
            }
            else {
                window.location.href = "Index";
            }
        }
    }, 500);
}

function cancelAction() {
    closeDataLostMessagePopup();
}

function checkHasChanges() {
    var hasChanges = false;
    $(":input:not(:button):not([type=hidden])").each(function () {
        if ((this.type == "text" || this.type == "textarea" || this.type == "hidden") && this.defaultValue != this.value) {
            hasChanges = true;
            return false;
        }
        else {
            if ((this.type == "radio" || this.type == "checkbox") && this.defaultChecked != this.checked) {
                hasChanges = true;
                return false;
            }
            else {
                if ((this.type == "select-one" || this.type == "select-multiple")) {
                    for (var x = 0; x < this.length; x++) {
                        if (this.options[x].selected != this.options[x].defaultSelected) {
                            hasChanges = true;
                            return false;
                        }
                    }
                }
            }
        }
    });
    return hasChanges;
}

function getHasChanges(url) {
    if (checkHasChanges()) {
        if (url != undefined) {
            $("#btnDataLostSuccess").attr("onclick", "successAction('" + url + "')");
        }
        showDataLostMessagePopup();
    }
    else {
        successAction(url);
    }
}

function checkHasChangesNewShipment() {
    var hasChanges = false;
    $(":input:not(:button):not([type=hidden]):not([name=PickupDate]):not([name=PickupTimeFrom]):not([name=PickupTimeTo])").each(function () {
        if ((this.type == "text" || this.type == "textarea" || this.type == "hidden") && this.defaultValue != this.value) {
            hasChanges = true;
            return false;
        }
        else {
            if ((this.type == "radio" || this.type == "checkbox") && this.defaultChecked != this.checked) {
                hasChanges = true;
                return false;
            }
            else {
                if ((this.type == "select-one" || this.type == "select-multiple")) {
                    var isValidate = true;
                    if ($("#hdn_IsDefault").val() == "True" && this.id == "ShippedFromCountryId") { isValidate = false; }
                    if (isValidate) {
                        for (var x = 0; x < this.length; x++) {
                            if (this.options[x].selected != this.options[x].defaultSelected) {
                                hasChanges = true;
                                return false;
                            }
                        }
                    }
                }
            }
        }
    });
    return hasChanges;
}

function getHasChangesNewShipment(url) {
    if (checkHasChangesNewShipment()) {
        if (url != undefined) {
            $("#btnDataLostSuccess").attr("onclick", "successAction('" + url + "')");
        }
        showDataLostMessagePopup();
    }
    else {
        successAction(url);
    }
}

function checkHasChangesEditAddress() {
    var hasChanges = false;
    $(":input:not(:button):not([type=hidden])").each(function () {
        if ((this.type == "text" || this.type == "textarea" || this.type == "hidden") && this.defaultValue != this.value) {
            if (this.value != $("#hdn_" + this.id).val()) {
                hasChanges = true;
                return false;
            }
        }
    });
    return hasChanges;
}

function getHasChangesEditAddress(url) {
    if (checkHasChangesEditAddress()) {
        if (url != undefined) {
            $("#btnDataLostSuccess").attr("onclick", "successAction('" + url + "')");
        }
        showDataLostMessagePopup();
    }
    else {
        successAction(url);
    }
}

function ShowAddressMessagePopup(msg) {
    $("#errorMessageAddress").text(msg);
    $("#divAddressMessage").slideDown();
    $("body").addClass('layout-hide');
}


function CloseAddressMessagePopup() {
    $("#errorMessageAddress").text('');
    $("#divAddressMessage").slideUp();
    $("body").removeClass('layout-hide');
    setTimeout(function () {
        location.href = "/Address/Index";
    }, 500);
}

function closeShipmentMessagePopup() {
    $("#divShipmentMessage").slideUp();
    $("#shipmentMessage").text('');
    $("body").removeClass('layout-hide');
    window.location.href = "/Shipment/Index";
}

function showShipmentMessagePopup(msg) {
    newShipmentID = 0;
    $("#divShipmentMessage").slideDown();
    $("#shipmentMessage").text(msg);
    $("body").addClass('layout-hide');
}

function showMessagePopupAndUpdateRedirectUrl(msg, url) {
    $("#divMessage").slideDown();
    $("#errorMessage").text(msg);
    $("body").addClass('layout-hide');
    if (url != undefined) {
        $("#divMessage .confirm-button").attr("onclick", "closeMessagePopupAndRedirect('" + url + "')");
    }
}

function closeMessagePopupAndRedirect(url) {
    closeMessagePopup();
    var loc = window.location;
    var strArray = loc.pathname.split("/");
    setTimeout(function () {
        if (url != undefined) {
            location.pathname = url;
        }
    }, 500);
}
