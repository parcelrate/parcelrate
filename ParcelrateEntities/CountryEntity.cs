﻿
namespace ParcelrateEntities
{
    public class CountryEntity
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string PhonePrefix { get; set; }
        public string Currency { get; set; }
        public string GermanName { get; set; }
    }
}
