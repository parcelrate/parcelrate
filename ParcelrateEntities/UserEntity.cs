﻿using System;

namespace ParcelrateEntities
{
    public class UserEntity
    {
        public long ID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public long ProviderID { get; set; }
        public long CountryID { get; set; }
        public string Culture { get; set; }
        public bool IsRememberMe { get; set; }
    }
}
