﻿
namespace ParcelrateEntities
{
    public class AddressEntity
    {
        public long Id { get; set; }
        public string CustomerID { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string PhonePrefix { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Company { get; set; }
        public long CountryId { get; set; }
        public string CountryCode { get; set; }
        public bool IsDefault { get; set; }
        public bool IsPickupOrder{ get; set; }
        public string CountryName{ get; set; }
        public string CountryCurrency{ get; set; }
    }
}
