﻿using System;
using System.Collections.Generic;

namespace ParcelrateEntities.Models
{
    public partial class ServiceProvider
    {
        public ServiceProvider()
        {
            ProviderCountryAssociation = new HashSet<ProviderCountryAssociation>();
            Shipment = new HashSet<Shipment>();
            User = new HashSet<User>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }

        public ICollection<ProviderCountryAssociation> ProviderCountryAssociation { get; set; }
        public ICollection<Shipment> Shipment { get; set; }
        public ICollection<User> User { get; set; }
    }
}
