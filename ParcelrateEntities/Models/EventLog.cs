﻿using System;
using System.Collections.Generic;

namespace ParcelrateEntities.Models
{
    public partial class EventLog
    {
        public long Id { get; set; }
        public string LogLevel { get; set; }
        public string Message { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
