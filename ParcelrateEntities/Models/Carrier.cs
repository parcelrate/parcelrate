﻿using System;
using System.Collections.Generic;

namespace ParcelrateEntities.Models
{
    public partial class Carrier
    {
        public Carrier()
        {
            Shipment = new HashSet<Shipment>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
        public long AllPackaCarrierId { get; set; }
        public bool DoesProvideLabel { get; set; }
        public string ColorCode { get; set; }

        public ICollection<Shipment> Shipment { get; set; }
    }
}
