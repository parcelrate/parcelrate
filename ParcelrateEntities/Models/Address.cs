﻿using System;
using System.Collections.Generic;

namespace ParcelrateEntities.Models
{
    public partial class Address
    {
        public long Id { get; set; }
        public string CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Company { get; set; }
        public string City { get; set; }
        public long? CountryId { get; set; }
        public string CountryCode { get; set; }
        public string PhonePrefix { get; set; }
        public long? PhoneNumber { get; set; }
        public string PostalCode { get; set; }
        public string Email { get; set; }
        public bool IsDefault { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsPickupOrder { get; set; }
        public long UserId { get; set; }

        public Country Country { get; set; }
        public User User { get; set; }
    }
}
