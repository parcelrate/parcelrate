﻿using System;
using System.Collections.Generic;

namespace ParcelrateEntities.Models
{
    public partial class Shipment
    {
        public long Id { get; set; }
        public long ProviderId { get; set; }
        public string Carrier { get; set; }
        public string Reference { get; set; }
        public long ShipmentId { get; set; }
        public decimal TotalPrice { get; set; }
        public DateTime? PickUpDate { get; set; }
        public string DimensionsCaptured { get; set; }
        public long PickupCountryId { get; set; }
        public long DeliveryCountryId { get; set; }
        public long CarrierId { get; set; }
        public string Awbnumber { get; set; }
        public DateTime OrderBookedOn { get; set; }
        public string Status { get; set; }
        public string Service { get; set; }
        public long UserId { get; set; }

        public Carrier CarrierNavigation { get; set; }
        public Country DeliveryCountry { get; set; }
        public Country PickupCountry { get; set; }
        public ServiceProvider Provider { get; set; }
        public User User { get; set; }
    }
}
