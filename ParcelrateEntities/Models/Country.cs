﻿using System;
using System.Collections.Generic;

namespace ParcelrateEntities.Models
{
    public partial class Country
    {
        public Country()
        {
            Address = new HashSet<Address>();
            ProviderCountryAssociation = new HashSet<ProviderCountryAssociation>();
            ShipmentDeliveryCountry = new HashSet<Shipment>();
            ShipmentPickupCountry = new HashSet<Shipment>();
            User = new HashSet<User>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string PhonePrefix { get; set; }
        public string Currency { get; set; }
        public string GermanName { get; set; }

        public ICollection<Address> Address { get; set; }
        public ICollection<ProviderCountryAssociation> ProviderCountryAssociation { get; set; }
        public ICollection<Shipment> ShipmentDeliveryCountry { get; set; }
        public ICollection<Shipment> ShipmentPickupCountry { get; set; }
        public ICollection<User> User { get; set; }
    }
}
