﻿using System;
using System.Collections.Generic;

namespace ParcelrateEntities.Models
{
    public partial class ProviderCountryAssociation
    {
        public long Id { get; set; }
        public long CountryId { get; set; }
        public long ProviderId { get; set; }

        public Country Country { get; set; }
        public ServiceProvider Provider { get; set; }
    }
}
