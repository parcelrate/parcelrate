﻿using System;
using System.Collections.Generic;

namespace ParcelrateEntities.Models
{
    public partial class User
    {
        public User()
        {
            Address = new HashSet<Address>();
            Shipment = new HashSet<Shipment>();
        }

        public long UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public long ProviderId { get; set; }
        public long CountryId { get; set; }
        public string Culture { get; set; }

        public Country Country { get; set; }
        public ServiceProvider Provider { get; set; }
        public ICollection<Address> Address { get; set; }
        public ICollection<Shipment> Shipment { get; set; }
    }
}
