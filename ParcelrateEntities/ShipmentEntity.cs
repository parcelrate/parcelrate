﻿using System;

namespace ParcelrateEntities
{
    public class ShipmentEntity
    {
        public long ID { get; set; }
        public long ProviderID { get; set; }
        public string Carrier { get; set; }
        public string Service { get; set; }
        public string Reference { get; set; }
        public long ShipmentID { get; set; }
        public decimal TotalPrice { get; set; }
        public string Status { get; set; }
        public string OrderBookedOn { get; set; }
        public string PickupDate { get; set; }
        public DateTime PickupDateTime { get; set; }
        public string DimensionsCaptured { get; set; }
        public Nullable<long> PickupCountryId { get; set; }
        public Nullable<long> DeliveryCountryId { get; set; }
        public Nullable<long> CarrierId { get; set; }
        public bool IsLetMeShip { get; set; }
        public string Currency { get; set; }
        public bool IsCancelled { get; set; }
        public string AWBNumber { get; set; }
        public string OldAWBNumber { get; set; }
        public long UserId { get; set; }
        public CountryEntity PickupCountry { get; set; }
    }

    public class ShipmentCountry
    {
        public long ShipmentID { get; set; }
        public string CountryCode { get; set; }
    }
}
