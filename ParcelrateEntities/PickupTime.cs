﻿
using System;
namespace ParcelrateEntities
{
    public class PickupTime
    {
        public string Caption { get; set; }
        public DateTime Value { get; set; }
    }
}
