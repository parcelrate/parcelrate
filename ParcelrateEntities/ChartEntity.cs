﻿
using System;
using System.Collections.Generic;

namespace ParcelrateEntities
{
    public class ChartEntity : KeyValue
    {
        public string Color { get; set; }
    }

    public class KeyValue
    {
        public string Key { get; set; }
        public double Value { get; set; }
        public string Currency { get; set; }
        public string Content { get; set; }
    }

    public class JSChart
    {
        public JSChart()
        {
            labels = new List<string>();
            datasets = new List<DataPoints>();
        }
        public List<string> labels { get; set; }
        public List<DataPoints> datasets { get; set; }
    }

    public class JSPieChart
    {
        public JSPieChart()
        {
            labels = new List<string>();
            datasets = new List<PieDataPoints>();
        }
        public List<string> labels { get; set; }
        public List<PieDataPoints> datasets { get; set; }
    }

    public class ChartPoint
    {
        public Decimal x { get; set; }
        public string label { get; set; }
    }

    public class PieDataPoints
    {
        public PieDataPoints()
        {
            data = new List<string>();
            backgroundColor = new List<string>();
        }
        public string label { get; set; }
        public List<string> backgroundColor { get; set; }
        public List<string> data { get; set; }
    }

    public class DataPoints
    {
        public DataPoints()
        {
            data = new List<ChartPoint>();
            borderWidth = "1";
        }
        public string label { get; set; }
        public string backgroundColor { get; set; }
        public string borderColor { get; set; }
        public string borderWidth { get; set; }
        public string stack { get; set; }
        public List<ChartPoint> data { get; set; }
    }
}
