﻿using LetMeShip;
using System.Collections.Generic;

namespace ParcelrateEntities
{
    public class CarrierEntity
    {
        public long Id { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public string CutoffTime { get; set; }
        public bool CutoffTimeSpecified { get; set; }
        public string Description { get; set; }
        public string Messages { get; set; }
        public string TransferTime { get; set; }
        public AllPacka.PriceInfoResponseModel PriceInfo { get; set; }
        public LetMeShip.PriceInfoResponseModel LetMeShipPriceInfo { get; set; }
        public decimal FuelSurcharge { get; set; }
        public List<SurchargeModel> Surcharges { get; set; }
        public bool IsLetMeShip { get; set; }
        public string Logo { get; set; }
        public long AllPackaCarrierId { get; set; }
        public bool DoesProvideLabel { get; set; }
        public string Currency { get; set; }
        public string ColorCode { get; set; }
        public bool IsAvailable { get; set; }
    }
}
