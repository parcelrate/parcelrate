﻿
namespace ParcelrateEntities
{
    public class ServiceProviderEntity
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
    }
}
