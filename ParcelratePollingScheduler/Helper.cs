﻿using LetMeShip;
using Microsoft.Extensions.Logging;
using ParcelrateComponent.Interfaces;
using ParcelrateEntities;
using ParcelrateUtilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Linq;

namespace ParceleratePollingScheduler
{
    public class Helper
    {
        private readonly IShipmentComponent _shipmentComponent;
        private readonly IUserComponent _userComponent;
        private readonly ICountryComponent _countryComponent;
        private readonly LetMeShip.Interfaces.IServices _lmsServices;
        private readonly ILogger<Helper> _logger;
        private string _username = string.Empty;
        private string _password = string.Empty;


        public Helper(IShipmentComponent shipmentBAL, IUserComponent userBAL, ICountryComponent countryBAL, LetMeShip.Interfaces.IServices lmsServices, ILogger<Helper> logger)
        {
            _shipmentComponent = shipmentBAL;
            _userComponent = userBAL;
            _countryComponent = countryBAL;
            _lmsServices = lmsServices;
            _logger = logger;
        }
        public void GetShipmentsStatus()
        {
            var shipments = new List<LetMeShip.Shipment>();
            try
            {
                var users = _userComponent.GetUsers();
                if (users.Count > 0)
                {
                    foreach (var userInfo in users)
                    {
                        _logger.LogInformation(Constants.Messages.GetShipmentsStartInfo);
                        Console.WriteLine(Constants.Messages.GetShipmentsStartInfo);
                        var lmsShipments = _shipmentComponent.GetLetMeShipShipmentIDs(userInfo.ID);
                        _logger.LogInformation(Constants.Messages.GetShipmentsEndInfo);
                        Console.WriteLine(Constants.Messages.GetShipmentsEndInfo);
                        _username = userInfo.UserName;
                        _password = StaticFunctions.DecodeString(userInfo.Password);
                        if (lmsShipments.Count > 0)
                        {
                            var shipmentStateRequest = new ShipmentStateRequestModel()
                            {
                                UserName = _username,
                                PassWord = _password,
                                Shipments = lmsShipments.Select(x => new ShipmentCountryRequest { ShipmentID = x.ShipmentID, CountryCode = x.CountryCode }).ToList()
                            };
                            _logger.LogInformation(Constants.Messages.GetShipmentsStatusRequestStartInfo);
                            Console.WriteLine(Constants.Messages.GetShipmentsStatusRequestStartInfo);
                            var result = _lmsServices.GetShipmentsStatus(shipmentStateRequest);

                            if (result != null)
                            {
                                if (result.Shipments != null && result.Shipments.Count > 0)
                                {
                                    shipments = result.Shipments;
                                    GetShipmentLabels(shipments, userInfo.ID);
                                }
                                else if (result.Messages != null && result.Messages.Count > 0)
                                {
                                    result.Messages[0] = "- " + result.Messages[0];
                                    _logger.LogInformation(string.Join(Environment.NewLine + "- ", result.Messages.ToArray()));
                                    Console.WriteLine(string.Join(Environment.NewLine + "- ", result.Messages.ToArray()));
                                }
                                else
                                {
                                    _logger.LogInformation(Constants.Messages.NoShipmentFoundInfo);
                                    Console.WriteLine(Constants.Messages.NoShipmentFoundInfo);
                                }
                            }
                            else
                            {
                                _logger.LogInformation(Constants.Messages.ServerErrorInfo);
                                Console.WriteLine(Constants.Messages.ServerErrorInfo);
                            }
                            _logger.LogInformation(Constants.Messages.GetShipmentsStatusRequestEndInfo);
                            Console.WriteLine(Constants.Messages.GetShipmentsStatusRequestEndInfo);
                        }
                        else
                        {
                            _logger.LogInformation(Constants.Messages.NoShipmentPendingLabelFoundInfo);
                            Console.WriteLine(Constants.Messages.NoShipmentPendingLabelFoundInfo);
                        }
                    }
                }
                else
                {
                    _logger.LogInformation(Constants.Messages.NoUserFound);
                    Console.WriteLine(Constants.Messages.NoUserFound);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ParcelrateShipmentStateScheduler, Constants.MethodName.GetShipmentsStatus, ex));
                Console.WriteLine(Constants.Messages.ErrorInfo);
            }
        }

        private void GetShipmentLabels(List<LetMeShip.Shipment> shipments, long userID)
        {
            try
            {
                if (shipments != null && shipments.Count > 0)
                {
                    _logger.LogInformation(Constants.Messages.GetShipmentsLabelsStartInfo);
                    Console.WriteLine(Constants.Messages.GetShipmentsLabelsStartInfo);
                    foreach (var shipmnt in shipments)
                    {
                        if (shipmnt.LabelState == LetMeShip.Sandbox.labelState.labelAvailable)
                        {
                            _logger.LogInformation(Constants.Messages.ShipmentLabelReadyInfo, shipmnt.ShipmentID);
                            var shipment = _shipmentComponent.GetShipmentByID(shipmnt.ShipmentID);
                            Console.WriteLine(Constants.Messages.ShipmentLabelReadyInfo, shipmnt.ShipmentID);
                            GetLabel(shipmnt.ShipmentID, shipment.PickupCountry.Code);
                        }
                        UpdateShipmentLabelStatus(shipmnt.ShipmentID, shipmnt.ShipmentState, userID);
                    }
                    _logger.LogInformation(Constants.Messages.GetShipmentsLabelsEndInfo);
                    Console.WriteLine(Constants.Messages.GetShipmentsLabelsEndInfo);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ParcelrateShipmentStateScheduler, Constants.MethodName.GetShipmentLabels, ex));
                Console.WriteLine(Constants.Messages.ErrorInfo);
            }
        }

        private void GetLabel(long shipmentID, string countryCode)
        {
            var labelRequest = new GetLabelRequestModel()
            {
                UserName = _username,
                PassWord = _password,
                ShipmentId = shipmentID,
                SendLabel = true,
                SendSummary = false,
                CountryCode = countryCode
            };
            _logger.LogInformation(Constants.Messages.GetShipmentLabelStartInfo, shipmentID);
            Console.WriteLine(Constants.Messages.GetShipmentLabelStartInfo, shipmentID);
            var labelResponse = _lmsServices.GetLabel(labelRequest);
            if (labelResponse != null)
            {
                if (labelResponse.Status == Constants.DocumentReadyStatus)
                {
                    string filePath = string.Empty;
                    var user = _userComponent.GetUserByUsername(_username);
                    if (user != null)
                    {
                        var rootPath = Directory.GetParent(Directory.GetParent(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)).ToString());
                        filePath = rootPath + string.Concat(Constants.PdfPath + @"\" + user.ID, Constants.LetMeShipPdfFolder);
                        if (!Directory.Exists(filePath))
                            Directory.CreateDirectory(filePath);
                        string pdfPath = filePath + @"\" + shipmentID + Constants.PdfFileExtension;
                        if (!File.Exists(pdfPath))
                            File.WriteAllBytes(pdfPath, labelResponse.Data);
                    }
                }
                if (labelResponse.Messages != null && labelResponse.Messages.Count > 0)
                {
                    labelResponse.Messages[0] = "- " + labelResponse.Messages[0];
                    Console.WriteLine(string.Join(Environment.NewLine + "- ", labelResponse.Messages.ToArray()));
                }
            }
            else
            {
                _logger.LogInformation(Constants.Messages.ServerErrorInfo);
                Console.WriteLine(Constants.Messages.ServerErrorInfo);
            }
            _logger.LogInformation(Constants.Messages.GetShipmentsLabelsEndInfo, shipmentID);
            Console.WriteLine(Constants.Messages.GetShipmentsLabelsEndInfo, shipmentID);
        }

        private void UpdateShipmentLabelStatus(long shipmentID, LetMeShip.Sandbox.shipmentState shipmentStatus, long userID)
        {
            var shipmentEntity = new ShipmentEntity { ShipmentID = shipmentID, IsLetMeShip = true, Status = shipmentStatus.ToString(), UserId = userID };
            _shipmentComponent.UpdateShipmentLabelStatus(shipmentEntity);
        }
    }
}
