﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Config;
using NLog.Extensions.Logging;
using ParceleratePollingScheduler;
using ParcelrateComponent;
using ParcelrateComponent.Interfaces;
using ParcelrateDataAccess;
using ParcelrateDataAccess.Interfaces;
using ParcelrateEntities.Models;
using ParcelrateUtilities;
using System;
using System.IO;
using System.Reflection;

namespace ParcelratePollingScheduler
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine(Constants.Messages.ShipmentStatusSchedulerStartInfo);
            ConfigureServices();
            Console.WriteLine(Constants.Messages.ShipmentStatusSchedulerEndInfo);
        }
        
        private static void ConfigureServices()
        {
            IConfiguration config = new ConfigurationBuilder()
              .AddJsonFile("appsettings.json", true, true)
              .Build();
            
            IServiceCollection services = new ServiceCollection();
            services.AddDbContext<ParcelrateDbEntities>(options =>
                     options.UseSqlServer(config.GetConnectionString("ParcelrateDbEntities")));
            ILoggerFactory loggerFactory = new LoggerFactory()
                .AddConsole();

            services.AddSingleton(loggerFactory);
            services.AddLogging();

            // Support typed Options
            services.AddOptions();
            services.AddScoped<IUserDataAccess, UserDataAccess>();
            services.AddScoped<IShipmentDataAccess, ShipmentDataAccess>();
            services.AddScoped<ICountryDataAccess, CountryDataAccess>();
            services.AddScoped<ICarrierDataAccess, CarrierDataAccess>();
            services.AddScoped<IServiceProviderDataAccess, ServiceProviderDataAccess>();
            services.AddScoped<IUserComponent, UserComponent>();
            services.AddScoped<IShipmentComponent, ShipmentComponent>();
            services.AddScoped<ICountryComponent, CountryComponent>();
            services.AddScoped<LetMeShip.Interfaces.IAvailableServices, LetMeShip.AvailableServices>();
            services.AddScoped<LetMeShip.Interfaces.IShipmentServices, LetMeShip.ShipmentServices>();
            services.AddScoped<LetMeShip.Interfaces.ILabelServices, LetMeShip.LabelServices>();
            services.AddScoped<LetMeShip.Interfaces.ITrackingServices, LetMeShip.TrackingServices>();
            services.AddScoped<LetMeShip.Interfaces.IServices, LetMeShip.Services>();
            services.AddTransient<Helper>();
            services.AddSingleton(config);

            IServiceProvider serviceProvider = services.BuildServiceProvider();

            var factory = serviceProvider.GetService<ILoggerFactory>();
            factory.AddNLog();

            var rootPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            LogManager.Configuration = new XmlLoggingConfiguration(rootPath + "\\nlog.config");

            if (Convert.ToBoolean(config.GetSection("IsDBLoggingEnabled").Value))
                LogManager.Configuration.Variables["connectionString"] = config.GetConnectionString("ParcelrateDbEntities");
            else
                LogManager.Configuration.Variables["configDir"] = rootPath + "\\logs\\" + DateTime.UtcNow.ToString("MMM-dd-yyyy");

            var logger = serviceProvider.GetService<ILogger<Program>>();
            
            logger.LogInformation(Constants.Messages.ShipmentStatusSchedulerStartInfo);
            var service = serviceProvider.GetService<Helper>();
            service.GetShipmentsStatus();
            logger.LogInformation(Constants.Messages.ShipmentStatusSchedulerEndInfo);
        }
    }
}
