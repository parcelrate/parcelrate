﻿using Microsoft.EntityFrameworkCore;
using ParcelrateDataAccess.Interfaces;
using ParcelrateEntities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using ParcelrateUtilities;

namespace ParcelrateDataAccess
{
    public class ShipmentDataAccess : IShipmentDataAccess
    {
        private readonly ParcelrateDbEntities ctx;
        public ShipmentDataAccess(ParcelrateDbEntities context)
        {
            ctx = context;
        }

        /// <summary>
        /// Save Shipment
        /// </summary>
        /// <param name="shipmentEntity">shipmentEntity</param>
        /// <returns></returns>
        public long SaveShipment(Shipment shipmentEntity)
        {
            if (shipmentEntity != null)
            {
                var existingShipment = ctx.Shipment.FirstOrDefault(s => s.ShipmentId == shipmentEntity.ShipmentId && s.UserId == shipmentEntity.UserId);
                if (existingShipment != null)
                {
                    existingShipment.Status = shipmentEntity.Status;
                    ctx.Entry(existingShipment).State = EntityState.Modified;
                }
                else
                    ctx.Shipment.Add(shipmentEntity);
                ctx.SaveChanges();
                return shipmentEntity.Id;
            }
            return 0;
        }

        /// <summary>
        /// Update Shipment Label Status
        /// </summary>
        /// <param name="shipment">shipment</param>
        /// <returns></returns>
        public bool UpdateShipmentLabelStatus(Shipment shipment)
        {
            var existingShipment = ctx.Shipment.FirstOrDefault(s => s.ShipmentId == shipment.ShipmentId && s.ProviderId == shipment.ProviderId && s.UserId == shipment.UserId);
            if (existingShipment != null)
            {
                existingShipment.Status = shipment.Status;
                ctx.Entry(existingShipment).State = EntityState.Modified;
            }
            return Convert.ToBoolean(ctx.SaveChanges());
        }

        /// <summary>
        /// Get Shipments
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<Shipment> GetShipmentsByProvider(long providerID, long userID)
        {
            return ctx.Shipment.Where(s => s.ProviderId == providerID && s.UserId == userID).ToList();
        }

        /// <summary>
        /// Get Shipment based on id
        /// </summary>
        /// <param name="shipmentID"></param>
        /// <returns></returns>
        public Shipment GetShipmentByID(long shipmentID)
        {
            return ctx.Shipment.Include(s => s.PickupCountry).FirstOrDefault(s => s.ShipmentId == shipmentID);
        }

        /// <summary>
        /// Get LMS Shipments
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<Shipment> GetLetMeShipShipments(long userID)
        {
            return ctx.Shipment.Include(s => s.PickupCountry).Where(s => s.UserId == userID && s.ProviderId == ctx.ServiceProvider.FirstOrDefault(p => p.Name.ToLower().Contains(Constants.LetMeShipText)).Id).ToList();
        }

        /// <summary>
        /// Update Shipment AWB Number
        /// </summary>
        /// <param name="shipment">shipment</param>
        /// <returns></returns>
        public bool UpdateShipmentAWBNumber(Shipment shipment)
        {
            var existingShipment = ctx.Shipment.FirstOrDefault(s => s.Id == shipment.Id);
            if (existingShipment != null)
            {
                existingShipment.Awbnumber = shipment.Awbnumber;
                ctx.Entry(existingShipment).State = EntityState.Modified;
            }
            return Convert.ToBoolean(ctx.SaveChanges());
        }

        /// <summary>
        /// Get Shipment Query
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public IQueryable<Shipment> GetShipmentsQuery(long providerID, long userID)
        {
            return ctx.Shipment.Include(s => s.PickupCountry).Include(s => s.DeliveryCountry).Include(x => x.Provider).Where(s => s.ProviderId == providerID && s.UserId == userID);
        }
    }
}
