﻿using ParcelrateDataAccess.Interfaces;
using ParcelrateEntities;
using ParcelrateEntities.Models;
using System.Collections.Generic;
using System.Linq;

namespace ParcelrateDataAccess
{
    public class UserDataAccess : IUserDataAccess
    {
        private readonly ParcelrateDbEntities ctx;
        public UserDataAccess(ParcelrateDbEntities context)
        {
            ctx = context;
        }

        /// <summary>
        /// Get User login Info
        /// /// </summary>
        /// <returns></returns>
        public List<User> GetUsers()
        {
            return ctx.User.ToList();
        }

        /// <summary>
        /// Add User Info
        /// </summary>
        ///<param name="userInfo"></param>
        /// <returns></returns>
        public bool AddUserInfo(UserEntity userInfo)
        {
            var existingUserInfo = ctx.User.FirstOrDefault(u => u.Username.Trim() == userInfo.UserName.Trim());
            if (existingUserInfo == null)
            {
                ctx.User.Add(new User
                {
                    Username = userInfo.UserName,
                    Password = userInfo.Password,
                    ProviderId = userInfo.ProviderID,
                    CountryId = userInfo.CountryID,
                    Culture = userInfo.Culture
                });
            }
            else
            {
                existingUserInfo.Username = userInfo.UserName;
                existingUserInfo.Password = userInfo.Password;
                existingUserInfo.ProviderId = userInfo.ProviderID;
                existingUserInfo.CountryId = userInfo.CountryID;
                existingUserInfo.Culture = userInfo.Culture;
            }
            ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// Delete User Info
        /// </summary>
        /// <returns></returns>
        public bool DeleteUserInfo()
        {
            var existingUserInfo = ctx.User.FirstOrDefault();
            if (existingUserInfo != null)
            {
                ctx.SaveChanges();
                return true;
            }
            return false;
        }

        /// <summary>
        /// get User Info based on username
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public User GetUserByUsername(string userName)
        {
            return ctx.User.FirstOrDefault(u => u.Username == userName);
        }

    }
}
