﻿using ParcelrateEntities;
using System.Collections.Generic;
using System.Linq;
using ParcelrateEntities.Models;
using ParcelrateUtilities;
using ParcelrateDataAccess.Interfaces;

namespace ParcelrateDataAccess
{
    public class ServiceProviderDataAccess: IServiceProviderDataAccess
    {
        private readonly ParcelrateDbEntities ctx;
        public ServiceProviderDataAccess(ParcelrateDbEntities context)
        {
            ctx = context;
        }

        /// <summary>
        /// Get ServiceProviders based on country
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        public List<ServiceProvider> GetServiceProvidersByCountry(long countryID)
        {
            var providerList = new List<ServiceProvider>();
            if (countryID > 0)
            {
                var countryProviders = ctx.ProviderCountryAssociation.Where(sc => sc.CountryId == countryID);
                foreach (var provider in countryProviders)
                {
                    providerList.Add(new ServiceProvider
                    {
                        Id = provider.ProviderId,
                        Name = ctx.ServiceProvider.FirstOrDefault(c => c.Id == provider.ProviderId) != null ? ctx.ServiceProvider.FirstOrDefault(c => c.Id == provider.ProviderId).Name : string.Empty,
                        Logo = ctx.ServiceProvider.FirstOrDefault(c => c.Id == provider.ProviderId) != null ? ctx.ServiceProvider.FirstOrDefault(c => c.Id == provider.ProviderId).Logo : string.Empty,
                    });
                }
            }
            return providerList;
        }

        /// <summary>
        /// Get ServiceProvider based on providerId
        /// </summary>
        /// <param name="providerID"></param>
        /// <returns></returns>
        public ServiceProviderEntity GetProviderByID(long providerID)
        {
            var providerEntity = new ServiceProviderEntity();
            var provider = ctx.ServiceProvider.FirstOrDefault(s => s.Id == providerID);
            if (provider != null)
            {
                providerEntity.ID = providerID;
                providerEntity.Name = provider.Name;
                providerEntity.Logo = provider.Logo;
            }
            return providerEntity;
        }

        /// Get LetMeShip ProviderId
        /// </summary>
        /// <returns></returns>
        public long GetLetMeShipProviderID()
        {
            return ctx.ServiceProvider.FirstOrDefault(p => p.Name.ToLower().Contains(Constants.LetMeShipText)).Id;
        }
    }
}
