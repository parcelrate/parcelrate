﻿using ParcelrateEntities.Models;
using System.Collections.Generic;
using System.Linq;

namespace ParcelrateDataAccess.Interfaces
{
    public interface IAddressDataAccess
    {
        /// <summary>
        /// Get addresses
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        IQueryable<Address> GetAddresses(long userID);

        /// <summary>
        /// Get address based on address id
        /// </summary>
        /// <param name="addressID"></param>
        /// <returns></returns>
        Address GetAddressByID(long addressID);

        /// <summary>
        /// Get default address
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        Address GetDefaultAddress(long userID);

        /// <summary>
        /// Save Addresses
        /// </summary>
        /// <param name="addressEntityList">projectId</param>
        /// <returns></returns>
        bool SaveAddresses(List<Address> addressEntityList);

        /// <summary>
        /// Save Addresse
        /// </summary>
        /// <param name="addressEntity">addressEntity</param>
        /// <returns></returns>
        long SaveAddress(Address address);

        /// <summary>
        ///Delete  address based on address id
        /// </summary>
        /// <param name="addressID"></param>
        /// <returns></returns>
        bool DeleteAddress(long addressID);
        
    }
}
