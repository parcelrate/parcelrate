﻿using ParcelrateEntities.Models;
using System.Collections.Generic;
using System.Linq;

namespace ParcelrateDataAccess.Interfaces
{
    public interface IShipmentDataAccess
    {
        /// <summary>
        /// Save Shipment
        /// </summary>
        /// <param name="shipmentEntity">shipmentEntity</param>
        /// <returns></returns>
        long SaveShipment(Shipment shipmentEntity);

        /// <summary>
        /// Update Shipment Label Status
        /// </summary>
        /// <param name="shipment">shipment</param>
        /// <returns></returns>
        bool UpdateShipmentLabelStatus(Shipment shipment);

        /// <summary>
        /// Get Shipments
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        List<Shipment> GetShipmentsByProvider(long providerID, long userID);

        /// <summary>
        /// Get Shipment based on id
        /// </summary>
        /// <param name="shipmentID"></param>
        /// <returns></returns>
        Shipment GetShipmentByID(long shipmentID);

        /// <summary>
        /// Get LMS Shipments
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        List<Shipment> GetLetMeShipShipments(long userID);

        /// <summary>
        /// Update Shipment AWB Number
        /// </summary>
        /// <param name="shipment">shipment</param>
        /// <returns></returns>
        bool UpdateShipmentAWBNumber(Shipment shipment);

        /// <summary>
        /// Get Shipments Query
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        IQueryable<Shipment> GetShipmentsQuery(long providerID, long userID);
    }
}
