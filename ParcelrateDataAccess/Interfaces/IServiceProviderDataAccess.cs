﻿using ParcelrateEntities;
using ParcelrateEntities.Models;
using System.Collections.Generic;

namespace ParcelrateDataAccess.Interfaces
{
    public interface IServiceProviderDataAccess
    {
        /// <summary>
        /// Get ServiceProviders based on country
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        List<ServiceProvider> GetServiceProvidersByCountry(long countryID);

        /// <summary>
        /// Get ServiceProvider based on providerId
        /// </summary>
        /// <param name="providerID"></param>
        /// <returns></returns>
        ServiceProviderEntity GetProviderByID(long providerID);

        /// Get LetMeShip ProviderId
        /// </summary>
        /// <returns></returns>
        long GetLetMeShipProviderID();
    }
}
