﻿using ParcelrateEntities;
using ParcelrateEntities.Models;
using System.Collections.Generic;

namespace ParcelrateDataAccess.Interfaces
{
    public interface IUserDataAccess
    {
        /// <summary>
        /// Get User login Info
        /// /// </summary>
        /// <returns></returns>
        List<User> GetUsers();

        /// <summary>
        /// Add User Info
        /// </summary>
        ///<param name="userInfo"></param>
        /// <returns></returns>
        bool AddUserInfo(UserEntity userInfo);

        /// <summary>
        /// Delete User Info
        /// </summary>
        /// <returns></returns>
        bool DeleteUserInfo();

        /// <summary>
        /// get User Info based on username
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        User GetUserByUsername(string userName);
    }
}
