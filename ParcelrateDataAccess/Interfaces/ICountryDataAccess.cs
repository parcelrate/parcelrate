﻿using ParcelrateEntities.Models;
using System.Collections.Generic;

namespace ParcelrateDataAccess.Interfaces
{
    public interface ICountryDataAccess
    {
        /// <summary>
        /// Get Countries
        /// </summary>
        /// <returns></returns>
        List<Country> GetCountries();

        /// <summary>
        /// Get Countries based on ServiceProvider
        /// </summary>
        /// <param name="studentID"></param>
        /// <returns></returns>
        List<Country> GetCountriesByServiceProvider(long serviceProviderID);

        /// <summary>
        /// Get all providers Country list
        /// </summary>
        /// <returns></returns>
        List<Country> GetAllProvidersCountryList();

        /// <summary>
        /// Get Country based on id
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        Country GetCountryByID(long countryID);
    }
}
