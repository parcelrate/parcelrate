﻿using ParcelrateEntities.Models;
using System.Collections.Generic;

namespace ParcelrateDataAccess.Interfaces
{
    public interface ICarrierDataAccess
    {
        /// <summary>
        /// Get Carriers
        /// </summary>
        /// <returns></returns>
        List<Carrier> GetCarriers();

        /// <summary>
        /// Get Carrier based on carrier id
        /// </summary>
        /// <param name="carrierID"></param>
        /// <returns></returns>
        Carrier GetCarrierByID(long carrierID);

        /// <summary>
        /// Get Carrier based on carrier name
        /// </summary>
        /// <param name="carrierName"></param>
        /// <returns></returns>
        Carrier GetCarrierByName(string carrierName);

        /// <summary>
        /// Add carrier
        /// </summary>
        /// <param name="carrier"></param>
        /// <returns></returns>
        long AddCarrier(Carrier carrier);
    }
}
