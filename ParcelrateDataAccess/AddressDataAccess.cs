﻿using Microsoft.EntityFrameworkCore;
using ParcelrateDataAccess.Interfaces;
using ParcelrateEntities.Models;
using System.Collections.Generic;
using System.Linq;

namespace ParcelrateDataAccess
{
    public class AddressDataAccess : IAddressDataAccess
    {
        private readonly ParcelrateDbEntities ctx;
        public AddressDataAccess(ParcelrateDbEntities context)
        {
            ctx = context;
        }

        /// <summary>
        /// Get addresses
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public IQueryable<Address> GetAddresses(long userID)
        {
            return ctx.Address.Include(a => a.Country).Where(a => a.UserId == userID && !a.IsDeleted);
        }

        /// <summary>
        /// Get address based on address id
        /// </summary>
        /// <returns></returns>
        public Address GetAddressByID(long addressID)
        {
            return ctx.Address.Include(a => a.Country).FirstOrDefault(a => a.Id == addressID && !a.IsDeleted);
        }

        /// <summary>
        /// Get default address
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public Address GetDefaultAddress(long userID)
        {
            return ctx.Address.Include(a => a.Country).FirstOrDefault(a => a.UserId == userID && !a.IsDeleted && a.IsDefault);
        }

        /// <summary>
        /// Save Addresses
        /// </summary>
        /// <param name="addressEntityList">projectId</param>
        /// <returns></returns>
        public bool SaveAddresses(List<Address> addressEntityList)
        {
            if (addressEntityList.Count > 0)
            {
                foreach (var addr in addressEntityList)
                {
                    if (ctx.Address.Any(a => !a.IsDeleted && a.CustomerId.ToUpper() == addr.CustomerId.ToUpper() && a.UserId == addr.UserId))
                    {
                        var address = ctx.Address.FirstOrDefault(a => !a.IsDeleted && a.CustomerId.ToUpper() == addr.CustomerId.ToUpper() && a.UserId == addr.UserId);
                        address.CustomerId = addr.CustomerId;
                        address.Company = addr.Company;
                        address.FirstName = addr.FirstName;
                        address.LastName = addr.LastName;
                        address.Address1 = addr.Address1;
                        address.Address2 = addr.Address2;
                        address.CountryId = addr.CountryId;
                        address.CountryCode = addr.CountryCode;
                        address.PostalCode = addr.PostalCode;
                        address.PhonePrefix = addr.PhonePrefix;
                        address.PhoneNumber = addr.PhoneNumber;
                        address.City = addr.City;
                        address.Email = addr.Email;
                        address.IsDefault = addr.IsDefault;
                        address.IsPickupOrder = addr.IsPickupOrder;
                        address.UserId = addr.UserId;
                        ctx.Address.Update(address);
                    }
                    else
                        ctx.Address.Add(addr);
                    ctx.SaveChanges();
                }
                return true;
            }
            return false;
        }


        /// <summary>
        /// Save Addresse
        /// </summary>
        /// <param name="addressEntity">addressEntity</param>
        /// <returns></returns>
        public long SaveAddress(Address address)
        {
            if (address.IsDefault)
            {
                Address addressRecord = ctx.Address.Where(a => a.IsDefault && a.UserId == address.UserId && !a.IsDeleted).FirstOrDefault();
                if (addressRecord != null)
                    MapAddress(address, addressRecord);
                else
                    ctx.Address.Add(address);
            }
            else
            {
                Address addressRecord = ctx.Address.Where(a => a.CustomerId.ToUpper() == address.CustomerId.ToUpper() && a.UserId == address.UserId && a.Id != address.Id && !a.IsDeleted).FirstOrDefault();
                if (addressRecord != null)
                    return -1;
                else
                {
                    addressRecord = ctx.Address.Where(x => x.Id == address.Id && !x.IsDeleted).FirstOrDefault();
                    if (addressRecord != null)
                        MapAddress(address, addressRecord);
                    else
                        ctx.Address.Add(address);
                }
            }
            ctx.SaveChanges();
            return address.Id;
        }

        private void MapAddress(Address address, Address addressRecord)
        {
            addressRecord.Address1 = address.Address1;
            addressRecord.Address2 = address.Address2;
            addressRecord.City = address.City;
            addressRecord.Company = address.Company;
            addressRecord.CountryCode = address.CountryCode;
            addressRecord.CountryId = address.CountryId;
            addressRecord.Email = address.Email;
            addressRecord.FirstName = address.FirstName;
            addressRecord.IsDefault = address.IsDefault;
            addressRecord.IsDeleted = address.IsDeleted;
            addressRecord.IsPickupOrder = address.IsPickupOrder;
            addressRecord.LastName = address.LastName;
            addressRecord.PhoneNumber = address.PhoneNumber;
            addressRecord.PhonePrefix = address.PhonePrefix;
            addressRecord.PostalCode = address.PostalCode;
            addressRecord.CustomerId = address.CustomerId;
            addressRecord.UserId = address.UserId;
            ctx.Update(addressRecord);
        }

        /// <summary>
        ///Delete  address based on address id
        /// </summary>
        /// <param name="addressID"></param>
        /// <returns></returns>
        public bool DeleteAddress(long addressID)
        {
            bool result = false;
            var address = ctx.Address.FirstOrDefault(a => a.Id == addressID);
            if (address != null)
            {
                address.IsDeleted = true;
                ctx.Entry(address).State = EntityState.Modified;
                ctx.SaveChanges();
                result = true;
            }
            return result;
        }
    }
}
