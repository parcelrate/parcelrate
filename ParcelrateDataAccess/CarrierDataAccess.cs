﻿using ParcelrateDataAccess.Interfaces;
using ParcelrateEntities.Models;
using System.Collections.Generic;
using System.Linq;

namespace ParcelrateDataAccess
{
    public class CarrierDataAccess: ICarrierDataAccess
    {
        private readonly ParcelrateDbEntities ctx;
        public CarrierDataAccess(ParcelrateDbEntities context)
        {
            ctx = context;
        }

        /// <summary>
        /// Get Carriers
        /// </summary>
        /// <returns></returns>
        public List<Carrier> GetCarriers()
        {
            return ctx.Carrier.ToList();
        }

        /// <summary>
        /// Get Carrier based on carrier id
        /// </summary>
        /// <param name="carrierID"></param>
        /// <returns></returns>
        public Carrier GetCarrierByID(long carrierID)
        {
            return ctx.Carrier.FirstOrDefault(c => c.Id == carrierID);
        }

        /// <summary>
        /// Get Carrier based on carrier name
        /// </summary>
        /// <param name="carrierName"></param>
        /// <returns></returns>
        public Carrier GetCarrierByName(string carrierName)
        {
            return ctx.Carrier.FirstOrDefault(c => c.Name.ToUpper() == carrierName.ToUpper());
        }

        /// <summary>
        /// Add carrier
        /// </summary>
        /// <param name="carrier"></param>
        /// <returns></returns>
        public long AddCarrier(Carrier carrier)
        {
            ctx.Carrier.Add(carrier);
            ctx.SaveChanges();
            return carrier.Id;
        }
    }
}
