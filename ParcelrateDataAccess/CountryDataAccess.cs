﻿using Microsoft.EntityFrameworkCore;
using ParcelrateDataAccess.Interfaces;
using ParcelrateEntities.Models;
using System.Collections.Generic;
using System.Linq;

namespace ParcelrateDataAccess
{
    public class CountryDataAccess : ICountryDataAccess
    {
        private readonly ParcelrateDbEntities ctx;
        public CountryDataAccess(ParcelrateDbEntities context)
        {
            ctx = context;
        }

        /// <summary>
        /// Get Countries
        /// </summary>
        /// <returns></returns>
        public List<Country> GetCountries()
        {
            return ctx.Country.ToList();
        }

        /// <summary>
        /// Get Countries based on ServiceProvider
        /// </summary>
        /// <param name="studentID"></param>
        /// <returns></returns>
        public List<Country> GetCountriesByServiceProvider(long serviceProviderID)
        {
            var countryList = new List<Country>();
            if (serviceProviderID > 0)
            {
                var serviceProviderCountries = ctx.ProviderCountryAssociation.Include(x => x.Country).Where(sc => sc.ProviderId == serviceProviderID);
                foreach (var item in serviceProviderCountries)
                {
                    if (item.Country != null)
                    {
                        countryList.Add(new Country
                        {
                            Id = item.Country.Id,
                            Name = item.Country.Name,
                            Code = item.Country.Code,
                            PhonePrefix = item.Country.PhonePrefix,
                            Currency = item.Country.Currency,
                            GermanName = item.Country.GermanName
                        });
                    }
                }
            }
            return countryList;
        }

        /// <summary>
        /// Get all providers Country list
        /// </summary>
        /// <returns></returns>
        public List<Country> GetAllProvidersCountryList()
        {
            var countryList = new List<Country>();
            var serviceProviderCountries = ctx.ProviderCountryAssociation.Include(x => x.Country);
            foreach (var item in serviceProviderCountries)
            {
                if (item.Country != null)
                {
                    countryList.Add(new Country
                    {
                        Id = item.Country.Id,
                        Name = item.Country.Name,
                        Code = item.Country.Code,
                        PhonePrefix = item.Country.PhonePrefix,
                        Currency = item.Country.Currency,
                        GermanName = item.Country.GermanName
                    });
                }
            }
            return countryList;
        }

        /// <summary>
        /// Get Country based on id
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        public Country GetCountryByID(long countryID)
        {
            return ctx.Country.FirstOrDefault(c => c.Id == countryID);
        }

    }
}
