﻿using ParcelrateComponent.Interfaces;
using ParcelrateDataAccess.Interfaces;
using ParcelrateEntities;
using System.Collections.Generic;
using System.Linq;

namespace ParcelrateComponent
{
    public class ServiceProviderComponent: IServiceProviderComponent
    {
        private readonly IServiceProviderDataAccess _serviceProviderDataAccess;
        public ServiceProviderComponent(IServiceProviderDataAccess serviceProviderDA)
        {
            _serviceProviderDataAccess = serviceProviderDA;
        }

        /// <summary>
        /// Get ServiceProviders List based on country
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        public List<ServiceProviderEntity> GetServiceProvidersByCountry(long countryID)
        {
            var serviceProviderList = new List<ServiceProviderEntity>();
            var serviceProviders = _serviceProviderDataAccess.GetServiceProvidersByCountry(countryID);
            if (serviceProviders.Count > 0)
            {
                serviceProviderList= serviceProviders.Select(s => new ServiceProviderEntity
                {
                    ID = s.Id,
                    Name = s.Name,
                    Logo = s.Logo
                }).ToList();
            }
            return serviceProviderList;
        }

        /// <summary>
        /// Get Provider By ID   
        /// </summary>
        /// <param name="providerID"></param>
        /// <returns></returns>
        public ServiceProviderEntity GetProviderByID(long providerID)
        {
            return _serviceProviderDataAccess.GetProviderByID(providerID);
        }

        /// <summary>
        /// Get LetMeShip Provider ID   
        /// </summary>
        /// <returns></returns>
        public long GetLetMeShipProviderID()
        {
            return _serviceProviderDataAccess.GetLetMeShipProviderID();
        }
    }
}
