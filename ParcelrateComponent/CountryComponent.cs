﻿using ParcelrateComponent.Interfaces;
using ParcelrateDataAccess;
using ParcelrateDataAccess.Interfaces;
using ParcelrateEntities;
using System.Collections.Generic;
using System.Linq;

namespace ParcelrateComponent
{
    public class CountryComponent : ICountryComponent
    {
        private readonly ICountryDataAccess _countryDataAccess;
        public CountryComponent(ICountryDataAccess countryDA)
        {
            _countryDataAccess = countryDA;
        }

        /// <summary>
        /// Get Countries
        /// </summary>
        /// <param name="culture"></param>
        /// <returns></returns>
        public List<CountryEntity> GetCountries(string culture)
        {
            var countryList = new List<CountryEntity>();
            var countries = _countryDataAccess.GetCountries();
            if (countries != null && countries.Count > 0)
            {
                countryList = countries.Select(c => new CountryEntity
                {
                    ID = c.Id,
                    Name = culture != null && culture.ToLower().Contains("de") ? c.GermanName : c.Name,
                    Code = c.Code,
                    PhonePrefix = c.PhonePrefix,
                    Currency = c.Currency,
                    GermanName = c.GermanName
                }).ToList();
            }
            return countryList;
        }

        /// <summary>
        /// Get Countries By ServiceProvider
        /// </summary>
        /// <param name="serviceProviderID"></param>
        /// <returns></returns>
        public List<CountryEntity> GetCountriesByServiceProvider(long serviceProviderID)
        {
            var countryList = new List<CountryEntity>();
            var countries = _countryDataAccess.GetCountriesByServiceProvider(serviceProviderID);
            if (countries != null && countries.Count > 0)
            {
                countryList = countries.Select(c => new CountryEntity
                {
                    ID = c.Id,
                    Name = c.Name,
                    Code = c.Code,
                    PhonePrefix = c.PhonePrefix,
                    Currency = c.Currency,
                    GermanName = c.GermanName
                }).ToList();
            }
            return countryList;
        }

        /// <summary>
        /// Get All providers Country list
        /// </summary>
        /// <param name="culture"></param>
        /// <returns></returns>
        public List<CountryEntity> GetAllProvidersCountryList(string culture)
        {
            var countryList = new List<CountryEntity>();
            var countries = _countryDataAccess.GetAllProvidersCountryList();
            if (countries != null && countries.Count > 0)
            {
                countryList = countries.Select(c => new CountryEntity
                {
                    ID = c.Id,
                    Name = culture != null && culture.ToLower().Contains("de") ? c.GermanName : c.Name,
                    Code = c.Code,
                    PhonePrefix = c.PhonePrefix,
                    Currency = c.Currency
                }).ToList();
            }
            return countryList;
        }

        /// <summary>
        /// Get Country based on id
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        public CountryEntity GetCountryByID(long countryID)
        {
            var country = _countryDataAccess.GetCountryByID(countryID);
            return new CountryEntity
            {
                ID = country.Id,
                Name = country.Name,
                Code = country.Code,
                PhonePrefix = country.PhonePrefix,
                Currency = country.Currency,
                GermanName = country.GermanName
            };
        }
    }
}
