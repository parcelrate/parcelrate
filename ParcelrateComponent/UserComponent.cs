﻿using ParcelrateComponent.Interfaces;
using ParcelrateDataAccess.Interfaces;
using ParcelrateEntities;
using System.Collections.Generic;
using System.Linq;

namespace ParcelrateComponent
{
    public class UserComponent : IUserComponent
    {
        private readonly IUserDataAccess _userDataAccess;
        public UserComponent(IUserDataAccess userDA)
        {
            _userDataAccess = userDA;
        }

        /// <summary>
        /// get User Info
        /// </summary>
        /// <returns></returns>
        public List<UserEntity> GetUsers()
        {
            var userList = new List<UserEntity>();
            var users = _userDataAccess.GetUsers();
            if (users != null && users.Count > 0)
            {
                userList = users.Select(u => new UserEntity
                {
                    ID = u.UserId,
                    UserName = u.Username,
                    Password = u.Password,
                    ProviderID = u.ProviderId,
                    CountryID = u.CountryId,
                    Culture = u.Culture
                }).ToList();
            }
            return userList;
        }

        /// <summary>
        /// Add User Info
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        public bool AddUserInfo(UserEntity userInfo)
        {
            return _userDataAccess.AddUserInfo(userInfo);
        }

        /// <summary>
        /// Delete User Info by user id
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public bool DeleteUserInfo()
        {
            return _userDataAccess.DeleteUserInfo();
        }

        /// <summary>
        /// get User Info based on username
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public UserEntity GetUserByUsername(string userName)
        {
            var userEntity = new UserEntity();
            var user = _userDataAccess.GetUserByUsername(userName);
            if (user != null)
            {
                userEntity.ID = user.UserId;
                userEntity.UserName = user.Username;
                userEntity.Password = user.Password;
                userEntity.ProviderID = user.ProviderId;
                userEntity.CountryID = user.CountryId;
                userEntity.Culture = user.Culture;
            }
            return userEntity;
        }
    }
}
