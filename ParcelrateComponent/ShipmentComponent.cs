﻿using ParcelrateEntities;
using ParcelrateUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using ParcelrateEntities.Models;
using ParcelrateDataAccess.Interfaces;
using ParcelrateComponent.Interfaces;
using ReflectionIT.Mvc.Paging;
using Microsoft.Extensions.Logging;

namespace ParcelrateComponent
{
    public class ShipmentComponent : IShipmentComponent
    {
        private readonly IShipmentDataAccess _shipmentDataAccess;
        private readonly ICountryDataAccess _countryDataAccess;
        private readonly IServiceProviderDataAccess _serviceProviderDataAccess;
        private readonly ICarrierDataAccess _carrierDataAccess;
        private readonly ILogger<ShipmentComponent> _logger;

        public ShipmentComponent(IShipmentDataAccess shipmentDA, ICountryDataAccess countryDA, IServiceProviderDataAccess serviceProviderDA, ICarrierDataAccess carrierDA, ILogger<ShipmentComponent> logger)
        {
            _shipmentDataAccess = shipmentDA;
            _countryDataAccess = countryDA;
            _serviceProviderDataAccess = serviceProviderDA;
            _carrierDataAccess = carrierDA;
            _logger = logger;
        }


        /// <summary>
        /// Get shipments based on provider
        /// </summary>
        ///<param name="page"></param>
        ///<param name="providerID"></param>
        ///<param name="sortExpression"></param>
        ///<param name="userID"></param>
        /// <returns></returns>
        public List<ShipmentEntity> GetShipments(int page, string sortExpression, long providerID, long userID)
        {
            var shipmentList = new List<ShipmentEntity>();
            try
            {
                var qry = _shipmentDataAccess.GetShipmentsQuery(providerID, userID);
                var pagingList = PagingList.Create(qry, Constants.PageSize, page, sortExpression, Constants.ShipmentDefaultSortExpression);
                shipmentList = pagingList.Select(s => new ShipmentEntity
                {
                    ID = s.Id,
                    Carrier = s.Carrier,
                    Service = s.Service,
                    ProviderID = s.ProviderId,
                    ShipmentID = s.ShipmentId,
                    Reference = s.Reference,
                    TotalPrice = s.TotalPrice,
                    Status = GetShipmentStatus(s.Status),
                    PickupDate = Convert.ToDateTime(s.PickUpDate).ToString(Constants.DefaultDateFormat),
                    OrderBookedOn = s.OrderBookedOn.ToString(Constants.DefaultDateFormat),
                    DimensionsCaptured = s.DimensionsCaptured == Constants.ManualText ? ParcelrateResources.Resources.Manually : ParcelrateResources.Resources.Bedal,
                    Currency = s.PickupCountry?.Currency,
                    IsCancelled = s.Status.ToLower() == Constants.ShipmentCancelledStatus ? true : false,
                    AWBNumber = s.Awbnumber,
                    OldAWBNumber = s.Awbnumber
                }).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentComponent, Constants.MethodName.GetShipments, ex));
            }
            return shipmentList;
        }

        /// <summary>
        /// Get the shipment status from resource file based on Enum
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public string GetShipmentStatus(string status)
        {
            string shipmentStatus = string.Empty;
            try
            {
                Enums.ShipmentStatus statusState;
                if (Enum.TryParse(status, out statusState))
                {
                    switch ((Enums.ShipmentStatus)System.Enum.Parse(typeof(Enums.ShipmentStatus), status))
                    {
                        case Enums.ShipmentStatus.cancelled:
                            shipmentStatus = ParcelrateResources.Resources.Cancelled;
                            break;
                        case Enums.ShipmentStatus.locked:
                            shipmentStatus = ParcelrateResources.Resources.Invoiced;
                            break;
                        case Enums.ShipmentStatus.bookingInProgress:
                            shipmentStatus = ParcelrateResources.Resources.Booking_InProgress;
                            break;
                        case Enums.ShipmentStatus.bookingFinished:
                            shipmentStatus = ParcelrateResources.Resources.Booking_Finished;
                            break;
                        case Enums.ShipmentStatus.notFound:
                            shipmentStatus = ParcelrateResources.Resources.Not_Found;
                            break;
                        case Enums.ShipmentStatus.cancelRequested:
                            shipmentStatus = ParcelrateResources.Resources.Cancel_Requested;
                            break;
                        default:
                            shipmentStatus = ParcelrateResources.Resources.Booking_InProgress;
                            break;
                    }
                }
                else
                    shipmentStatus = ParcelrateResources.Resources.Booking_InProgress;
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentComponent, Constants.MethodName.GetShipmentStatus, ex));
            }
            return shipmentStatus;
        }


        /// <summary>
        /// Add Shipment
        /// </summary>
        /// <param name="shipment"></param>
        /// <returns></returns>
        public long AddUpdateShipment(ShipmentEntity shipment)
        {
            try
            {
                var shipmentEntity = new Shipment
                {
                    ProviderId = shipment.ProviderID,
                    Carrier = shipment.Carrier,
                    Service = shipment.Service,
                    ShipmentId = shipment.ShipmentID,
                    TotalPrice = shipment.TotalPrice,
                    Reference = shipment.Reference,
                    Status = shipment.Status,
                    PickUpDate = shipment.PickupDateTime,
                    OrderBookedOn = DateTime.Now,
                    DimensionsCaptured = shipment.DimensionsCaptured,
                    PickupCountryId = (long)shipment.PickupCountryId,
                    DeliveryCountryId = (long)shipment.DeliveryCountryId,
                    CarrierId = (long)shipment.CarrierId,
                    UserId = shipment.UserId
                };
                return _shipmentDataAccess.SaveShipment(shipmentEntity);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentComponent, Constants.MethodName.AddUpdateShipment, ex));
                return Constants.DefaultId;
            }
        }

        /// <summary>
        /// Update Shipment Label Status
        /// </summary>
        /// <param name="shipment"></param>
        /// <returns></returns>
        public bool UpdateShipmentLabelStatus(ShipmentEntity shipment)
        {
            bool isSuccess = false;
            try
            {
                long providerID;
                if (shipment.ProviderID <= 0 && shipment.IsLetMeShip)
                    providerID = _serviceProviderDataAccess.GetLetMeShipProviderID();
                else
                    providerID = shipment.ProviderID;
                var shipmentEntity = new Shipment
                {
                    ProviderId = providerID,
                    ShipmentId = shipment.ShipmentID,
                    Status = shipment.Status,
                    UserId = shipment.UserId
                };
                isSuccess = _shipmentDataAccess.UpdateShipmentLabelStatus(shipmentEntity);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentComponent, Constants.MethodName.UpdateShipmentLabelStatus, ex));
            }
            return isSuccess;
        }

        /// <summary>
        /// Get Shipments price by country
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<ChartEntity> GetShipmentsPriceByCountry(long providerID, long userID)
        {
            var chartDataList = new List<ChartEntity>();
            try
            {
                var shipments = _shipmentDataAccess.GetShipmentsByProvider(providerID, userID).Where(s => s.DeliveryCountryId > 0 && (DateTime.Now.Date - s.OrderBookedOn.Date).TotalDays <= 7).ToList();
                if (shipments.Count > 0)
                {
                    var random = new Random();
                    var countries = _countryDataAccess.GetCountries();
                    var countryGroup = shipments.GroupBy(s => s.DeliveryCountryId);
                    foreach (var item in countryGroup)
                    {
                        if (item.Key > 0)
                        {
                            var country = countries.FirstOrDefault(c => c.Id == (long)item.Key);
                            chartDataList.Add(new ChartEntity
                            {
                                Key = country != null ? country.Code : string.Empty,
                                Value = Convert.ToDouble(item.Select(x => x.TotalPrice).Sum()),
                                Color = string.Format("#{0:X6}", random.Next(0x1000000) & 0x7F7F7F),
                                Currency = country != null && country.Currency != null ? country.Currency : string.Empty,
                                Content = country != null ? country.Name : string.Empty
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentComponent, Constants.MethodName.GetShipmentsPriceByCountry, ex));
            }
            return chartDataList;
        }

        /// <summary>
        /// Get Shipments List price by country
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<Shipment> GetShipmentListPriceByCountry(long providerID, long userID)
        {
            var chartDataList = new List<Shipment>();
            try
            {
                chartDataList = _shipmentDataAccess.GetShipmentsQuery(providerID, userID).Where(s => s.DeliveryCountryId > 0 && (DateTime.Now.Date - s.OrderBookedOn.Date).TotalDays <= 7).ToList();

            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentComponent, Constants.MethodName.GetShipmentListPriceByCountry, ex));
            }
            return chartDataList;
        }

        /// <summary>
        /// Get Shipments price by carrier
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<ChartEntity> GetShipmentsPriceByCarrier(long providerID, long userID)
        {
            var chartDataList = new List<ChartEntity>();
            try
            {
                var shipments = _shipmentDataAccess.GetShipmentsByProvider(providerID, userID).Where(s => s.CarrierId > 0 && (DateTime.Now.Date - s.OrderBookedOn.Date).TotalDays <= 7).ToList();
                if (shipments.Count > 0)
                {
                    var random = new Random();
                    var carriers = _carrierDataAccess.GetCarriers();
                    var carrierGroup = shipments.GroupBy(s => s.CarrierId);
                    foreach (var item in carrierGroup)
                    {
                        if (item.Key > 0)
                        {
                            var carrier = carriers.FirstOrDefault(c => c.Id == (long)item.Key);
                            chartDataList.Add(new ChartEntity
                            {
                                Key = carrier != null ? carrier.Name : string.Empty,
                                Value = Convert.ToDouble(item.Select(x => x.TotalPrice).Sum()),
                                Color = carrier != null && string.IsNullOrEmpty(carrier.ColorCode) ? string.Format("#{0:X6}", random.Next(0x1000000) & 0x7F7F7F) : string.Format("#{0:X6}", carrier.ColorCode),
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentComponent, Constants.MethodName.GetShipmentsPriceByCarrier, ex));
            }
            return chartDataList;
        }

        /// <summary>
        /// Get Shipments List price by carrier
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<Shipment> GetShipmentsListPriceByCarrier(long providerID, long userID)
        {
            var chartDataList = new List<Shipment>();
            try
            {
                chartDataList = _shipmentDataAccess.GetShipmentsQuery(providerID, userID).Where(s => s.CarrierId > 0 && (DateTime.Now.Date - s.OrderBookedOn.Date).TotalDays <= 7).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentComponent, Constants.MethodName.GetShipmentsListPriceByCarrier, ex));
            }
            return chartDataList;
        }

        /// <summary>
        /// Get Shipments over lat one year
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<ChartEntity> GetShipmentsOverOneYear(long providerID, long userID)
        {
            var chartDataList = new List<ChartEntity>();
            try
            {
                var shipments = _shipmentDataAccess.GetShipmentsByProvider(providerID, userID).Where(x => x.OrderBookedOn.Date <= DateTime.Now.Date && x.OrderBookedOn.Date >= DateTime.Now.AddYears(-1).Date).ToList();
                if (shipments.Count > 0)
                {
                    var random = new Random();
                    var dateGroup = shipments.GroupBy(s => s.OrderBookedOn.Date);
                    foreach (var item in dateGroup)
                    {
                        if (item.Key != null)
                        {
                            chartDataList.Add(new ChartEntity
                            {
                                Key = item.Key.ToString(Constants.DefaultDateFormat),
                                Value = item.Select(x => x).Count(),
                                Color = string.Format("#{0:X6}", random.Next(0x1000000) & 0x7F7F7F)
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentComponent, Constants.MethodName.GetShipmentsOverOneYear, ex));
            }
            return chartDataList;
        }

        /// <summary>
        /// Get Shipments List over lat one year
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<Shipment> GetShipmentsListOverOneYear(long providerID, long userID)
        {
            var chartDataList = new List<Shipment>();
            try
            {
                chartDataList = _shipmentDataAccess.GetShipmentsQuery(providerID, userID).Where(x => x.OrderBookedOn.Date <= DateTime.Now.Date && x.OrderBookedOn.Date >= DateTime.Now.AddYears(-1).Date).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentComponent, Constants.MethodName.GetShipmentsListOverOneYear, ex));
            }
            return chartDataList;
        }

        /// <summary>
        /// Get Shipment based on id
        /// </summary>
        /// <param name="shipmentID"></param>
        /// <returns></returns>
        public ShipmentEntity GetShipmentByID(long shipmentID)
        {
            var shipmentEntity = new ShipmentEntity();
            try
            {
                var shipment = _shipmentDataAccess.GetShipmentByID(shipmentID);
                if (shipment != null)
                {
                    shipmentEntity.ID = shipment.Id;
                    shipmentEntity.Carrier = shipment.Carrier;
                    shipmentEntity.CarrierId = shipment.CarrierId;
                    shipmentEntity.Service = shipment.Service;
                    shipmentEntity.ShipmentID = shipment.ShipmentId;
                    shipmentEntity.Status = shipment.Status;
                    shipmentEntity.AWBNumber = shipment.Awbnumber;
                    if (shipment.PickupCountry != null)
                    {
                        shipmentEntity.PickupCountry = new CountryEntity
                        {
                            ID = shipment.PickupCountry.Id,
                            Name = shipment.PickupCountry.Name,
                            Code = shipment.PickupCountry.Code,
                            PhonePrefix = shipment.PickupCountry.PhonePrefix,
                            Currency = shipment.PickupCountry.Currency,
                            GermanName = shipment.PickupCountry.GermanName,
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentComponent, Constants.MethodName.GetShipmentByID, ex));
            }
            return shipmentEntity;
        }

        /// <summary>
        /// Get Shipment ids by provider
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<ShipmentCountry> GetLetMeShipShipmentIDs(long userID)
        {
            //var shipmentIDs = new List<long>();
            var shipmentList = new List<ShipmentCountry>();
            try
            {
                var shipments = _shipmentDataAccess.GetLetMeShipShipments(userID);
                if (shipments != null && shipments.Count > 0)
                    shipmentList = shipments.Where(x => x.Status.ToLower() != Constants.ShipmentCancelledStatus && x.Status.ToLower() != Constants.ShipmentInvoicedStatus).Select(x =>
                     new ShipmentCountry { ShipmentID = x.ShipmentId, CountryCode = x.PickupCountry.Code }).ToList();
                //shipmentIDs = shipments.Where(x => x.Status.ToLower() != Constants.ShipmentCancelledStatus && x.Status.ToLower() != Constants.ShipmentInvoicedStatus).Select(x => x.ShipmentId).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentComponent, Constants.MethodName.GetLetMeShipShipmentIDs, ex));
            }
            return shipmentList;
        }

        /// <summary>
        /// Get Shipments count by carrier
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<ChartEntity> GetShipmentsCountByCarrier(long providerID, long userID)
        {
            var chartDataList = new List<ChartEntity>();
            try
            {
                var shipments = _shipmentDataAccess.GetShipmentsByProvider(providerID, userID).Where(s => s.CarrierId > 0 && (DateTime.Now.Date - s.OrderBookedOn.Date).TotalDays <= 7).ToList();
                if (shipments.Count > 0)
                {
                    var random = new Random();
                    var carriers = _carrierDataAccess.GetCarriers();
                    var carrierGroup = shipments.GroupBy(s => s.CarrierId);
                    foreach (var item in carrierGroup)
                    {
                        if (item.Key > 0)
                        {
                            var carrier = carriers.FirstOrDefault(c => c.Id == (long)item.Key);
                            chartDataList.Add(new ChartEntity
                            {
                                Key = carrier != null ? carrier.Name : string.Empty,
                                Value = Convert.ToDouble(item.Select(x => x).ToList().Count),
                                Color = carrier != null && string.IsNullOrEmpty(carrier.ColorCode) ? string.Format("#{0:X6}", random.Next(0x1000000) & 0x7F7F7F) : string.Format("#{0:X6}", carrier.ColorCode),
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentComponent, Constants.MethodName.GetShipmentsCountByCarrier, ex));
            }
            return chartDataList;
        }

        /// <summary>
        /// Get Shipments List count by carrier
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<Shipment> GetShipmentsListCountByCarrier(long providerID, long userID)
        {
            var chartDataList = new List<Shipment>();
            try
            {
                chartDataList = _shipmentDataAccess.GetShipmentsQuery(providerID, userID).Where(s => s.CarrierId > 0 && (DateTime.Now.Date - s.OrderBookedOn.Date).TotalDays <= 7).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentComponent, Constants.MethodName.GetShipmentsListCountByCarrier, ex));
            }
            return chartDataList;
        }

        /// <summary>
        /// Update Shipment AWB Number
        /// </summary>
        /// <param name="awbNumber"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool UpdateShipmentAWBNumber(long id, string awbNumber)
        {
            bool isSuccess = false;
            try
            {
                var shipmentEntity = new Shipment
                {
                    Id = id,
                    Awbnumber = awbNumber
                };
                isSuccess = _shipmentDataAccess.UpdateShipmentAWBNumber(shipmentEntity);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.ShipmentComponent, Constants.MethodName.UpdateShipmentAWBNumber, ex));
            }
            return isSuccess;
        }

        /// <summary>
        /// Get Shipment Query
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public IQueryable<Shipment> GetShipmentsQuery(long providerID, long userID)
        {
            return _shipmentDataAccess.GetShipmentsQuery(providerID, userID);
        }
    }
}
