﻿using Microsoft.Extensions.Logging;
using ParcelrateComponent.Interfaces;
using ParcelrateDataAccess.Interfaces;
using ParcelrateEntities;
using ParcelrateEntities.Models;
using ParcelrateUtilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ParcelrateComponent
{
    public class CarrierComponent : ICarrierComponent
    {
        private readonly ICarrierDataAccess _carrierDataAccess;
        private readonly ILogger<CarrierComponent> _logger;
        public CarrierComponent(ICarrierDataAccess carrierDA, ILogger<CarrierComponent> logger)
        {
            _carrierDataAccess = carrierDA;
            _logger = logger;
        }

        /// <summary>
        /// Get Carriers List
        /// </summary>
        /// <returns></returns>
        public List<CarrierEntity> GetCarriers()
        {
            var carriers = new List<CarrierEntity>();
            var carrierList = _carrierDataAccess.GetCarriers();
            if (carrierList != null && carrierList.Count > 0)
            {
                carriers = carrierList.Select(c => new CarrierEntity
                {
                    Id = c.Id,
                    Name = c.Name,
                    Logo = c.Logo,
                    AllPackaCarrierId = c.AllPackaCarrierId,
                    DoesProvideLabel = c.DoesProvideLabel
                }).ToList();
            }
            return carriers;
        }

        /// <summary>
        /// Get Carrier based on carrier id
        /// </summary>
        /// <param name="carrierID"></param>
        /// <returns></returns>
        public CarrierEntity GetCarrierByID(long carrierID)
        {
            var carrier = new CarrierEntity();
            var carrierEntity = _carrierDataAccess.GetCarrierByID(carrierID);
            if (carrierEntity != null)
            {
                carrier.Id = carrierEntity.Id;
                carrier.Name = carrierEntity.Name;
                carrier.ColorCode = carrierEntity.ColorCode;
            }
            return carrier;
        }

        /// <summary>
        /// Get Carrier based on carrier name
        /// </summary>
        /// <param name="carrierName"></param>
        /// <returns></returns>
        public long GetCarrierIDByName(string carrierName)
        {
            try
            {
                var carrierEntity = _carrierDataAccess.GetCarrierByName(carrierName);
                if (carrierEntity != null)
                    return carrierEntity.Id;
                else
                {
                    return _carrierDataAccess.AddCarrier(new Carrier
                    {
                        Name = carrierName,
                        Logo = Constants.DefaultCarrierLogo,
                        ColorCode = string.Empty
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.CarrierComponent, Constants.MethodName.ProcessShipment, ex));
                return Constants.DefaultId;
            }
        }
    }
}
