﻿using Microsoft.Extensions.Logging;
using ParcelrateComponent.Interfaces;
using ParcelrateDataAccess.Interfaces;
using ParcelrateEntities;
using ParcelrateEntities.Models;
using ParcelrateUtilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ParcelrateComponent
{
    public class AddressComponent : IAddressComponent
    {
        private readonly IAddressDataAccess _addressDataAccess;
        private readonly ICountryDataAccess _countryDataAccess;
        private readonly ILogger<AddressComponent> _logger;
        public AddressComponent(IAddressDataAccess addressDA, ICountryDataAccess countryDA, ILogger<AddressComponent> logger)
        {
            _addressDataAccess = addressDA;
            _countryDataAccess = countryDA;
            _logger = logger;
        }

        /// <summary>
        /// Get Addresses
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<AddressEntity> GetAddresses(long userID)
        {
            var addressList = new List<AddressEntity>();
            var addresses = _addressDataAccess.GetAddresses(userID).ToList();
            if (addresses != null && addresses.Count > 0)
            {
                addressList = addresses.Select(a => new AddressEntity
                {
                    Id = a.Id,
                    CustomerID = a.CustomerId,
                    FirstName = a.FirstName,
                    LastName = a.LastName,
                    Address1 = a.Address1,
                    Address2 = a.Address2,
                    Company = a.Company,
                    PhonePrefix = a.Country.PhonePrefix,
                    PhoneNo = Convert.ToString(a.PhoneNumber),
                    PostalCode = a.PostalCode,
                    City = a.City,
                    Email = a.Email,
                    CountryId = Convert.ToInt64(a.CountryId),
                    CountryCode = a.Country.Code,
                    IsDefault = a.IsDefault,
                    IsPickupOrder = a.IsPickupOrder,
                    CountryName = a.Country?.Name
                }).ToList();
            }
            return addressList;
        }

        /// <summary>
        /// Get Address based on address id
        /// </summary>
        /// <param name="addressID"></param>
        /// <returns></returns>
        public AddressEntity GetAddressByID(long addressID)
        {
            var address = _addressDataAccess.GetAddressByID(addressID);
            if (address != null)
                return MapAddress(address);
            return null;
        }

        /// <summary>
        /// Get Address based on address id
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public AddressEntity GetDefaultAddress(long userID)
        {
            var address = _addressDataAccess.GetDefaultAddress(userID);
            if (address != null)
            {
                return MapAddress(address);
            }
            return null;
        }

        private AddressEntity MapAddress(Address address)
        {
            return new AddressEntity
            {
                Id = address.Id,
                CustomerID = address.CustomerId,
                FirstName = address.FirstName,
                LastName = address.LastName,
                Address1 = address.Address1,
                Address2 = address.Address2,
                Company = address.Company,
                CountryId = Convert.ToInt64(address.CountryId),
                CountryCode = address.Country.Code,
                PhonePrefix = address.Country?.PhonePrefix,
                PhoneNo = Convert.ToString(address.PhoneNumber),
                PostalCode = address.PostalCode,
                City = address.City,
                Email = address.Email,
                IsDefault = address.IsDefault,
                IsPickupOrder = address.IsPickupOrder,
                CountryCurrency = address.Country?.Currency
            };
        }

        /// <summary>
        /// Add Address
        /// </summary>
        /// <param name="address"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public long AddAddress(AddressEntity address, long userID)
        {
            try
            {
                if (address != null)
                {
                    var addr = new Address
                    {
                        Id = address.Id,
                        CustomerId = address.CustomerID,
                        FirstName = address.FirstName,
                        LastName = address.LastName,
                        Address1 = address.Address1,
                        Address2 = address.Address2,
                        Company = address.Company,
                        City = address.City,
                        CountryId = address.CountryId,
                        CountryCode = address.CountryCode,
                        PhonePrefix = address.PhonePrefix,
                        PhoneNumber = Convert.ToInt64(address.PhoneNo),
                        PostalCode = address.PostalCode,
                        Email = address.Email,
                        IsDefault = address.IsDefault,
                        IsPickupOrder = address.IsPickupOrder,
                        UserId = userID
                    };
                    return _addressDataAccess.SaveAddress(addr);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.AddressComponent, Constants.MethodName.AddAddress, ex));
            }
            return Constants.DefaultId;
        }

        /// <summary>
        /// Add Addresses from datatable
        /// </summary>
        /// <param name="dtAddress"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public bool AddAddressesFromDataTable(DataTable dtAddress, long userID)
        {
            try
            {
                var addressEntityList = new List<Address>();
                if (dtAddress != null && dtAddress.Rows.Count > 0)
                {
                    var countryList = _countryDataAccess.GetCountries();
                    for (int i = 0; i < dtAddress.Rows.Count; i++)
                    {
                        long? phone = long.TryParse(Convert.ToString(dtAddress.Rows[i]["PhoneNumber"]), System.Globalization.NumberStyles.Float, null, out long result) ? result : (Nullable<long>)null;
                        var addr = new Address
                        {
                            CustomerId = Convert.ToString(dtAddress.Rows[i]["CustomerId"]).ToUpper(),
                            FirstName = Convert.ToString(dtAddress.Rows[i]["FirstName"]),
                            LastName = Convert.ToString(dtAddress.Rows[i]["LastName"]),
                            Address1 = Convert.ToString(dtAddress.Rows[i]["Street"]).Replace("|", ","),
                            Address2 = Convert.ToString(dtAddress.Rows[i]["HouseNo"]).Replace("|", ","),
                            Company = Convert.ToString(dtAddress.Rows[i]["Company"]),
                            City = Convert.ToString(dtAddress.Rows[i]["City"]),
                            CountryCode = Convert.ToString(dtAddress.Rows[i]["CountryCode"]),
                            PhoneNumber = phone,
                            PostalCode = Convert.ToString(dtAddress.Rows[i]["PostalCode"]),
                            Email = Convert.ToString(dtAddress.Rows[i]["Email"]),
                            IsDefault = false,
                            IsPickupOrder = false,
                            UserId = userID
                        };

                        var country = countryList.FirstOrDefault(c => c.Code == addr.CountryCode);
                        if (country != null)
                        {
                            addr.CountryId = country.Id;
                            addr.PhonePrefix = country.PhonePrefix;
                        }
                        addressEntityList.Add(addr);
                    }
                }
                return _addressDataAccess.SaveAddresses(addressEntityList);
            }
            catch (Exception ex)
            {
                _logger.LogError(StaticFunctions.LogErrorMessage(Constants.ViewModels.AddressComponent, Constants.MethodName.AddAddresses, ex));
                return false;
            }
        }

        /// <summary>
        /// Get Address Customers List
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<CustomerEntity> GetAddressCustomers(long userID)
        {
            var customerList = new List<CustomerEntity>();
            var addresses = _addressDataAccess.GetAddresses(userID).Where(a => !a.IsDefault).ToList();
            if (addresses.Count > 0)
            {
                customerList = addresses.Select(a => new CustomerEntity
                {
                    AddressID = a.Id,
                    CustomerID = a.CustomerId
                }).ToList();
            }
            return customerList;
        }

        /// <summary>
        ///Delete  address based on address id
        /// </summary>
        /// <param name="addressID"></param>
        /// <returns></returns>
        public bool DeleteAddress(long addressID)
        {
            return _addressDataAccess.DeleteAddress(addressID);
        }

        /// <summary>
        /// Get Addresses Query
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public IQueryable<Address> GetAddressesQuery(long userID)
        {
            return _addressDataAccess.GetAddresses(userID);
        }
    }
}
