﻿using ParcelrateEntities;
using System.Collections.Generic;

namespace ParcelrateComponent.Interfaces
{
    public interface IServiceProviderComponent
    {
        /// <summary>
        /// Get ServiceProviders List based on country
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        List<ServiceProviderEntity> GetServiceProvidersByCountry(long countryID);

        /// <summary>
        /// Get Provider By ID   
        /// </summary>
        /// <param name="providerID"></param>
        /// <returns></returns>
        ServiceProviderEntity GetProviderByID(long providerID);

        /// <summary>
        /// Get LetMeShip Provider ID   
        /// </summary>
        /// <returns></returns>
        long GetLetMeShipProviderID();
    }
}
