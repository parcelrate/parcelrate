﻿using ParcelrateEntities;
using System.Collections.Generic;

namespace ParcelrateComponent.Interfaces
{
    public interface ICountryComponent
    {
        /// <summary>
        /// Get Countries
        /// </summary>
        /// <param name="culture"></param>
        /// <returns></returns>
        List<CountryEntity> GetCountries(string culture);

        /// <summary>
        /// Get Countries By ServiceProvider
        /// </summary>
        /// <param name="serviceProviderID"></param>
        /// <returns></returns>
        List<CountryEntity> GetCountriesByServiceProvider(long serviceProviderID);

        /// <summary>
        /// Get All providers Country list
        /// </summary>
        /// <param name="culture"></param>
        /// <returns></returns>
        List<CountryEntity> GetAllProvidersCountryList(string culture);

        /// <summary>
        /// Get Country based on id
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        CountryEntity GetCountryByID(long countryID);
    }
}
