﻿using ParcelrateEntities;
using System.Collections.Generic;

namespace ParcelrateComponent.Interfaces
{
    public interface ICarrierComponent
    {
        /// <summary>
        /// Get Carriers List
        /// </summary>
        /// <returns></returns>
        List<CarrierEntity> GetCarriers();

        /// <summary>
        /// Get Carrier based on carrier id
        /// </summary>
        /// <param name="carrierID"></param>
        /// <returns></returns>
        CarrierEntity GetCarrierByID(long carrierID);

        /// <summary>
        /// Get Carrier based on carrier name
        /// </summary>
        /// <param name="carrierName"></param>
        /// <returns></returns>
        long GetCarrierIDByName(string carrierName);
    }
}
