﻿using ParcelrateEntities;
using System.Collections.Generic;

namespace ParcelrateComponent.Interfaces
{
    public interface IUserComponent
    {
        /// <summary>
        /// get User Info
        /// </summary>
        /// <returns></returns>
        List<UserEntity> GetUsers();

        /// <summary>
        /// Add User Info
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        bool AddUserInfo(UserEntity userInfo);

        /// <summary>
        /// Delete User Info
        /// </summary>
        /// <returns></returns>
        bool DeleteUserInfo();

        /// <summary>
        /// get User Info based on username
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        UserEntity GetUserByUsername(string userName);
    }
}
