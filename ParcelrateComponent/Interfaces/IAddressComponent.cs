﻿using ParcelrateEntities;
using ParcelrateEntities.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ParcelrateComponent.Interfaces
{
    public interface IAddressComponent
    {
        /// <summary>
        /// Get Addresses
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        List<AddressEntity> GetAddresses(long userID);

        /// <summary>
        /// Get Address based on address id
        /// </summary>
        /// <param name="addressID"></param>
        /// <returns></returns>
        AddressEntity GetAddressByID(long addressID);

        /// <summary>
        /// Get Address based on address id
        /// </summary>
        /// <returns></returns>
        AddressEntity GetDefaultAddress(long userID);

        /// <summary>
        /// Add Address
        /// </summary>
        /// <param name="address"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        long AddAddress(AddressEntity address, long userID);

        /// <summary>
        /// Add Addresses from datatable
        /// </summary>
        /// <param name="dtAddress"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        bool AddAddressesFromDataTable(DataTable dtAddress, long userID);

        /// <summary>
        /// Get Address Customers List
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        List<CustomerEntity> GetAddressCustomers(long userID);

        /// <summary>
        ///Delete  address based on address id
        /// </summary>
        /// <param name="addressID"></param>
        /// <returns></returns>
        bool DeleteAddress(long addressID);

        /// <summary>
        /// Get Address Query
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        IQueryable<Address> GetAddressesQuery(long userID);
    }
}
