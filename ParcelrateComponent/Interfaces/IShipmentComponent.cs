﻿using ParcelrateEntities;
using ParcelrateEntities.Models;
using System.Collections.Generic;
using System.Linq;

namespace ParcelrateComponent.Interfaces
{
    public interface IShipmentComponent
    {
        /// <summary>
        /// Get shipments based on provider
        /// </summary>
        ///<param name="page"></param>
        ///<param name="providerID"></param>
        ///<param name="sortExpression"></param>
        ///<param name="userID"></param>
        /// <returns></returns>
        List<ShipmentEntity> GetShipments(int page, string sortExpression, long providerID, long userID);

        /// <summary>
        /// Get the shipment status from resource file based on Enum
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        string GetShipmentStatus(string status);

        /// <summary>
        /// Add Shipment
        /// </summary>
        /// <param name="shipment"></param>
        /// <returns></returns>
        long AddUpdateShipment(ShipmentEntity shipment);

        /// <summary>
        /// Update Shipment Label Status
        /// </summary>
        /// <param name="shipment"></param>
        /// <returns></returns>
        bool UpdateShipmentLabelStatus(ShipmentEntity shipment);

        /// <summary>
        /// Get Shipments price by country
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        List<ChartEntity> GetShipmentsPriceByCountry(long providerID, long userID);

        /// <summary>
        /// Get Shipments List price by country
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        List<Shipment> GetShipmentListPriceByCountry(long providerID, long userID);

        /// <summary>
        /// Get Shipments price by carrier
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        List<ChartEntity> GetShipmentsPriceByCarrier(long providerID, long userID);

        /// <summary>
        /// Get Shipments List price by carrier
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        List<Shipment> GetShipmentsListPriceByCarrier(long providerID, long userID);

        /// <summary>
        /// Get Shipments over lat one year
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        List<ChartEntity> GetShipmentsOverOneYear(long providerID, long userID);

        /// <summary>
        /// Get Shipments List over lat one year
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        List<Shipment> GetShipmentsListOverOneYear(long providerID, long userID);

        /// <summary>
        /// Get Shipment based on id
        /// </summary>
        /// <param name="shipmentID"></param>
        /// <returns></returns>
        ShipmentEntity GetShipmentByID(long shipmentID);

        /// <summary>
        /// Get Shipment ids by provider
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        List<ShipmentCountry> GetLetMeShipShipmentIDs(long userID);

        /// <summary>
        /// Get Shipments count by carrier
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        List<ChartEntity> GetShipmentsCountByCarrier(long providerID, long userID);

        /// <summary>
        /// Get Shipments List count by carrier
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        List<Shipment> GetShipmentsListCountByCarrier(long providerID, long userID);

        /// <summary>
        /// Update Shipment AWB Number
        /// </summary>
        /// <param name="awbNumber"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        bool UpdateShipmentAWBNumber(long id, string awbNumber);

        /// <summary>
        /// Get Shipment Query
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        IQueryable<Shipment> GetShipmentsQuery(long providerID, long userID);
    }
}
